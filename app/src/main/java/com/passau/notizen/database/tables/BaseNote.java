package com.passau.notizen.database.tables;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.passau.notizen.data.Note;
import com.passau.notizen.sync.dto.NoteDTO;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity(tableName = "basenote_table")
public class BaseNote {

    @PrimaryKey
    @NonNull
    private final UUID id;

    private final String headline;

    @NonNull
    private final String body;

    @NonNull
    @ColumnInfo(name = "last_change")
    private final LocalDateTime lastChange;

    @NonNull
    private final String email;

    @ColumnInfo(name = "is_archived")
    private final boolean isArchived;

    @ColumnInfo(name = "is_pinned")
    private final boolean isPinned;

    @ColumnInfo(name = "due_date")
    private final LocalDateTime dueDate;

    public BaseNote(@NonNull UUID id, String headline, @NonNull String body, @NonNull LocalDateTime lastChange,
                    @NonNull String email, boolean isArchived, boolean isPinned, LocalDateTime dueDate) {
        this.id = id;
        this.headline = headline;
        this.body = body;
        this.lastChange = lastChange;
        this.email = email;
        this.isArchived = isArchived;
        this.isPinned = isPinned;
        this.dueDate = dueDate;
    }

    public BaseNote(Note note) {
        this.id = note.getId();
        this.headline = note.getHeadline();
        this.body = note.getBody();
        this.lastChange = note.getLastChange();
        this.email = note.getEmail();
        this.isArchived = note.isArchived();
        this.isPinned = note.isPinned();
        this.dueDate = note.getDueDate();
    }

    public BaseNote(NoteDTO noteDTO, boolean isPinned){
        this.id = UUID.fromString(noteDTO.getId());
        this.headline = noteDTO.getHeadline();
        this.body = noteDTO.getBody();
        this.lastChange = noteDTO.getLastChangeTimeStamp();
        this.email = noteDTO.getAuthor();
        this.isArchived = noteDTO.isArchived();
        this.isPinned = isPinned;
        this.dueDate = noteDTO.getDueDate();
    }

    public BaseNote(NoteDTO noteDTO){
        this(noteDTO, false);
    }

    @NonNull
    public UUID getId() {
        return id;
    }

    public String getHeadline() {
        return headline;
    }

    @NonNull
    public String getBody() {
        return body;
    }

    @NonNull
    public LocalDateTime getLastChange() {
        return lastChange;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public boolean isPinned() {
        return isPinned;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }
}

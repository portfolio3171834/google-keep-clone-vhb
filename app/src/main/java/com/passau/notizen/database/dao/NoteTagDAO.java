package com.passau.notizen.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.passau.notizen.database.tables.NoteTag;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Dao
public interface NoteTagDAO {
    @Insert
    void insertNoteTag(NoteTag noteTag);

    @Update
    void updateNoteTag(NoteTag noteTag);

    @Delete
    void deleteNoteTag(NoteTag noteTag);

    @Query("DELETE FROM note_tags_table WHERE tag_id NOT IN (:tagIds) AND base_note_id = :baseNoteId")
    void deleteNoteTagByTagIds(List<UUID>  tagIds, UUID baseNoteId);

    @Query("SELECT tag_id FROM note_tags_table WHERE base_note_id = :noteId")
    List<UUID> getTagIdByNoteIs(UUID noteId);
}



package com.passau.notizen.database;

import android.content.Context;

import androidx.room.Room;

import com.passau.notizen.cryptography.DatabaseKeyManager;
import com.passau.notizen.database.tables.BaseNote;
import com.passau.notizen.database.tables.MediaObject;
import com.passau.notizen.database.tables.NoteTag;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.database.tables.User;

import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SupportFactory;

import java.util.ArrayList;
import java.util.UUID;


// Helper class to grant access to the database
public class NoteDatabaseService {

    private static NoteDatabaseService noteDbService;

    private static final String DATABASE_NAME = "tasks-db";
    private final Context context;
    private NoteDatabase db;

    public static void forceCloseDatabaseService() {
        // TODO: 26.03.2022 keep?
        noteDbService.db.close();
        noteDbService.db = null;
        noteDbService = null;
    }


    private NoteDatabaseService(Context context, char[] passkey) throws DatabaseKeyManager.DatabaseKeyException {
        this.context = context;
        initDatabase(passkey);
    }

    public static NoteDatabaseService getInstance(Context context, char[] passkey) throws DatabaseKeyManager.DatabaseKeyException {
        if (noteDbService == null) {
            noteDbService =  new NoteDatabaseService(context, passkey);
        }
        return noteDbService;
    }

    public void initDatabase(char[] passcode) throws DatabaseKeyManager.DatabaseKeyException {
        // TODO: 10.03.2022 needs extra thread
        char[] dbKey = DatabaseKeyManager.getInstance(context).getCharKey(passcode, context);

        SupportFactory factory = new SupportFactory(SQLiteDatabase.getBytes(dbKey));
        db = Room.databaseBuilder(context, NoteDatabase.class, DATABASE_NAME)
                .openHelperFactory(factory)
                .allowMainThreadQueries().build();
    }


    public User getUserByMail(String email) {
        // TODO: 10.03.2022 if null abfangen
        return db.userDAO().findUserByEmail(email);
    }



    public void addUser(User user) {
        if (db.userDAO().findUserByEmail(user.getEmailAddress()) == null)
            db.userDAO().insertUser(user);
        else {
            // TODO: 12.03.2022 call listener method -> user already exists
        }
    }

    //Use Cases

    //Adds a Note to the Database table BaseNote
    public void addBaseNote(BaseNote baseNote){
        try {
            db.baseNoteDAO().insertBaseNote(baseNote);
        } catch (SQLException ignored){}
    }

    //returns all Tags
    public ArrayList<Tag> getAllTags(){
        return new ArrayList<>(db.tagDAO().getAllTags()) ;
    }

    //returns all BaseNotes in a List for the email of the user sort by lastChange date
    public ArrayList<BaseNote> getAllNotes(){
        return new ArrayList<>(db.baseNoteDAO().getAllNotes());
    }

    public ArrayList<MediaObject> getAllMediaObjects() {
        return new ArrayList<>(db.mediaObjectDAO().getAllMediaObjects());
    }

    public ArrayList<UUID> getTagIdsByNoteId(UUID noteId) {
        return new ArrayList<>(db.noteTagDAO().getTagIdByNoteIs(noteId));
    }

    public void deleteBaseNote( BaseNote note) {
        db.baseNoteDAO().deleteNote(note);
    }

    //updates the BaseNote
    public void updateBaseNote(BaseNote baseNote){
        db.baseNoteDAO().updateBaseNote(baseNote);
    }

    //assign a Tag to a BaseNote
    public void addTagToBaseNote(NoteTag noteTag){
        db.noteTagDAO().insertNoteTag(noteTag);
    }

    public void removeTagFromBaseNote(NoteTag noteTag) {
        db.noteTagDAO().deleteNoteTag(noteTag);
    }

    public void removeTagsFromBaseNoteNotHavingIds(ArrayList<UUID> tagIds, UUID baseNoteId) {
        db.noteTagDAO().deleteNoteTagByTagIds(tagIds, baseNoteId);
    }

    public void removeMediaObjectsNotHavingIds(ArrayList<UUID> mediaObjectIds) {
        db.mediaObjectDAO().deleteMediaObjectsNotInIds(mediaObjectIds);
    }

    //Inserts a new Tag in the tag table
    public void addTag(Tag tag){
        db.tagDAO().insertTag(tag);
    }

    //deletes a Tag by the Tag
    public void deleteTag(Tag tag){
        db.tagDAO().deleteTag(tag);
    }

    //Updates a Tag by the Tag Objekt
    public void updateTag(Tag tag){
        db.tagDAO().updateTag(tag);
    }

    //inserts a MediaObject and assign it to the BaseNote
    public void addMediaObject(MediaObject mediaObject){
        db.mediaObjectDAO().insertMediaObject(mediaObject);
    }

    public void deleteMediaObject(MediaObject mediaObject){
        db.mediaObjectDAO().deleteMediaObject(mediaObject);
    }

    public void updateMediaObject(MediaObject mediaObject) {
        db.mediaObjectDAO().updateMediaObject(mediaObject);
    }

    public User getUser(){
        return db.userDAO().getUsers().get(0);
    }


}

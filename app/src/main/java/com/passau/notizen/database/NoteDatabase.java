package com.passau.notizen.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.passau.notizen.database.dao.BaseNoteDAO;
import com.passau.notizen.database.dao.MediaObjectDAO;
import com.passau.notizen.database.dao.NoteTagDAO;
import com.passau.notizen.database.dao.TagDAO;
import com.passau.notizen.database.dao.UserDAO;
import com.passau.notizen.database.tables.BaseNote;
import com.passau.notizen.database.tables.MediaObject;
import com.passau.notizen.database.tables.NoteTag;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.database.tables.User;


@Database(entities = {User.class, MediaObject.class, BaseNote.class, Tag.class, NoteTag.class}, version = 1)
@TypeConverters({DataBaseTypeConverter.class})
public abstract class NoteDatabase extends RoomDatabase {

    public abstract UserDAO userDAO();

    public abstract MediaObjectDAO mediaObjectDAO();

    public abstract BaseNoteDAO baseNoteDAO();

    public abstract TagDAO tagDAO();

    public abstract NoteTagDAO noteTagDAO();

}

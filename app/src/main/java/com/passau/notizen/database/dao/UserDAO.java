package com.passau.notizen.database.dao;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.passau.notizen.database.tables.User;

import java.util.List;


@Dao
public interface UserDAO {
    @Insert
    void insertUser(User user);

    @Update
    void updateUser(User user);

    @Delete
    void deleteUser(User user);

    @Query("SELECT * FROM user_table where email_address = :email")
    User findUserByEmail(String email);

    @Query("SELECT * FROM user_table")
    List<User> getUsers();
}

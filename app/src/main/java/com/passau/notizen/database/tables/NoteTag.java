package com.passau.notizen.database.tables;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import java.util.UUID;

@Entity(tableName = "note_tags_table", primaryKeys = {"tag_id", "base_note_id"}, foreignKeys = {
        @ForeignKey(entity = BaseNote.class,
                parentColumns = "id",
                childColumns = "base_note_id",
                onDelete = ForeignKey.CASCADE),
        @ForeignKey(entity = Tag.class,
                parentColumns = "id",
                childColumns = "tag_id",
                onDelete = ForeignKey.CASCADE)})

public class NoteTag {
    //@PrimaryKey
    @NonNull
    @ColumnInfo(name = "base_note_id")
    private final UUID baseNoteID;

    //@PrimaryKey
    @NonNull
    @ColumnInfo(name = "tag_id")
    private final UUID tagID;

    public NoteTag(@NonNull UUID baseNoteID, @NonNull UUID tagID) {
        this.baseNoteID = baseNoteID;
        this.tagID = tagID;
    }

    @NonNull
    public UUID getBaseNoteID() {
        return baseNoteID;
    }

    @NonNull
    public UUID getTagID() {
        return tagID;
    }
}

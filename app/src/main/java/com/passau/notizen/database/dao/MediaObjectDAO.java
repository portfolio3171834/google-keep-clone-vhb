package com.passau.notizen.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.passau.notizen.database.tables.MediaObject;

import java.util.List;
import java.util.UUID;

@Dao
public interface MediaObjectDAO {
    @Insert
    void insertMediaObject(MediaObject mediaObject);

    @Update
    void updateMediaObject(MediaObject mediaObject);

    @Delete
    void deleteMediaObject(MediaObject mediaObject);

    @Query("SELECT * FROM media_object_table")
    List<MediaObject> getAllMediaObjects();

    @Query("DELETE FROM media_object_table where id NOT IN (:ids)")
    void deleteMediaObjectsNotInIds(List<UUID> ids);
}



package com.passau.notizen.database.tables;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity(tableName = "media_object_table", foreignKeys = {@ForeignKey(entity = BaseNote.class,
        parentColumns = "id",
        childColumns = "note_uuid",
        onDelete = ForeignKey.CASCADE)})
public class MediaObject implements Parcelable {

    @PrimaryKey
    @NonNull
    private final UUID id;

    @NonNull
    @ColumnInfo(name = "local_path")
    private final String localPath;

    @NonNull
    @ColumnInfo(name = "media_type")
    private final MediaType mediaType;

    @NonNull
    @ColumnInfo(name = "note_uuid")
    private final UUID noteUUID;

    public MediaObject(@NonNull UUID id, @NonNull String localPath, @NonNull MediaType mediaType, @NonNull UUID noteUUID) {
        this.id = id;
        this.localPath = localPath;
        this.mediaType = mediaType;
        this.noteUUID = noteUUID;
    }

    @NonNull
    public UUID getId() {
        return id;
    }

    @NonNull
    public String getLocalPath() {
        return localPath;
    }

    @NonNull
    public MediaType getMediaType() {
        return mediaType;
    }

    @NonNull
    public UUID getNoteUUID() {
        return noteUUID;
    }


	/*=====================
	===== Parcelable ======
	* ====================*/

    protected MediaObject(Parcel in) {
        id = UUID.fromString(in.readString());
        localPath = in.readString();
        mediaType = MediaType.values()[in.readInt()];
        noteUUID = UUID.fromString(in.readString());
    }

    public static final Creator<MediaObject> CREATOR = new Creator<MediaObject>() {
        @Override
        public MediaObject createFromParcel(Parcel in) {
            return new MediaObject(in);
        }

        @Override
        public MediaObject[] newArray(int size) {
            return new MediaObject[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id.toString());
        dest.writeString(localPath);
        dest.writeInt(mediaType.ordinal());
        dest.writeString(noteUUID.toString());
    }

    public enum MediaType {
        AUDIO,
        IMAGE
    }
}



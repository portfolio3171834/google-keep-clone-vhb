package com.passau.notizen.database.tables;

import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.passau.notizen.sync.dto.TagDTO;

import java.util.Objects;
import java.util.UUID;

@Entity(tableName = "tag_table")
public class Tag implements Comparable<Tag>, Parcelable {


    @NonNull
    @PrimaryKey
    private final UUID id;

    @ColumnInfo(name = "tag_name")
    private final String tagName;

    public Tag(@NonNull UUID id, @NonNull String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public Tag(TagDTO tagDTO){
        this.id = UUID.fromString(tagDTO.getId());
        this.tagName = tagDTO.getTagName();
    }


    @NonNull
    public UUID getId() {
        return id;
    }

    @NonNull
    public String getTagName() {
        return tagName;
    }


    public Tag copy() {
        return new Tag(id,tagName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(o instanceof Tag) {
            Tag tagObject = (Tag) o;
            return (id.equals(tagObject.id) || tagName.equals(tagObject.tagName));
        } else return false;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", tagName='" + tagName + '\'' +
                '}';
    }

    @Override
    public int compareTo(Tag o) {
        return this.getTagName().compareTo(o.getTagName());
    }

        /*=====================
	===== Parcelable ======
	* ====================*/

    // Implemented using https://stackoverflow.com/a/48643772/7831593 as example
    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            String tagName = in.readString();
            String idString = in.readString();
            UUID id = UUID.fromString(idString);
            return new Tag(id, tagName);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tagName);
        dest.writeString(id.toString());
    }

    @Override
    public int describeContents() {
        return 0;
    }
}

package com.passau.notizen.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.passau.notizen.database.tables.Tag;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface TagDAO {
    @Insert
    void insertTag(Tag tag);

    @Update
    void updateTag(Tag tag);

    @Delete
    void deleteTag(Tag tag);

    //returns all Tags from the Tag table (regardless of the user) because only one User on one Device
    @Query("SELECT DISTINCT * FROM tag_table")
    List<Tag> getAllTags();
}

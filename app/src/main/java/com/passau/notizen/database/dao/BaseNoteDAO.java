package com.passau.notizen.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.passau.notizen.database.tables.BaseNote;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface BaseNoteDAO {
    @Insert
    void insertBaseNote(BaseNote baseNote);

    @Update
    void updateBaseNote(BaseNote baseNote);

    @Delete
    void deleteNote(BaseNote baseNote);

    //returns all Basenotes order by last change
    @Query("SELECT * FROM basenote_table ORDER BY last_change")
    List<BaseNote> getAllNotes();
}

package com.passau.notizen.database.tables;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.LocalDateTime;

@Entity(tableName = "user_table")
public class User {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "email_address")
    private final String emailAddress;

    @NonNull
    @ColumnInfo(name = "root_path")
    private final String rootPath;

    @NonNull
    @ColumnInfo(name = "last_synchronized")
    private LocalDateTime lastSynchronized;

    public User(@NonNull String emailAddress, @NonNull String rootPath, @NonNull LocalDateTime lastSynchronized) {
        this.emailAddress = emailAddress;
        this.rootPath = rootPath;
        this.lastSynchronized = lastSynchronized;
    }

    @NonNull
    public String getEmailAddress() {
        return emailAddress;
    }

    @NonNull
    public String getRootPath() {
        return rootPath;
    }

    @NonNull
    public LocalDateTime getLastSynchronized() {
        return lastSynchronized;
    }

    public void setLastSynchronized(@NonNull LocalDateTime lastSynchronized) {
        this.lastSynchronized = lastSynchronized;
    }
}

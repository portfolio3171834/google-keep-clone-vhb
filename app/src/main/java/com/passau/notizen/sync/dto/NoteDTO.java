package com.passau.notizen.sync.dto;

import android.content.Context;

import com.passau.notizen.cryptography.DatabaseKeyManager;
import com.passau.notizen.database.NoteDatabaseService;
import com.passau.notizen.database.dao.NoteTagDAO;
import com.passau.notizen.database.tables.BaseNote;
import com.passau.notizen.database.tables.Tag;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class NoteDTO {
    private String id;
    private String author;
    private LocalDateTime lastChangeTimeStamp;
    private String headline;
    private String body;
    private boolean archived;
    private LocalDateTime dueDate;
    private List<TagDTO> tags = new ArrayList<>();

    private NoteDatabaseService dbService;

    public NoteDTO(String id, String userMail, LocalDateTime lastChangeTimeStamp, String headline, String body, boolean archived, LocalDateTime dueDate, List<TagDTO> tags) {
        this.id = id;
        this.author = userMail;
        this.lastChangeTimeStamp = lastChangeTimeStamp;
        this.headline = headline;
        this.body = body;
        this.archived = archived;
        this.dueDate = dueDate;
        this.tags = tags;
    }

    public NoteDTO(NoteDatabaseService dbService, BaseNote baseNote) throws DatabaseKeyManager.DatabaseKeyException{
        this.dbService = dbService;

        this.id = baseNote.getId().toString();
        this.author = baseNote.getEmail();
        this.lastChangeTimeStamp = baseNote.getLastChange();
        this.headline = baseNote.getHeadline();
        this.body = baseNote.getBody();
        this.archived = baseNote.isArchived();
        this.dueDate = baseNote.getDueDate();

        List<UUID> tagIDs = dbService.getTagIdsByNoteId(baseNote.getId());
        Map<UUID, Tag> allTags = new HashMap<>();
        dbService.getAllTags().forEach(t -> {
            allTags.put(t.getId(), t);
        });
        for(UUID tagID : tagIDs){
            tags.add(new TagDTO(allTags.get(tagID)));
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDateTime getLastChangeTimeStamp() {
        return lastChangeTimeStamp;
    }

    public void setLastChangeTimeStamp(LocalDateTime lastChangeTimeStamp) {
        this.lastChangeTimeStamp = lastChangeTimeStamp;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("author", author);
        json.put("headline", headline);
        json.put("body", body);
        json.put("archived", archived);
        json.put("dueDate", dueDate != null ? dueDate.toString() : null);
        json.put("lastChangeTimeStamp", lastChangeTimeStamp);
        JSONArray tagsJson = new JSONArray();
        for (TagDTO tag : tags) {
            tagsJson.put(tag.toJson());
        }
        json.put("tags", tagsJson);

        return json;
    }
}

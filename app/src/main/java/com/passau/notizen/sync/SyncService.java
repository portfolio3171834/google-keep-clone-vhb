package com.passau.notizen.sync;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.Response;
import com.passau.notizen.R;
import com.passau.notizen.cryptography.DatabaseKeyManager;
import com.passau.notizen.database.NoteDatabaseService;
import com.passau.notizen.database.tables.BaseNote;
import com.passau.notizen.database.tables.NoteTag;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.sync.dto.NoteDTO;
import com.passau.notizen.ui.overview.OverviewActivity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class SyncService extends Service {
    WebServiceController webServiceController = new WebServiceController();
    private NoteDatabaseService dbService;

    /**
     * Konstanten zur Erstellung der Benachrichtungen
     */
    public static final String NOTIFICATION_CHANNEL_ID = "SyncService_Channel_ID";
    public static final String NOTIFICATION_CHANNEL_NAME = "Sync Service Channel";
    public static final String NOTIFICATION_CHANNEL_DESCRIPTION= "Notification channel for SyncService";

    public char[] passkey;
    public static final String PASSKEY = "passkey";
    public static final String NO_NOTIFICATION = "noNotification";

    private static int currentNotificationID = 0;
    private boolean noNotification;

    Map<String, BaseNote> localNotes = new HashMap<>();

    Notification foregroundNotification;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
        foregroundNotification = getNotificationForForegroundService();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            passkey = ((String) intent.getSerializableExtra(PASSKEY)).toCharArray();
            noNotification = intent.getBooleanExtra(NO_NOTIFICATION, false);
            dbService = NoteDatabaseService.getInstance(this, passkey);


            if(!noNotification){
                startForeground(getCurrentNotificationID(), foregroundNotification);
            }
            startSync();
        } catch (DatabaseKeyManager.DatabaseKeyException e) {
            e.printStackTrace();
        }

        // Einlesen der Notes aus der Datenbank
        List<BaseNote> localNoteObjects = dbService.getAllNotes();
        localNoteObjects.forEach(note -> {
            localNotes.put(note.getId().toString(), note);
        });

        return Service.START_NOT_STICKY;
        // return super.onStartCommand(intent, flags, startId);
    }

    /**
     * private Methoden zum Erstellen von Notifications
     */
    private void createNotificationChannel() {
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription(NOTIFICATION_CHANNEL_DESCRIPTION);
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }

    private Notification getNotificationForForegroundService() {
        return new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setContentTitle(getText(R.string.sync_foreground_notification_title))
                .setContentText(getText(R.string.sync_foreground_notification_text))
                .setSmallIcon(R.drawable.ic_outline_sync_24)
                .build();
    }

    /**
     * Inkrementiert einen Zähler, der als Identifikationsnummer der aktuellen Benachrichtigung genutzt wird.
     */
    private int getCurrentNotificationID() {
        currentNotificationID++;
        return currentNotificationID;
    }

    /**
     * Synchronisierungsmethode
     */
    public void startSync() throws DatabaseKeyManager.DatabaseKeyException{
        webServiceController.getAllNotes(this, this, dbService.getUser().getEmailAddress());
    }

    public void processRemoteNoteList(Map<String, LocalDateTime> remoteNotes) throws DatabaseKeyManager.DatabaseKeyException {
        List<NoteDTO> updateRemote = new ArrayList<>();
        List<String> downloadNotes = new ArrayList<>();

        for(String id : remoteNotes.keySet()){
            if(!localNotes.containsKey(id)) {
                downloadNotes.add(id);
            }else if(remoteNotes.get(id).compareTo(localNotes.get(id).getLastChange()) > 0){
                downloadNotes.add(id);
            }else if(remoteNotes.get(id).compareTo(localNotes.get(id).getLastChange()) < 0) {
                updateRemote.add(new NoteDTO(dbService, localNotes.get(id)));
            } // if lastChange is equal no Changes are necessary
        }

        for(String id : localNotes.keySet()){
            if(!remoteNotes.containsKey(id)){
                updateRemote.add(new NoteDTO(dbService, localNotes.get(id)));
            }
        }

        if(updateRemote.size() > 0){
            updateRemoteNotes(updateRemote);
        }
        if(downloadNotes.size() > 0 ){
            downloadRemoteNotes(downloadNotes);
        }

        this.stopSelf();
    }

    public void updateRemoteNotes(List<NoteDTO> notes){
        webServiceController.uploadNotes(this, this, notes);
    }

    public void downloadRemoteNotes(List<String> ids){
        webServiceController.getMissingNotes(this, this, ids);
    }

    public void processRemoteNotes(List<NoteDTO> newNotes){
        newNotes.forEach(note ->{
            if(localNotes.containsKey(note.getId())){
                BaseNote oldNote = localNotes.get(note.getId());
                BaseNote newNote = new BaseNote(note, oldNote.isPinned());

                ArrayList<UUID> oldTagIds = dbService.getTagIdsByNoteId(oldNote.getId());
                ArrayList<UUID> newTags = new ArrayList<>();
                note.getTags().forEach(tagDTO -> {
                    newTags.add(UUID.fromString(tagDTO.getId()));
                });

                dbService.updateBaseNote(newNote);
                for (UUID tag : newTags) {
                    if(!oldTagIds.contains(tag)){
                        NoteTag noteTag = new NoteTag(UUID.fromString(note.getId()), tag);
                        dbService.addTagToBaseNote(noteTag);
                    }
                }

                for (UUID tag : oldTagIds){
                    if(!newTags.contains(tag)){
                        dbService.removeTagFromBaseNote(new NoteTag(newNote.getId(), tag));
                    }
                }
            }else{
                BaseNote newNote = new BaseNote(note);

                try{
                    dbService.addBaseNote(newNote);
                }catch(Exception ignored){
                }

                Map<UUID, Tag> allTags = new HashMap<>();
                dbService.getAllTags().forEach(tag ->{
                    allTags.put(tag.getId(), tag);
                });
                note.getTags().forEach(tagDTO -> {
                    NoteTag noteTag = new NoteTag(UUID.fromString(note.getId()), UUID.fromString(tagDTO.getId()));
                    try {
                        if (allTags.containsKey(UUID.fromString(tagDTO.getId()))) {
                            dbService.addTagToBaseNote(noteTag);
                        } else {
                            dbService.addTag(new Tag(tagDTO));
                            dbService.addTagToBaseNote(noteTag);
                        }
                    }catch(Exception ignored){
                    }
                });
            }
        });

        sendRefreshOverviewBroadcast();
    }

    private void sendRefreshOverviewBroadcast (){
        Intent intent = new Intent (OverviewActivity.REFRESH_VIEW); //put the same message as in the filter you used in the activity when registering the receiver
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}

package com.passau.notizen.sync.dto;

import com.passau.notizen.database.tables.Tag;

import org.json.JSONException;
import org.json.JSONObject;

public class TagDTO {
    private String id;
    private String tagName;

    public TagDTO(String id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public TagDTO(Tag tagEntity){
        this.id = tagEntity.getId().toString();
        this.tagName = tagEntity.getTagName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("tagName", tagName);

        return json;
    }
}

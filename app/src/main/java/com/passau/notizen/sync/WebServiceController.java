package com.passau.notizen.sync;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.passau.notizen.cryptography.DatabaseKeyManager;
import com.passau.notizen.sync.dto.NoteDTO;
import com.passau.notizen.sync.dto.TagDTO;

import org.json.JSONArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class WebServiceController {
    public void getAllNotes(Context context, SyncService syncService, String userMail) throws DatabaseKeyManager.DatabaseKeyException{
        Log.d("Sync", "getAllNotes wurde aufgerufen");

        String url = "https://sharednotes.christopheuskirchen.de/getAllNotes?userMail=" + userMail;
        RequestQueue queue = Volley.newRequestQueue(context);

        Map<String, LocalDateTime> notes = new HashMap<>();

        JsonObjectRequest request = new JsonObjectRequest(url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Sync", response.toString());
                        try {
                            for (Iterator<String> it = response.keys(); it.hasNext(); ) {
                                String s = it.next();
                                notes.put(s, LocalDateTime.parse(response.getString(s)));
                            }

                            syncService.processRemoteNoteList(notes);
                        } catch (JSONException | DatabaseKeyManager.DatabaseKeyException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Sync", error.toString());
                error.printStackTrace();
            }
        });

        Log.d("Sync", request.toString());

        queue.add(request);
        queue.start();
    }

    public List<NoteDTO> getMissingNotes(Context context, SyncService syncService, List<String> noteIDs){
        String url = "https://sharednotes.christopheuskirchen.de/getMissingNotes";
        RequestQueue queue = Volley.newRequestQueue(context);

        List<NoteDTO> noteDTOS = new ArrayList<>();

        try {
            JSONObject json = new JSONObject();
            json.put("ids", new JSONArray(noteIDs));
            Log.d("SyncMissingNotes", json.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url,
                    json,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("SyncMissingNotes", response.toString());
                                JSONArray jsonList = response.getJSONArray("noteDTOs");
                                Log.d("SyncMissingNotes", jsonList.toString());
                                for(int i=0; i < jsonList.length(); i++){
                                    JSONObject jsonNote = jsonList.getJSONObject(i);
                                    String id = jsonNote.getString("id");
                                    String userMail = jsonNote.getString("author");
                                    LocalDateTime lastChangeTimeStamp = LocalDateTime.parse(jsonNote.getString("lastChangeTimeStamp"));
                                    String headline = jsonNote.getString("headline");
                                    String body = jsonNote.getString("body");
                                    String dueDateString = jsonNote.getString("dueDate");
                                    LocalDateTime dueDate = dueDateString.equals("null") ? null : LocalDateTime.parse(dueDateString);
                                    boolean archived = jsonNote.getBoolean("archived");

                                    JSONArray tagsJson = jsonNote.getJSONArray("tags");
                                    List<TagDTO> tags = new ArrayList<>();
                                    for(int j = 0; j < tagsJson.length(); j++){
                                        JSONObject jsonTag = jsonNote.getJSONArray("tags").getJSONObject(j);
                                        tags.add(new TagDTO(jsonTag.getString("id"), jsonTag.getString("tagName")));
                                    }


                                    NoteDTO noteDTO = new NoteDTO(id, userMail, lastChangeTimeStamp, headline, body, archived, dueDate, tags);
                                    noteDTOS.add(noteDTO);
                                }

                                syncService.processRemoteNotes(noteDTOS);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                public String getBodyContentType()
                {
                    return "application/json";
                }
        };
        ;

            queue.add(request);
            queue.start();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return noteDTOS;
    }

    public void uploadNotes(Context context, SyncService syncService, List<NoteDTO> noteDTOs){
        String url = "https://sharednotes.christopheuskirchen.de/uploadNotes";
        RequestQueue queue = Volley.newRequestQueue(context);

        try {
            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            for(NoteDTO noteDTO : noteDTOs){
                jsonArray.put(noteDTO.toJson());
            }
            json.put("noteDTOs", jsonArray);


            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url,
                    json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Success", "Successfully uploaded notes");
                        }
                    }, new Response.ErrorListener(){

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            queue.add(request);
            queue.start();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void deleteNotes(Context context, List<String> noteIDs){
        String url = "https://sharednotes.christopheuskirchen.de/deleteNotes";
        RequestQueue queue = Volley.newRequestQueue(context);

        try{
            JSONObject json = new JSONObject();
            json.put("ids", new Gson().toJson(noteIDs));

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url,
                    new JSONObject(new Gson().toJson(noteIDs)),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Success", "Successfully deleted notes");
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            queue.add(request);
            queue.start();
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}

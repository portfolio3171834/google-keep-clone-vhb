package com.passau.notizen.sync;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.passau.notizen.database.NoteDatabaseService;

public class PeriodicSyncService extends Service {
    Handler handler = new Handler();
    public static final int SYNC_DELAY = 120000;
    public static final String PASSKEY = "passkey";
    char[] passkey;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.post(periodicSync);
        passkey = ((String) intent.getSerializableExtra(PASSKEY)).toCharArray();
        handler.post(periodicSync);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(periodicSync);
    }

    /**
     * Sync periodische Ausführung
     */
    private Runnable periodicSync = new Runnable(){
        @Override
        public void run() {
            Intent intent = new Intent(PeriodicSyncService.this, SyncService.class);
            intent.putExtra(SyncService.PASSKEY, new String(passkey));
            intent.putExtra(SyncService.NO_NOTIFICATION, true);
            startService(intent);
            handler.postDelayed(this, SYNC_DELAY);
        }
    };
}

package com.passau.notizen.cryptography;

/**
 * This class is used to store the database key, which is generated with
 * a initialization vector, a private key and a salt value
 **/

public class Storable {

    private String iv, key, salt;

    public Storable(String iv, String key, String salt) {
        this.iv = iv;
        this.key = key;
        this.salt = salt;
    }


    public String getIv() {
        return iv;
    }

    public String getKey() {
        return key;
    }

    public String getSalt() {
        return salt;
    }
}

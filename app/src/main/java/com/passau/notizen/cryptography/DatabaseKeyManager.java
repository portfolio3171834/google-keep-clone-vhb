package com.passau.notizen.cryptography;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class for encrypting the database key by user passcode
 */
public class DatabaseKeyManager {

    private static DatabaseKeyManager keyManager;

    private final Context context;
    private final int ITERATION_COUNT = 65536;
    private final int KEY_LENGTH = 256;
    private final String CYPHER_TRANSFORMATION = "AES/GCM/NoPadding";
    private byte[] rawByteKey;
    private char[] dbCharKey;


    private DatabaseKeyManager(Context context) {
        this.context = context;
    }

    public static DatabaseKeyManager getInstance(Context context) {
        if (keyManager == null) {
            keyManager = new DatabaseKeyManager(context);
        }
        return keyManager;
    }


    // return the database key suitable for using with room
    public char[] getCharKey(char[] passCode, Context context) throws DatabaseKeyException {
        if (dbCharKey == null) {
            initKey(passCode, context);
        }
        if (dbCharKey == null) {
//            throw new DatabaseKeyException("Failed to decrypt database key");
            // TODO: 26.03.2022 remove?
        }
        return dbCharKey;
    }


    private void initKey(char[] passCode, Context context) throws DatabaseKeyException {
        Storable storable = getStorable(context);

        if (storable == null) {
            createNewKey();
            persistRawKey(passCode);
        } else {
            rawByteKey = getRawByteKey(passCode, storable);
            dbCharKey = byteArrayToHex(rawByteKey);
        }
    }


    private void persistRawKey(char[] userPasscode) {
        Storable storable = toStorable(rawByteKey, userPasscode);
        saveToPrefs(context, storable);
    }


    private void createNewKey() throws DatabaseKeyException {
        rawByteKey = generateRandomKey();
        dbCharKey = byteArrayToHex(rawByteKey);
    }


    /**
     * generates an random 32 byte key, which will be converted to a char array
     * to be used by SQLCipher
     *
     * @return random 32 byte Array
     */
    private byte[] generateRandomKey() {
        byte[] bytes = new byte[32];
        try {
            SecureRandom.getInstanceStrong().nextBytes(bytes);
        } catch (NoSuchAlgorithmException exception) {
            Log.d("KEY_GENERATION", exception.getMessage());
        }
        return bytes;
    }


    /**
     * encodes the given byte array and turns it to a char array
     *
     * @return hex encoded byte array -> raw db-key
     */
    private char[] byteArrayToHex(byte[] thisToHex) {
        final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();
        StringBuilder result = new StringBuilder();

        for (byte aByte : thisToHex) {
            //  int octet = ByteBuffer.wrap(aByte).getInt();
            int firstIndex = (aByte & 240) >>> 4;
            int secondIndex = aByte & 15;
            result.append(HEX_CHARS[firstIndex]);
            result.append(HEX_CHARS[secondIndex]);
        }
        return result.toString().toCharArray();
    }


    private void saveToPrefs(Context context, Storable storable) {
        String serialized = new Gson().toJson(storable);
        SharedPreferences preferences = context.getSharedPreferences("database", Context.MODE_PRIVATE);
        preferences.edit().putString("key", serialized).apply();
    }


    private Storable getStorable(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("database", Context.MODE_PRIVATE);
        String serialized = preferences.getString("key", null);
        if (serialized == null || serialized.isEmpty()) {
            return null;
        }
        try {
            return new Gson().fromJson(serialized, (new TypeToken<Storable>() {
            }).getType());
        } catch (JsonSyntaxException exception) {
            return null;
        }
    }


    // decrypt the Storable instance using the passcode
    private byte[] getRawByteKey(char[] passCode, Storable storable) throws DatabaseKeyException {
        byte[] aesWrappedKey = Base64.decode(storable.getKey(), Base64.DEFAULT);
        byte[] iv = Base64.decode(storable.getIv(), Base64.DEFAULT);
        byte[] salt = Base64.decode(storable.getSalt(), Base64.DEFAULT);

        SecretKey secret = generateSecretKey(passCode, salt);

        // aesWrappedKey gets decrypted
        try {
            Cipher cipher = Cipher.getInstance(CYPHER_TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
            return cipher.doFinal(aesWrappedKey);
        } catch (Exception exception) {
            throw new DatabaseKeyException("Decrypt of database key failed");
        }
    }


    private Storable toStorable(byte[] rawDbKey, char[] userPasscode) {
        byte[] salt = new byte[8];
        try {
            SecureRandom.getInstanceStrong().nextBytes(salt);
            SecretKey secret = generateSecretKey(userPasscode, salt);

            // database key is encrypted with PBD
            Cipher cipher = Cipher.getInstance(CYPHER_TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secret);
            byte[] iv = cipher.getIV();
            byte[] cipherText = cipher.doFinal(rawDbKey);

            // return the initialization vector and cipher text to be stored to disk
            return new Storable(
                    Base64.encodeToString(iv, Base64.DEFAULT),
                    Base64.encodeToString(cipherText, Base64.DEFAULT),
                    Base64.encodeToString(salt, Base64.DEFAULT));

        } catch (Exception exception) {
            Log.d("SALT_GENERATION", exception.getMessage());
            return null;
        }
    }


    private SecretKey generateSecretKey(char[] passCode, byte[] salt) {

        try {
            //factory defines which method is used to generate the key
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            // spec defines the key length and the number of iterations during generation
            KeySpec spec = new PBEKeySpec(passCode, salt, ITERATION_COUNT, KEY_LENGTH);
            SecretKey tmp = factory.generateSecret(spec);
            return new SecretKeySpec(tmp.getEncoded(), "AES");
        } catch (Exception exception) {
            Log.d("SECRET_KEY_GENERATION", exception.getMessage());
            return null;
        }
    }

    public static class DatabaseKeyException extends Throwable {
        public DatabaseKeyException(String failed_to_decrypt_database_key) {
        }
    }
}

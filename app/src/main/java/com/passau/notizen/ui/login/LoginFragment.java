package com.passau.notizen.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.passau.notizen.R;

public class LoginFragment extends Fragment {

	public static final int MAX_LOGIN_TRIES = 5;
	public static final int MAX_LOGIN_TRIES_WARNING_THRESHOLD = 3;

	private int loginTriesCounter = MAX_LOGIN_TRIES;

	Button loginButton, registerButton;
	EditText etEmail, etPassword;
	String email, password, personEmail;


	//Google Sign in Button
	SignInButton googleSignInButton;

	ActivityResultLauncher<Intent> activityResultLauncher;
	TextView message;

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_login, container, false);

		etEmail = view.findViewById(R.id.login_email_edittext);
		etPassword = view.findViewById(R.id.login_password_edittext);
		loginButton = view.findViewById(R.id.button_login);
		registerButton = view.findViewById(R.id.button_login_sign_up);
		message = view.findViewById(R.id.login_message_textview);


		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				message.setText("");

				email = etEmail.getText().toString().trim();
				password = etPassword.getText().toString().trim();


				String wrongLogin = getResources().getString(R.string.wrong_login_details);

				if (email.isEmpty()) return;
				loginTriesCounter--;

				if (loginTriesCounter == 0) {
					new CountDownTimer(30000, 1000) {

						public void onTick(long millisUntilFinished) {
							String text = getResources().getString(R.string.seconds_remaining) + " " + millisUntilFinished / 1000;
							message.setText(text);
						}

						public void onFinish() {
							lockLoginButton();
							enableLoginButton();
							String text = getResources().getString(R.string.try_again);
							message.setText(text);
							loginTriesCounter = MAX_LOGIN_TRIES;
						}
					}.start();
					lockLoginButton();
				} else if (loginTriesCounter <= MAX_LOGIN_TRIES_WARNING_THRESHOLD && loginTriesCounter > 0) {
					String text = loginTriesCounter + " " + getResources().getString(R.string.tries_left);
					message.setText(text);
				} else {
					message.setText(wrongLogin);
				}

				// ---------------
				if (getExistingUserType() == LoginPreferenceKeys.USER_TYPE_GOOGLE) {
					String text = getResources().getString(R.string.a_google_user_already_exists);
					Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show();
				} else if (existsAccountWithMail(email)) {
					if (requireActivity() instanceof LoginActivity) {
						message.setText("");
						((LoginActivity) requireActivity()).dataBaseCall(email, password);
					}
				}

			}
		});

		registerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changeFragment(new LoginRegisterFragment());
			}
		});

		initResultLauncher();

		//opens google sign in window
		googleSignInButton = view.findViewById(R.id.sign_in_button_google);
		googleSignInButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// check existing user
				int userType = getExistingUserType();

				if (userType == LoginPreferenceKeys.USER_TYPE_LOCAL) {
					// TODO: 26.03.2022 error
					String text = getResources().getString(R.string.a_local_user_already_exists);
					Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show();

				} else {
					GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
							.requestEmail()
							.build();

					GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso);

					activityResultLauncher.launch(mGoogleSignInClient.getSignInIntent());
				}
			}
		});

		return view;
	}

	private int getExistingUserType() {
		SharedPreferences prefs = requireActivity().getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
		return prefs.getInt(LoginPreferenceKeys.KEY_USER_TYPE, LoginPreferenceKeys.DEFAULT_ERROR_RETURN);
	}

	/**
	 * creates a SharedPreference with the value, if it's the first sign in or not
	 * value is 1 then changes screen to enterPinFragment if already sign in an Pin is already exists
	 * value is 0 then changes screen to assignPinFragment and user has to assign a Pin for the Database decryption
	 *
	 * @author Marco Wenger
	 */

	private void initResultLauncher() {
		// You can do the assignment inside onAttach or onCreate, i.e, before the activity is displayed
		activityResultLauncher = registerForActivityResult(
				new ActivityResultContracts.StartActivityForResult(),
				result -> {
					if (result.getResultCode() == Activity.RESULT_OK) {

						SharedPreferences prefs = requireActivity().getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
						boolean i = prefs.getBoolean(LoginPreferenceKeys.KEY_PIN_ASSIGNED, false);

						GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(requireActivity());
						if (acct != null) {
							personEmail = acct.getEmail();
						}
						if (sharedPrefGmailLogin(personEmail)) {
							String text = getResources().getString(R.string.sign_in_success);
							Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show();
							if (i) {
								changeFragment(new EnterPinFragment());
							} else {
								changeFragment(new AssignPinFragment());
							}
						} else {
							String text = getResources().getString(R.string.a_local_user_already_exists);
							message.setText(text);
						}
					} else {
						String text = getResources().getString(R.string.google_login_failed);
						Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show();
					}

				});
	}

	/**
	 * Changes the Fragments.
	 *
	 * @author Marco Wenger
	 */

	private void changeFragment(Fragment newFragment) {
		FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.login_fragment_view, newFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}

	/**
	 * login check for the user.
	 *
	 * @author Marco Wenger
	 */
	public void login() {
		email = etEmail.getText().toString().trim();
		password = etPassword.getText().toString().trim();

		loginSecurity();
		if (existsAccountWithMail(email)) {
			if (requireActivity() instanceof LoginActivity) {
				message.setText("");
				((LoginActivity) requireActivity()).dataBaseCall(email, password);
			}
		}
	}


	/**
	 * security for spam passing passwords
	 */

	public void loginSecurity() {
		loginTriesCounter--;

		if (loginTriesCounter == 0) {
			new CountDownTimer(30000, 1000) {

				public void onTick(long millisUntilFinished) {
					String text = getResources().getString(R.string.seconds_remaining) + " " + millisUntilFinished / 1000;
					message.setText(text);
				}

				public void onFinish() {
					lockLoginButton();
					enableLoginButton();
					String text = getResources().getString(R.string.try_again);
					message.setText(text);
					loginTriesCounter = MAX_LOGIN_TRIES;
				}
			}.start();
			lockLoginButton();
		} else if (loginTriesCounter == MAX_LOGIN_TRIES - 1) {
			String text = getResources().getString(R.string.wrong_login_details);
			message.setText(text);
		} else if (loginTriesCounter <= MAX_LOGIN_TRIES_WARNING_THRESHOLD && loginTriesCounter > 0) {
			String text = loginTriesCounter + " " + getResources().getString(R.string.tries_left);
			message.setText(text);
		} else {
			message.setText("");
		}

	}

	private void lockLoginButton() {
		loginButton.setEnabled(false);
		loginButton.setTextColor(ContextCompat.getColor(requireContext(), R.color.grey_700));
	}

	private void enableLoginButton() {
		loginButton.setEnabled(true);
		loginButton.setTextColor(ContextCompat.getColor(requireContext(), R.color.primary_500));
	}

	/**
	 * checks whether an email already exists in SharedPref
	 *
	 * @author Marco Wenger
	 */
	private boolean existsAccountWithMail(String email) {

		SharedPreferences prefs = requireActivity().getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
		String i = prefs.getString(LoginPreferenceKeys.KEY_EMAIL, null);
		if (i == null) {
			message.setText(getResources().getString(R.string.register_first));
			return false;
		} else return i.equals(email);
	}

	private boolean sharedPrefGmailLogin(String email) {
		SharedPreferences prefs = requireActivity().getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
		String i = prefs.getString(LoginPreferenceKeys.KEY_EMAIL, null);
		return i == null || i.equals(email);
	}

}


package com.passau.notizen.ui.login;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.passau.notizen.R;

public class EnterPinFragment extends Fragment {

	Button buttonCancel, buttonEnter;
	EditText pinEditText;
	TextView pinMsgTextView;
	String personEmail, pin;

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
	}

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_enter_pin, container, false);
		pinMsgTextView = view.findViewById(R.id.enter_pin_message_textview);
		pinEditText = view.findViewById(R.id.enter_pin_edittext);
		buttonCancel = view.findViewById(R.id.button_sign_in_cancel);
		buttonEnter = view.findViewById(R.id.button_enter_pin);

		/** Button go back one screen*/
		buttonCancel.setOnClickListener(v -> popBackStack());
		/** Button for entering Pin*/
		buttonEnter.setOnClickListener(v -> checkPinForUnlock());

		//get google Email
		GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(requireActivity());
		if (acct != null) {
			personEmail = acct.getEmail();
		}

		return view;
	}

	/**
	 * checks the pin for Unlocking the db
	 *
	 * @author Marco Wenger
	 */
	private void checkPinForUnlock() {
		String pin = pinEditText.getText().toString().trim();
		String text = getResources().getString(R.string.wrong_pin);
		/** checks if the entered Pin is correct */
		if (!pin.isEmpty() && requireActivity() instanceof LoginActivity) {
			boolean success = ((LoginActivity) requireActivity()).dataBaseCall(personEmail, pin);
			if (!success) {
				pinMsgTextView.setText(text);
			}
		}
	}

	/**
	 * Changes the Fragments to previous.
	 *
	 * @author Marco Wenger
	 */
	private void popBackStack() {
		requireActivity().getSupportFragmentManager().popBackStack();
	}
}
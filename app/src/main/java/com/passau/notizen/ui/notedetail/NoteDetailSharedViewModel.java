package com.passau.notizen.ui.notedetail;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.passau.notizen.data.AttachmentThumbnail;
import com.passau.notizen.data.Note;
import com.passau.notizen.database.tables.MediaObject;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.ui.util.DataSharing;

import org.apache.commons.io.FilenameUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

/**
 * Shared ViewModel for the NoteEdit Screens
 * Owner must always be {@link com.passau.notizen.ui.notedetail.NoteDetailActivity}
 *
 * @author Manuel Thöne
 */
public class NoteDetailSharedViewModel extends AndroidViewModel {
	private int position = DataSharing.NO_POSITION;
	private MutableLiveData<Note> note;
	private MutableLiveData<Note> noteToSave;
	private MutableLiveData<String> email;
	private MutableLiveData<String> bodyText;
	private MutableLiveData<String> titleText;
	private MutableLiveData<Boolean> isPinned;
	private MutableLiveData<Boolean> isArchived;
	private MutableLiveData<ArrayList<AttachmentThumbnail>> attachmentThumbnails;
	private MutableLiveData<ArrayList<String>> newCreatedImages;

	private MutableLiveData<ArrayList<Tag>> selectedTags;
	private MutableLiveData<ArrayList<Tag>> allAvailableTags;

	private MutableLiveData<String> lastCreatedFile = null;
	private MutableLiveData<Boolean> isEdited = new MutableLiveData<>(false);
	private volatile boolean runAttachmentLoading = true;

	public NoteDetailSharedViewModel(@NonNull Application application) {
		super(application);
	}

	@Override
	protected void onCleared() {
		super.onCleared();
		runAttachmentLoading = false;
	}

	/*=====================
	========= GETTER ======
	* ====================*/

	/**
	 * Returns the position in the Manager list of the current note
	 *
	 * @return DataSharing.NO_POSITION if new; otherwise position
	 */
	public int getPosition() {
		return this.position;
	}

	public LiveData<Note> getNoteToSave() {
		if (this.noteToSave == null) {
			this.noteToSave = new MutableLiveData<>();
		}
		return this.noteToSave;
	}

	public boolean generateNoteToSave() {
		this.noteToSave = null;

		Note noteToSave = new Note();
		UUID id;
		String email;

		if (this.note == null || this.note.getValue() == null) {
			// Case: new Note
			id = UUID.randomUUID();
			email = getEmail().getValue();

		} else {
			// Case: edited Note
			id = this.note.getValue().getId();
			email = this.note.getValue().getEmail();
		}
		noteToSave.setId(id).setEmail(email);

		// headText
		String headText = getTitleText().getValue().trim();
		noteToSave.setHeadline(headText);

		// bodyText
		String bodyText = getBodyText().getValue().trim();

		if (bodyText.isEmpty() && headText.isEmpty()) {
			return false;
		} else if (bodyText.isEmpty()) {
			bodyText = "";
		}
		noteToSave.setBody(bodyText);

		// isArchived
		Boolean isArchived = isArchived().getValue();
		if (isArchived == null) isArchived = false;
		noteToSave.setArchived(isArchived);

		// isPinned
		Boolean isPinned = isPinned().getValue();
		if (isPinned == null) isPinned = false;
		noteToSave.setPinned(isPinned);

		// lastChange
		noteToSave.setLastChange(LocalDateTime.now());

		// dueDate
		// TODO: 20.03.2022 dueDate

		// mediaObjects
		noteToSave.setMediaObjects(getMediaObjectsToSave(id));

		// tags
		noteToSave.setTags(getTagsToSave());

		this.noteToSave = new MutableLiveData<>();
		this.noteToSave.setValue(noteToSave);

		return true;
	}

	/**
	 * Returns all attached and/or created media objects
	 *
	 * @param noteId The id of the note they belong to
	 * @return ArrayList of MediaObjects
	 * @author Manuel Thöne
	 */
	public ArrayList<MediaObject> getMediaObjectsToSave(@NonNull UUID noteId) {
		// TODO: 20.03.2022 Add audio
		ArrayList<MediaObject> mediaObjects = new ArrayList<>();
		// If available: add existing media object
		if (this.note != null && this.note.getValue() != null && this.note.getValue().getMediaObjects() != null) {
			mediaObjects.addAll(this.note.getValue().getMediaObjects());
		}
		// Add new created images
		LiveData<ArrayList<String>> images = getNewCreatedImages();
		if (images.getValue() != null) {
			for (String fileName : images.getValue()) {
				String suffix = FilenameUtils.getExtension(fileName);
				MediaObject mediaObject;
				if (AttachmentManager.AttachmentImageFormat.containsSuffix(suffix)) {
					mediaObject = new MediaObject(UUID.randomUUID(), fileName, MediaObject.MediaType.IMAGE, noteId);
					mediaObjects.add(mediaObject);
				}
			}
		}
		return mediaObjects;
	}

	/**
	 * Returns all attached and/or created tags
	 *
	 * @return ArrayList of tags
	 * @author Manuel Thöne
	 */
	public ArrayList<Tag> getTagsToSave() {
		ArrayList<Tag> tags = new ArrayList<>();

		if (selectedTags != null && selectedTags.getValue() != null) {
			tags.addAll(selectedTags.getValue());
		}
		return tags;
	}

	public LiveData<Note> getNote() {
		if (this.note == null) {
			this.note = new MutableLiveData<>();
		}
		return this.note;
	}

	public LiveData<String> getBodyText() {
		if (this.bodyText == null) {
			this.bodyText = new MutableLiveData<>();
		}
		if (this.bodyText.getValue() == null) {
			this.bodyText.setValue("");
		}
		return this.bodyText;
	}

	public LiveData<String> getTitleText() {
		if (this.titleText == null) {
			this.titleText = new MutableLiveData<>();
		}
		if (this.titleText.getValue() == null) {
			this.titleText.setValue("");
		}
		return this.titleText;
	}

	public LiveData<String> getEmail() {
		if (this.email == null) {
			this.email = new MutableLiveData<>();
		}
		return this.email;
	}

	public LiveData<ArrayList<String>> getNewCreatedImages() {
		if (this.newCreatedImages == null) {
			this.newCreatedImages = new MutableLiveData<>();
		}
		if (this.newCreatedImages.getValue() == null) {
			this.newCreatedImages.setValue(new ArrayList<>());
		}
		return this.newCreatedImages;
	}

	public void toggleSelectedTag(Tag tag) {
		if (this.selectedTags == null) {
			this.selectedTags = new MutableLiveData<>();
		}
		if (this.selectedTags.getValue() == null) {
			this.selectedTags.setValue(new ArrayList<>());
		}
		ArrayList<Tag> currentSelectedTags = selectedTags.getValue();
		if (currentSelectedTags.contains(tag)) {
			currentSelectedTags.remove(tag);
		} else {
			currentSelectedTags.add(tag);
		}
		selectedTags.setValue(currentSelectedTags);
	}

	@NonNull
	public LiveData<ArrayList<Tag>> getSelectedTags() {
		if (this.selectedTags == null) {
			this.selectedTags = new MutableLiveData<>();
		}
		if (this.selectedTags.getValue() == null) {
			this.selectedTags.setValue(new ArrayList<>());
		}
		ArrayList<Tag> list = selectedTags.getValue();
		Collections.sort(list);
		selectedTags.setValue(list);
		return this.selectedTags;
	}

	public LiveData<ArrayList<Tag>> getAllAvailableTags() {
		if (this.allAvailableTags == null) {
			this.allAvailableTags = new MutableLiveData<>();
		}
		if (this.allAvailableTags.getValue() == null) {
			this.allAvailableTags.setValue(new ArrayList<>());
		}
		ArrayList<Tag> list = allAvailableTags.getValue();
		Collections.sort(list);
		allAvailableTags.setValue(list);
		return this.allAvailableTags;
	}

	public LiveData<Boolean> isPinned() {
		if (this.isPinned == null) {
			this.isPinned = new MutableLiveData<>(false);
		}
		return this.isPinned;
	}

	@NonNull
	public LiveData<Boolean> isArchived() {
		if (this.isArchived == null) {
			this.isArchived = new MutableLiveData<>(false);
		}
		return this.isArchived;
	}

	public LiveData<Boolean> isEdited() {
		return this.isEdited;
	}

	public LiveData<ArrayList<AttachmentThumbnail>> getAttachmentThumbnails() {
		if (attachmentThumbnails == null) {
			attachmentThumbnails = new MutableLiveData<>();
		}
		return attachmentThumbnails;
	}

	public void loadAttachmentThumbnails(AttachmentObserverListener listener) {
		if (attachmentThumbnails == null) {
			attachmentThumbnails = new MutableLiveData<>();
		}
		if (attachmentThumbnails.getValue() == null || attachmentThumbnails.getValue().size() == 0) {
			loadAttachmentThumbnailsFromExistingNote(listener);
		}
	}

	public LiveData<String> getLastCreatedFile() {
		return lastCreatedFile;
	}

	/**
	 * Returns a Thumbnail as Bitmap. Use only after successful camera intent!
	 *
	 * @param width  of thumbnail
	 * @param height of thumbnail
	 * @return null if not found or image as thumbnail
	 */
	public Bitmap getCapturedImageAsThumbnail(int width, int height) {
		if (lastCreatedFile.getValue() != null) {
			return AttachmentManager.getThumbnailByNameFromInternalStorage(getApplication().getApplicationContext(), lastCreatedFile.getValue(), width, height);
		} else return null;
	}

	/*=====================
	========= SETTER ======
	* ====================*/

	public void setPosition(int position) {
		this.position = position;
	}

	public void setNote(@NonNull Note note) {
		if (this.note == null) {
			this.note = new MutableLiveData<>();
		}
		this.note.setValue(note);
	}

	public void setEmail(@NonNull String email) {
		if (this.email == null) {
			this.email = new MutableLiveData<>();
		}
		this.email.setValue(email);
	}

	public void setBodyText(String bodyText) {
		if (this.bodyText == null) {
			this.bodyText = new MutableLiveData<>();
		}
		this.bodyText.setValue(bodyText);
	}

	public void setTitleText(String titleText) {
		if (this.titleText == null) {
			this.titleText = new MutableLiveData<>();
		}
		this.titleText.setValue(titleText);
	}

	public void setIsPinned(boolean isPinned) {
		if (this.isPinned == null) {
			this.isPinned = new MutableLiveData<>();
		}
		this.isPinned.setValue(isPinned);
	}

	public void setIsArchived(boolean isArchived) {
		if (this.isArchived == null) {
			this.isArchived = new MutableLiveData<>();
		}
		this.isArchived.setValue(isArchived);
	}

	public void setLastCreatedFile(String fileName) {
		if (lastCreatedFile == null) {
			lastCreatedFile = new MutableLiveData<>();
		}
		lastCreatedFile.setValue(fileName);
	}

	public void setIsEdited(boolean isEdited) {
		this.isEdited.setValue(isEdited);
	}

	public void addNewCreatedImage(String s) {
		if (newCreatedImages == null) {
			newCreatedImages = new MutableLiveData<>();
		}
		if (newCreatedImages.getValue() == null) {
			newCreatedImages.setValue(new ArrayList<>());
		}
		setIsEdited(true);
		newCreatedImages.getValue().add(s);
	}

	/**
	 * Only to be used in the Activity to pass data
	 */
	public void setAllAvailableTags(ArrayList<Tag> tags) {
		if (this.allAvailableTags == null) {
			this.allAvailableTags = new MutableLiveData<>();
		}
		this.allAvailableTags.setValue(tags);
	}

	public void setAllSelectedTags(ArrayList<Tag> tags) {
		if (this.selectedTags == null) {
			this.selectedTags = new MutableLiveData<>();
		}
		this.selectedTags.setValue(tags);
	}

	public void addNewSelectedTag(Tag tag) {
		if (selectedTags == null) {
			selectedTags = new MutableLiveData<>();
		}
		if (selectedTags.getValue() == null) {
			selectedTags.setValue(new ArrayList<>());
		}
		setIsEdited(true);
		ArrayList<Tag> currentNewCreated = selectedTags.getValue();
		if (!currentNewCreated.contains(tag)) {
			currentNewCreated.add(tag);
		}
		selectedTags.setValue(currentNewCreated);
	}

	public void addToAllAvailableTags(Tag newTag) {
		if (allAvailableTags == null) {
			allAvailableTags = new MutableLiveData<>();
		}
		if (allAvailableTags.getValue() == null) {
			allAvailableTags.setValue(new ArrayList<>());
		}
		setIsEdited(true);
		ArrayList<Tag> currentAvailableTags = allAvailableTags.getValue();
		if (!currentAvailableTags.contains(newTag)) {
			currentAvailableTags.add(newTag);
		}
		allAvailableTags.setValue(currentAvailableTags);
	}

	public boolean allAvailableTagsContain(Tag tag) {
		if (allAvailableTags == null || allAvailableTags.getValue() == null) return false;
		return (allAvailableTags.getValue().contains(tag));
	}

	public void setAttachmentThumbnails(@NonNull ArrayList<AttachmentThumbnail> newThumbnails) {
		if (this.attachmentThumbnails == null) {
			this.attachmentThumbnails = new MutableLiveData<>();
		}
		this.attachmentThumbnails.setValue(newThumbnails);
	}

	public void addAttachmentThumbnail(@NonNull AttachmentThumbnail thumbnail) {
		if (attachmentThumbnails == null) {
			attachmentThumbnails = new MutableLiveData<>();
		}
		ArrayList<AttachmentThumbnail> currentThumbnails = attachmentThumbnails.getValue();
		if (currentThumbnails == null) {
			currentThumbnails = new ArrayList<>();
		}
		currentThumbnails.add(thumbnail);
		attachmentThumbnails.setValue(currentThumbnails);
	}

	private boolean removeFromNewCreatedImages(String s) {
		if (this.newCreatedImages == null || this.newCreatedImages.getValue() == null) {
			return false;
		}
		return this.newCreatedImages.getValue().removeIf(listString -> listString.equals(s));
	}

	/**
	 * Deletes the image from the internal storage and removes it from the attachment list
	 *
	 * @param listener to inform for successful deletion
	 * @return true if and only if image was removed from list and deleted from storage; otherwise false
	 * @author Manuel Thöne
	 */
	public boolean removeImageAttachment(String fileName, AttachmentObserverListener listener) {
		boolean deleted = AttachmentManager.deleteImageByName(getApplication().getApplicationContext(), fileName);
		boolean removed;

		if (!deleted) return false;

		if (removeFromNewCreatedImages(fileName)) {
			removed = true;
			Log.d("Notiz", "in SharedViewModel -> removeFromNewCreatedImages() -> '" + fileName + "'");
		} else if (getNote().getValue() != null && getNote().getValue().getMediaObjects() != null) {
			// Delete if in existing images
			removed = getNote().getValue().getMediaObjects().removeIf(mediaObject -> mediaObject.getLocalPath().equals(fileName));
			Log.d("Notiz", "in SharedViewModel -> removed from existing images -> '" + fileName + "'");
		} else return false;

		// remove from thumbnails
		ArrayList<AttachmentThumbnail> currentThumbnails = getAttachmentThumbnails().getValue();
		if (currentThumbnails != null) {
			removed = currentThumbnails.removeIf(thumb -> fileName.equals(thumb.getFileName()));
			attachmentThumbnails.postValue(currentThumbnails);
		}

		if (removed && listener != null) {
			listener.onAttachmentViewFileDelete();
		}
		return removed;
	}

	/**
	 * Removes all images that were newly created and not loaded by opening an existing note
	 *
	 * @return true if no images were created or all images were deleted; false otherwise
	 * @author Manuel Thöne
	 */
	public boolean removeAllNewCreatedImageAttachments() {
		if (newCreatedImages == null || newCreatedImages.getValue() == null || newCreatedImages.getValue().size() == 0)
			return true;
		boolean deleted = true;
		ArrayList<String> images = new ArrayList<>(newCreatedImages.getValue());    // Use copy to not get a ConcurrentModificationException!
		for (String image : images) {
			if (!removeImageAttachment(image, null)) {
				deleted = false;
			}
		}
		return deleted;
	}

	public void toggleIsPinned() {
		if (isPinned == null) {
			isPinned = new MutableLiveData<>();
		}
		if (isPinned.getValue() == null || !isPinned.getValue()) {
			isPinned.setValue(true);
		} else {
			isPinned.postValue(false);
		}
		setIsEdited(true);
	}

	public void toggleIsArchived() {
		if (isArchived == null) {
			isArchived = new MutableLiveData<>();
		}
		if (isArchived.getValue() == null || !isArchived.getValue()) {
			isArchived.setValue(true);
		} else {
			isArchived.postValue(false);
		}
		setIsEdited(true);
	}

	/**
	 * Loads the thumbnails asynchronously and informs a listener for every loaded thumbnail
	 *
	 * @author Manuel Thöne
	 */
	public void loadAttachmentThumbnailsFromExistingNote(AttachmentObserverListener listener) {
		if (this.note == null || this.note.getValue() == null) return;
		Note note = this.note.getValue();
		if (note.getMediaObjects() == null || note.getMediaObjects().size() == 0) return;

		ArrayList<MediaObject> currentMediaObjects = note.getMediaObjects();

		Thread t = new Thread(() -> {
			for (MediaObject mediaObject : currentMediaObjects) {
				// Stop thread if loading exceeds lifecycle of ViewModel
				if (!runAttachmentLoading) break;
				if (mediaObject.getMediaType() == MediaObject.MediaType.IMAGE) {
					Context context = getApplication().getApplicationContext();
					Bitmap bitmap = AttachmentManager.getImageByNameFromInternalStorage(context, mediaObject.getLocalPath());
					if (bitmap != null) {
						AttachmentThumbnail thumbnail = new AttachmentThumbnail(mediaObject.getLocalPath(), bitmap);
						listener.onExistingNoteAttachmentThumbnailLoaded(thumbnail);
					}
				}
			}
		});
		t.start();
	}

	/**
	 * Interface for custom communication between fragment and ViewModel
	 *
	 * @author Manuel Thöne
	 */
	public interface AttachmentObserverListener {
		void onAttachmentViewFileDelete();

		void onExistingNoteAttachmentThumbnailLoaded(AttachmentThumbnail thumbnail);
	}

}

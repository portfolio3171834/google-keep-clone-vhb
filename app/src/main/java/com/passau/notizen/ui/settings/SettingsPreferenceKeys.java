package com.passau.notizen.ui.settings;

public abstract class SettingsPreferenceKeys {
	public static final String NAME_SETTINGS_PREFS = "settings";

	public static final String KEY_ALLOW_SCREENSHOTS = "allow_screenshots";
	public static final String KEY_NOTE_ADAPTER_PREVIEW = "note_adapter_preview";
	public static final String KEY_AUTO_SYNCHRONIZATION = "auto_synchronization";
}

package com.passau.notizen.ui.notedetail;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import androidx.annotation.NonNull;
import androidx.exifinterface.media.ExifInterface;

import com.passau.notizen.R;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The AttachmentFileManager Class is a utility class for the NoteDetailEditFragment.
 * It provides methods to create attachment files in the private directory and retrieve images from there.
 *
 * @author Manuel Thöne
 */
public class AttachmentManager {

	public final static String PICTURE_PATH = "pictures";
	public final static String AUDIO_PATH = "audio";

	/**
	 * Creates a File in the internal storage depending on the suffix.
	 * The Name will be generated based on a timestamp
	 *
	 * @param context of fragment or activity
	 * @param suffix  See constants of {@link AttachmentManager}
	 * @return the created file
	 */
	public static File createAttachmentFile(@NonNull Context context, String suffix) throws IOException, SuffixNotSupportedException {
		return createAttachmentFile(context, null, suffix);
	}

	/**
	 * Creates a File in the internal storage depending on the suffix.
	 * If no name is provided it will be generated based on a timestamp
	 *
	 * @param context  of fragment or activity
	 * @param suffix   See enums of {@link AttachmentManager.AttachmentImageFormat} and {@link AttachmentManager.AttachmentAudioFormat}
	 * @param fileName the name of the file
	 * @return the created file
	 */
	public static File createAttachmentFile(@NonNull Context context, String fileName, @NonNull String suffix) throws IOException, SuffixNotSupportedException {
		if (fileName == null || fileName.isEmpty()) {

			LocalDateTime localDate = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
			String imageName = localDate.format(formatter);

			String subfolder;
			if (AttachmentImageFormat.containsSuffix(suffix)) {
				subfolder = PICTURE_PATH;
			} else if (AttachmentAudioFormat.containsSuffix(suffix)) {
				subfolder = AUDIO_PATH;
			} else {
				throw new SuffixNotSupportedException("The provided suffix is empty or not supported");
			}
			File storageDir = new File(context.getFilesDir(), subfolder);
			if (!storageDir.exists()) {
				if (!storageDir.mkdir()) {
					throw new IOException("Directory could not be created");
				}
			}

			return File.createTempFile(
					imageName,  /* prefix */
					"." + suffix,     /* suffix */
					storageDir  /* directory */
			);

		} else {
			return createAttachmentFile(context, suffix);
		}
	}

	/**
	 * Reads an image from the internal app storage and rotates it if exif data is available.<br>
	 * Exif rotation from <a href="https://stackoverflow.com/a/14066265/7831593">Stackoverflow</a>
	 *
	 * @param context   of fragment or activity
	 * @param imageName name of the image including its suffix
	 * @return null if imageName is empty or file does not exist
	 * @author Manuel Thöne
	 */
	public static Bitmap getImageByNameFromInternalStorage(@NonNull Context context, @NonNull String imageName) {
		if (imageName.isEmpty()) {
			return null;
		}

		File pictureFile = getPicturePathFromName(context, imageName);
		if (!pictureFile.exists()) return null;

		Bitmap originalBitmap = BitmapFactory.decodeFile(pictureFile.getAbsolutePath());
		Bitmap rotatedBitmap = originalBitmap;
		ExifInterface ei = null;
		try {
			ei = new ExifInterface(pictureFile.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (ei != null) {
			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_UNDEFINED);

			switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_90:
					rotatedBitmap = rotateImage(originalBitmap, 90);
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					rotatedBitmap = rotateImage(originalBitmap, 180);
					break;
				case ExifInterface.ORIENTATION_ROTATE_270:
					rotatedBitmap = rotateImage(originalBitmap, 270);
					break;
				case ExifInterface.ORIENTATION_NORMAL:
				default:
					rotatedBitmap = originalBitmap;
			}
		}
		return rotatedBitmap;
	}

	private static File getPicturePathFromName(@NonNull Context context, String imageName) {
		File directory = new File(context.getFilesDir(), PICTURE_PATH);
		return new File(directory, imageName);
	}

	/**
	 * Rotates the image around a specific angle.
	 * Source from <a href="https://stackoverflow.com/a/14066265/7831593">Stackoverflow</a>
	 *
	 * @param source image to rotate
	 * @param angle  of rotation in degrees
	 * @return rotated copy of the image
	 */
	public static Bitmap rotateImage(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
				matrix, true);
	}

	/**
	 * Taken from <a href="https://developer.android.com/topic/performance/graphics/load-bitmap#load-bitmap">developer.android.com</a>
	 * */
	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) >= reqHeight
					&& (halfWidth / inSampleSize) >= reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}


	/**
	 * Creates a thumbnail from the passed image name.
	 * When width or height equal to -1 it uses: dimens/attachment_thumbnail_height and dimens/attachment_thumbnail_width
	 *
	 * @param context   of fragment or activity
	 * @param imageName name of the image including its suffix
	 * @param width     width of the image.
	 * @param height    width of the image.
	 * @return null if imageName is empty or file does not exist
	 * @author Manuel Thöne
	 */
	public static Bitmap getThumbnailByNameFromInternalStorage(@NonNull Context context, @NonNull String imageName, int width, int height) {
		if (imageName.isEmpty()) {
			return null;
		}
		File picturePath = getPicturePathFromName(context, imageName);
		if (!picturePath.exists()) return null;

		if (width == -1 || height == -1) {
			width = Math.round(context.getResources().getDimension(R.dimen.attachment_thumbnail_width));
			height = Math.round(context.getResources().getDimension(R.dimen.attachment_thumbnail_width));
		}

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(picturePath.getAbsolutePath(), options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, width, height);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		Bitmap originalBitmap = BitmapFactory.decodeFile(picturePath.getAbsolutePath(), options);

		Bitmap rotatedBitmap = originalBitmap;

		ExifInterface ei = null;
		try {
			ei = new ExifInterface(picturePath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (ei != null) {
			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_UNDEFINED);

			switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_90:
					rotatedBitmap = rotateImage(originalBitmap, 90);
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					rotatedBitmap = rotateImage(originalBitmap, 180);
					break;
				case ExifInterface.ORIENTATION_ROTATE_270:
					rotatedBitmap = rotateImage(originalBitmap, 270);
					break;
				case ExifInterface.ORIENTATION_NORMAL:
				default:
					rotatedBitmap = originalBitmap;
			}
		}

		return rotatedBitmap;
	}

	/**
	 * Gets the file size in bytes
	 *
	 * @param filePath absolute path of a file
	 * @return -1 if file not existing
	 * @author Manuel Thöne
	 */
	public static long getFileSizeByFileName(@NonNull String filePath) {
		if (filePath.isEmpty()) return -1;
		File file = new File(filePath);
		return getFileSize(file);
	}

	/**
	 * Gets the file size in bytes
	 *
	 * @param file a specific file
	 * @return -1 if file not existing
	 * @author Manuel Thöne
	 */
	public static long getFileSize(@NonNull File file) {
		if (!file.exists()) return -1;
		return file.length();
	}

	/**
	 * Deletes a temporary image when the file is empty
	 *
	 * @param context   of fragment or activity
	 * @param imageName name of the image including its suffix
	 * @return true if file sucessfully deleted; otherwise false
	 * @author Manuel Thöne
	 */
	public static boolean deleteIfEmptyImage(@NonNull Context context, String imageName) {
		if (imageName.isEmpty()) return false;
		File pictureFile = getPicturePathFromName(context, imageName);
		long fileSize = getFileSize(pictureFile);
		if (fileSize == 0) {
			// TODO: 16.03.2022 delete File
			return pictureFile.delete();
		} else return false;
	}

	/**
	 * Deletes an image from internal storage
	 *
	 * @param context  of fragment or activity
	 * @param fileName name of the image including its suffix
	 * @return true if file sucessfully deleted; otherwise false
	 * @author Manuel Thöne
	 */
	public static boolean deleteImageByName(@NonNull Context context, @NonNull String fileName) {
		if (fileName.isEmpty()) return false;
		File pictureFile = getPicturePathFromName(context, fileName);
		return pictureFile.delete();
	}


	public static class SuffixNotSupportedException extends Throwable {
		public SuffixNotSupportedException(String s) {
			super(s);
		}
	}

	/**
	 * The AttachmentImageFormat enum contains all supported image file formats with the corresponding suffix.
	 *
	 * @author Manuel Thöne
	 */
	public enum AttachmentImageFormat {
		IMAGE_SUFFIX_BMP("bmp"),
		IMAGE_SUFFIX_GIF("gif"),
		IMAGE_SUFFIX_JPG("jpg"),
		IMAGE_SUFFIX_PNG("png"),
		IMAGE_SUFFIX_WEBP("gif"),
		IMAGE_SUFFIX_HEIF("webp");

		public final String suffix;

		AttachmentImageFormat(String suffix) {
			this.suffix = suffix;
		}

		/**
		 * Checks if a provided suffix is part of the supported image formats.
		 *
		 * @param suffix the suffix which should be checked out.
		 * @return true if suffix is part of the supported formats
		 * @author Manuel Thöne
		 */
		public static boolean containsSuffix(String suffix) {
			for (AttachmentImageFormat imageFormat : AttachmentImageFormat.values()) {
				if (imageFormat.suffix.equals(suffix)) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * The AttachmentImageFormat enum contains all supported audio file formats with the corresponding suffix.
	 *
	 * @author Manuel Thöne
	 */
	public enum AttachmentAudioFormat {
		AUDIO_SUFFIX_M4A("m4a"),
		AUDIO_SUFFIX_AAC("aac"),
		AUDIO_SUFFIX_3GP("3gp"),
		AUDIO_SUFFIX_MP4("mp4"),
		AUDIO_SUFFIX_FLAC("flac"),
		AUDIO_SUFFIX_MP3("mp3"),
		AUDIO_SUFFIX_OGG("ogg"),
		AUDIO_SUFFIX_WAV("wav");

		public final String suffix;

		AttachmentAudioFormat(String suffix) {
			this.suffix = suffix;
		}

		/**
		 * Checks if a provided suffix is part of the supported audio formats.
		 *
		 * @param suffix the suffix which should be checked out.
		 * @return true if suffix is part of the supported formats
		 * @author Manuel Thöne
		 */
		public static boolean containsSuffix(String suffix) {
			for (AttachmentImageFormat imageFormat : AttachmentImageFormat.values()) {
				if (imageFormat.suffix.equals(suffix)) {
					return true;
				}
			}
			return false;
		}
	}
}

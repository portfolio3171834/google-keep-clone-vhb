package com.passau.notizen.ui.tag;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.passau.notizen.R;
import com.passau.notizen.database.tables.Tag;

import java.util.ArrayList;

/**
 * Adapter with edit functionality for tag editing
 *
 * @author Manuel Thöne
 */
public class RecyclerViewTagEditAdapter extends RecyclerView.Adapter<RecyclerViewTagEditAdapter.ViewHolder> {
	final Context context;
	RecyclerViewTagEditAdapter.TagAdapterListener activityListener;
	ArrayList<Tag> tags;

	public RecyclerViewTagEditAdapter(ArrayList<Tag> allAvailableTags,
									  RecyclerViewTagEditAdapter.TagAdapterListener listener,
									  Context context) {
		this.activityListener = listener;
		this.context = context;
		if (allAvailableTags == null) {
			tags = new ArrayList<>();
		} else {
			tags = allAvailableTags;
		}
	}

	@NonNull
	@Override
	public RecyclerViewTagEditAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_edit_tag_combined, parent, false);
		return new ViewHolder(itemLayoutView, new ChangeListener());
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerViewTagEditAdapter.ViewHolder holder, final int position) {
		holder.listener.updatePosition(holder.getBindingAdapterPosition());
		holder.listener.updatePosition(holder.getBindingAdapterPosition());
		String tagName = tags.get(position).getTagName();
		holder.textViewInactive.setText(tagName);
		holder.editTextActive.setText(tagName);
	}

	@Override
	public int getItemCount() {
		return tags.size();
	}


	public static class ViewHolder extends RecyclerView.ViewHolder {
		protected LinearLayout selectedLayout;
		protected LinearLayout inactiveLayout;
		protected EditText editTextActive;
		protected TextView textViewInactive;
		protected ImageButton applyBtn;
		protected ImageButton deleteBtn;

		protected final ChangeListener listener;

		public ViewHolder(@NonNull View itemView, ChangeListener listener) {
			super(itemView);
			this.listener = listener;
			inactiveLayout = itemView.findViewById(R.id.edit_tags_layout_inactive);
			selectedLayout = itemView.findViewById(R.id.edit_tags_layout_selected);
			editTextActive = itemView.findViewById(R.id.tagedit_tagname_editText_active);
			textViewInactive = itemView.findViewById(R.id.tagedit_tagname_textView_inactive);
			applyBtn = itemView.findViewById(R.id.tagedit_apply_imagebutton);
			deleteBtn = itemView.findViewById(R.id.tagedit_delete_imagebutton);

			listener.initChangeListener(selectedLayout, inactiveLayout, editTextActive);
			inactiveLayout.setOnClickListener(listener);
			editTextActive.setOnFocusChangeListener(listener);
			applyBtn.setOnClickListener(listener);
			deleteBtn.setOnClickListener(listener);

		}
	}

	private class ChangeListener implements View.OnFocusChangeListener, View.OnClickListener {
		private int position;
		private LinearLayout selectedLayout;
		private LinearLayout inactiveLayout;
		private EditText editTextActive;

		protected void updatePosition(int position) {
			this.position = position;
		}

		protected void initChangeListener(LinearLayout activeLayout, LinearLayout inactiveLayout, EditText editTextActive) {
			this.selectedLayout = activeLayout;
			this.inactiveLayout = inactiveLayout;
			this.editTextActive = editTextActive;
		}

		@Override
		public void onClick(View v) {
			int id = v.getId();

			if (id == inactiveLayout.getId()) {
				inactiveLayout.setVisibility(View.GONE);
				selectedLayout.setVisibility(View.VISIBLE);
				editTextActive.requestFocus();
				editTextActive.setSelection(editTextActive.getText().length());
			} else if (id == R.id.tagedit_apply_imagebutton) {

				if (!editTextActive.getText().toString().trim().isEmpty()) {
					Tag editedTag = tags.get(position);
					Tag newTag = new Tag(editedTag.getId(), editTextActive.getText().toString().trim());
					// TODO: 25.03.2022 change name of tag
					activityListener.onTagChanged(newTag, position);
					tags.set(position, newTag);
					notifyItemChanged(position);
					selectedLayout.setVisibility(View.GONE);
					inactiveLayout.setVisibility(View.VISIBLE);
				}
			} else if (id == R.id.tagedit_delete_imagebutton) {
				selectedLayout.setVisibility(View.GONE);
				inactiveLayout.setVisibility(View.VISIBLE);
				Tag tagToRemove = tags.get(position);
				Log.d("Notiz", "in Recycler -> onclick: removes " + tagToRemove + " at position " + position);
				tags.remove(position);
				notifyItemRemoved(position);
				notifyItemRangeChanged(position, getItemCount());
				activityListener.onTagRemoved(tagToRemove, position);
				Log.d("Notiz", "in Recycler -> onclick: removed " + tagToRemove + " at position " + position);
			}
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			int id = v.getId();
			if (v == editTextActive) {
				if (!hasFocus) {
					((EditText) v).setText(tags.get(position).getTagName());
					selectedLayout.setVisibility(View.GONE);
					inactiveLayout.setVisibility(View.VISIBLE);
				}
			}
		}
	}


	public interface TagAdapterListener {
		public void onTagChanged(Tag tag, int position);
		public void onTagRemoved(Tag tag, int position);
	}
}

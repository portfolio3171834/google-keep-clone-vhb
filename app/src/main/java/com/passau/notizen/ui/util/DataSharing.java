package com.passau.notizen.ui.util;

public abstract class DataSharing {
	public static final String KEY_NOTE = "note";
	public static final String KEY_TAGS = "tags";
	public static final String KEY_META = "meta";
	public static final String KEY_POSITION = "position";
	public static final String KEY_NEW_TAGS = "new_tags";
	public static final String KEY_DELETED_TAGS = "deleted_tags";
	public static final String KEY_EDITED_TAGS = "edited_tags";

	public static final String KEY_LOGIN_EMAIL = "email";
	public static final String KEY_LOGIN_PASSWORD = "password";

	public static final String KEY_ACCOUNT_NOTE_COUNT = "note_count";
	public static final String KEY_ACCOUNT_TAG_COUNT = "tag_count";


	public static final String NOTE_ARCHIVED = "note_archived";
	public static final String NOTE_UNARCHIVED = "note_unarchived";
	public static final String NOTE_SAVED = "note_saved";
	public static final String NOTE_DELETED = "note_deleted";
	public static final String TAG_EDIT = "tag_edit";
	public static final boolean ARCHIVED = true;
	public static final boolean UN_ARCHIVED = false;

	public static final int TAGS_DELETED = -1;
	public static final int TAGS_EDITED = 0;
	public static final int TAGS_ADDED = 1;



	public static final int NO_POSITION = -1;
}

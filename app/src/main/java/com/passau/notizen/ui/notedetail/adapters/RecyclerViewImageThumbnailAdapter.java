package com.passau.notizen.ui.notedetail.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.passau.notizen.R;
import com.passau.notizen.data.AttachmentThumbnail;

import java.util.ArrayList;

public class RecyclerViewImageThumbnailAdapter extends RecyclerView.Adapter<RecyclerViewImageThumbnailAdapter.ViewHolder> {
	private ArrayList<AttachmentThumbnail> data;

	public RecyclerViewImageThumbnailAdapter() {
		this.data = new ArrayList<>();
	}

	public RecyclerViewImageThumbnailAdapter(ArrayList<AttachmentThumbnail> data) {
		this.data = data;
	}

	public void updateAttachmentThumbnails(ArrayList<AttachmentThumbnail> attachmentThumbnails) {
		data = null;
		data = attachmentThumbnails;
		notifyDataSetChanged();
	}

	public void addAttachmentThumbnail(AttachmentThumbnail attachmentThumbnail) {
		data.add(attachmentThumbnail);
		notifyItemInserted(data.size() - 1);
	}

	@NonNull
	@Override
	public RecyclerViewImageThumbnailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_image_attachment_view, parent, false);
		return new ViewHolder(itemLayoutView);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerViewImageThumbnailAdapter.ViewHolder holder, int position) {
		holder.imageView.setImageBitmap(data.get(position).getThumbnail());

	}

	@Override
	public int getItemCount() {
		return data.size();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public ImageView imageView;

		public ViewHolder(@NonNull View itemView) {
			super(itemView);
			imageView = itemView.findViewById(R.id.viewholder_image_view);
		}
	}
}

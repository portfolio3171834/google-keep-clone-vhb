package com.passau.notizen.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.passau.notizen.R;
import com.passau.notizen.ui.settings.SettingsPreferenceKeys;


public abstract class BaseActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

	private SharedPreferences settingsPreference;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.settingsPreference = getSharedPreferences(SettingsPreferenceKeys.NAME_SETTINGS_PREFS, Context.MODE_PRIVATE);
		settingsPreference.registerOnSharedPreferenceChangeListener(this);
		initScreenshotProtection();
	}

	/**
	 * Sets up the action bar in order to work around the coloring issue
	 *
	 * @author Manuel Thöne
	 */
	protected void initActionBar() {
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setNavigationOnClickListener(v -> finish());
	}

	/**
	 * Method to provide all child classes the option to execute system wide preferences in their override;<br>
	 * Parameters see {@link com.passau.notizen.ui.BaseActivity#onSharedPreferenceChanged(SharedPreferences, String)}
	 *
	 * @param sharedPreferences should be SharedPreference {@link SettingsPreferenceKeys#NAME_SETTINGS_PREFS}
	 * @author Manuel Thöne
	 */
	protected void defaultSettingsExecute(SharedPreferences sharedPreferences, String key) {
		if (key.equals(SettingsPreferenceKeys.KEY_ALLOW_SCREENSHOTS)) {
			setScreenshotsProtection(sharedPreferences.getBoolean(key, true));
		}
	}

	/**
	 * Sets the screenshot protection
	 *
	 * @param allowed true if and only if screenshots should be allowed; flase otherwise
	 * @author Manuel Thöne
	 */
	protected void setScreenshotsProtection(Boolean allowed) {
		if (allowed) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
		} else {
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
		}
	}

	/**
	 * initializes the screenshot protection in the creation of the activity
	 *
	 * @author Manuel Thöne
	 */
	public void initScreenshotProtection() {
		setScreenshotsProtection(settingsPreference.getBoolean(SettingsPreferenceKeys.KEY_ALLOW_SCREENSHOTS, true));
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		defaultSettingsExecute(sharedPreferences, key);
	}
}

package com.passau.notizen.ui.util;

import android.app.Activity;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public abstract class InterfaceUtility {

	/**
	 * Mthod to hide the soft keyboard.
	 * Source on <a href="https://stackoverflow.com/a/11656129/7831593">Stackoverflow</a>
	 */
	public static boolean hideSoftKeyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		boolean worked = true;
		try {
			if (inputMethodManager.isAcceptingText()) {
				inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),0);
			}
		} catch (Exception e) {
			worked = false;
		}

		if(!worked) {
			try {
				activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN );
			} catch (Exception e) {
				worked = false;
			}
		}
		return worked;
	}
}

package com.passau.notizen.ui.notedetail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;
import com.passau.notizen.R;
import com.passau.notizen.data.Note;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.ui.BaseActivity;
import com.passau.notizen.ui.util.DataSharing;

import java.util.ArrayList;


public class NoteDetailActivity extends BaseActivity {

	NoteDetailSharedViewModel model;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		model = new ViewModelProvider(this).get(NoteDetailSharedViewModel.class);

		initIntentData();

		setContentView(R.layout.activity_note_detail);
	}

	private void initIntentData() {
		Intent i = getIntent();

		// get all available tags
		ArrayList<Tag> parcelableTags = i.getParcelableArrayListExtra(DataSharing.KEY_TAGS);
		if (parcelableTags != null) {
			model.setAllAvailableTags(parcelableTags);
		}

		// get note if passed
		Note note = i.getParcelableExtra(DataSharing.KEY_NOTE);
//		Log.d("Notiz", "initIntentData()-> note=" + note.toString());
		if (note != null) {
			model.setNote(note);
		}

		// get position if clicked existing note
		model.setPosition(i.getIntExtra(DataSharing.KEY_POSITION, DataSharing.NO_POSITION));
	}

	/**
	 * Saves puts the saved note into the result
	 */
	public void setNoteAsResultAndFinish(String metaDataValue) {
		Note saveNote = model.getNoteToSave().getValue();
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putInt(DataSharing.KEY_POSITION, model.getPosition());
		bundle.putString(DataSharing.KEY_META, metaDataValue);
		setResult(Activity.RESULT_OK, intent);
		if (saveNote != null) {
			Log.d("Notiz", "NoteDetailActivity.saveNote()-> " + saveNote.toString());
			bundle.putParcelable("note", saveNote);
		} else if(metaDataValue.equals(DataSharing.NOTE_DELETED)) {
			bundle.putString(DataSharing.KEY_META, metaDataValue);
		} else {
			bundle = null;
			setResult(Activity.RESULT_CANCELED);
		}
		intent.putExtras(bundle);
		finish();
	}


	/**
	 * Handles back press from hard/software buttons.
	 * After last fragment: Shows dialog to close the activity.
	 *
	 * @author Manuel Thöne
	 */
	@Override
	public void onBackPressed() {
		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			getSupportFragmentManager().popBackStack();
		} else {
			showAlertDialog();
		}
	}

	/**
	 * Shows an alert dialog for discarding the current note draft
	 *
	 * @author Manuel Thöne
	 */
	private void showAlertDialog() {
		if(model.isArchived().getValue()) {
			finish();
		} else {
			AlertDialog dialog;
			AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
			String message = getResources().getString(R.string.note_delete_draft_message);

			if (model.getNote().getValue() != null) {
				message = getResources().getString(R.string.note_delete_opening_draft_message);
			}

			builder.setMessage(message)
					.setTitle(getResources().getString(R.string.note_delete_draft_title));
			builder.setPositiveButton(getResources().getString(R.string.delete_draft), (dialogInterface, which) -> {
				// Remove all newly created attachments if not saved
				if (model.removeAllNewCreatedImageAttachments()) {
					finish();
				} else {
					String messageSnackbar = getResources().getString(R.string.snackbar_delete_attachments);
					Snackbar snackbar = Snackbar.make(findViewById(R.id.notedetail_fragment_view), messageSnackbar, Snackbar.LENGTH_INDEFINITE);
					snackbar.setAction(R.string.understood, v -> dialogInterface.dismiss());
					snackbar.show();
				}
			});
			builder.setNegativeButton(R.string.cancel, (dialogInterface, which) -> dialogInterface.dismiss());
			dialog = builder.create();
			dialog.show();
		}
	}
}
package com.passau.notizen.ui.notedetail.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.passau.notizen.R;
import com.passau.notizen.data.AttachmentThumbnail;
import com.passau.notizen.ui.notedetail.AttachmentManager;
import com.passau.notizen.ui.notedetail.NoteDetailActivity;
import com.passau.notizen.ui.notedetail.NoteDetailSharedViewModel;
import com.passau.notizen.ui.notedetail.adapters.RecyclerViewImageThumbnailAdapter;
import com.passau.notizen.ui.util.DataSharing;
import com.passau.notizen.ui.util.InterfaceUtility;
import com.passau.notizen.ui.util.ItemClickSupport;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Fragment for editing a note
 *
 * @author Manuel Thöne
 */
public class NoteDetailEditFragment extends Fragment implements ItemClickSupport.OnItemClickListener, ItemClickSupport.OnItemLongClickListener, TextWatcher {

	protected Context context;
	private View fragmentView;
	private NoteDetailSharedViewModel model;

	private RecyclerViewImageThumbnailAdapter thumbnailAdapter;

	private ActivityResultLauncher<Intent> activityResultLauncher;
	private final ActivityResultLauncher<String[]> activityResultPermissionLauncher;
	private String[] appPerms = new String[]{Manifest.permission.CAMERA};

	private EditText textEditHead;
	private EditText textEditBody;
	private LinearLayout attachmentLayout;
	private Dialog bottomSheetDialog;


	public NoteDetailEditFragment() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
			appPerms = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
		}


		activityResultPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), result -> {
			Log.d("Notiz", "" + result.toString());
			boolean areAllGranted = true;
			for (Boolean b : result.values()) {
				areAllGranted = areAllGranted && b;
			}
			if (areAllGranted) {
				openCameraActivityForResult();
			} else {
				String message = getResources().getString(R.string.snackbar_attachment_permission);
				Snackbar snackbar = Snackbar.make(fragmentView, message, Snackbar.LENGTH_LONG);
				snackbar.setAction(R.string.understood, v -> {
				});
				snackbar.show();
			}
		});
	}

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		if (this.context == null && context instanceof NoteDetailActivity) {
			this.context = context;    // Sets the context
		}
	}

	@Override
	public void onDetach() {
		this.context = null;
		super.onDetach();
	}

	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {

		model = new ViewModelProvider(requireActivity()).get(NoteDetailSharedViewModel.class);

		setHasOptionsMenu(true);
		fragmentView = inflater.inflate(R.layout.fragment_note_detail_edit, container, false);

		initActionBar();
		initBottomToolBar();
		// Wait for content fill until note has loaded in LiveData
		initExistingNode();

		initTextInputs();
		initActivityResultLauncher();
		attachmentLayout = fragmentView.findViewById(R.id.attachment_container);
		initThumbnailAdapter();


		model.isEdited().observe(getViewLifecycleOwner(), isEdited -> Log.d("Notiz", "in onChanged()->isEdited=" + isEdited.toString()));

		return fragmentView;
	}

	/**
	 * Initializes all inputs when an existing note was passed and initialized
	 */
	private void initExistingNode() {
		model.getNote().observe(getViewLifecycleOwner(), note -> {
			if (note != null) {
				model.loadAttachmentThumbnails(new NoteDetailSharedViewModel.AttachmentObserverListener() {
					@Override
					public void onAttachmentViewFileDelete() {
					}

					@Override
					public void onExistingNoteAttachmentThumbnailLoaded(AttachmentThumbnail thumbnail) {
						Activity activity = getActivity();
						if (activity != null) {
							activity.runOnUiThread(() -> model.addAttachmentThumbnail(thumbnail));
						}
					}
				});

				if (note.getBody() != null && !note.getBody().isEmpty()) {
					model.setBodyText(note.getBody());
				}
				if (note.getHeadline() != null && !note.getHeadline().isEmpty()) {
					model.setTitleText(note.getHeadline());
				}
				if (note.getTags() != null && note.getTags().size() > 0) {
					model.setAllSelectedTags(note.getTags());
				}
				model.setIsPinned(note.isPinned());
				model.setIsArchived(note.isArchived());
				ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
				if (note.isArchived()) {
					BottomNavigationView bottomNavigationView = fragmentView.findViewById(R.id.toolbar_include);
					if (bottomNavigationView != null) {
						bottomNavigationView.setVisibility(View.GONE);
					}
					if (actionBar != null) {
						actionBar.setTitle(getResources().getString(R.string.archive));
					}
				} else {
					if (actionBar != null) {
						actionBar.setTitle("");
					}
				}

//					textEditHead.addTextChangedListener(NoteDetailEditFragment.this);
//					textEditBody.addTextChangedListener(NoteDetailEditFragment.this);
				// TODO: 20.03.2022 set isEdited to true if textinput
				model.setIsEdited(false);

				Log.d("Notiz", "in onChanged() -> Note = " + note.toString());
			}

		});
	}

	/**
	 * Initializes the thumbnail adapter
	 *
	 * @author Manuel Thöne
	 */
	private void initThumbnailAdapter() {
		thumbnailAdapter = new RecyclerViewImageThumbnailAdapter();
		RecyclerView imageAttachmentRecyclerView = fragmentView.findViewById(R.id.image_recyclerView);
		imageAttachmentRecyclerView.setAdapter(thumbnailAdapter);
		ItemClickSupport.addTo(imageAttachmentRecyclerView).setOnItemClickListener(this);
		ItemClickSupport.addTo(imageAttachmentRecyclerView).setOnItemLongClickListener(this);

		model.getAttachmentThumbnails().observe(getViewLifecycleOwner(), attachmentThumbnails -> {
			if (attachmentThumbnails == null) return;
			thumbnailAdapter.updateAttachmentThumbnails(attachmentThumbnails);
			if (attachmentThumbnails.size() <= 0) {
				hideAttachmentLayout();
			} else {
				showAttachmentLayout();
			}
		});
	}

	/**
	 * Sets up the action bar and the back button
	 *
	 * @author Manuel Thöne
	 */
	private void initActionBar() {
		Toolbar toolbar = fragmentView.findViewById(R.id.toolbar);
		((AppCompatActivity) context).setSupportActionBar(toolbar);

		toolbar.setNavigationOnClickListener(view -> {

			if (model.isArchived().getValue()) {
				((NoteDetailActivity) context).finish();
			} else {
				AlertDialog dialog;
				AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme);

				String message = getResources().getString(R.string.note_delete_draft_message);

				if (model.getNote().getValue() != null) {
					message = getResources().getString(R.string.note_delete_opening_draft_message);
				}

				builder.setMessage(message)
						.setTitle(getResources().getString(R.string.note_delete_draft_title));
				builder.setPositiveButton(R.string.delete_draft, (dialogInterface, which) -> {
					// Remove all newly created attachments if not saved
					if (model.removeAllNewCreatedImageAttachments()) {
						((NoteDetailActivity) context).finish();
					} else {
						String messageSnackbar = getResources().getString(R.string.snackbar_delete_attachments);
						Snackbar snackbar = Snackbar.make(fragmentView, messageSnackbar, Snackbar.LENGTH_INDEFINITE);
						snackbar.setAction(R.string.understood, v -> dialogInterface.dismiss());
						snackbar.show();
					}
				});
				builder.setNegativeButton(R.string.cancel, (dialogInterface, which) -> dialogInterface.dismiss());
				dialog = builder.create();
				dialog.show();
			}

		});
	}

	/**
	 * Sets up the bottom toolbar
	 *
	 * @author Manuel Thöne
	 */
	private void initBottomToolBar() {
		BottomNavigationView bottomNavigationView = fragmentView.findViewById(R.id.toolbar_include);
		bottomNavigationView.setOnItemSelectedListener(item -> {
			int itemId = item.getItemId();
			if (itemId == R.id.toolbar_attachment) {
				showAttachmentSheet();
			} else if (itemId == R.id.toolbar_tags) {
				openTagSelection();
			} else if (itemId == R.id.toolbar_notification) {
				// Todo: Notification functionality
				Toast.makeText(context.getApplicationContext(), "Todo: Notification!", Toast.LENGTH_SHORT).show();
			} else if (itemId == R.id.toolbar_save) {
				if (!saveNoteAndFinish(DataSharing.NOTE_SAVED)) {
					showSaveFailDialog();
				}
			}
			return false;
		});

		model.isEdited().observe(getViewLifecycleOwner(), isEdited -> {
//				MenuItem saveItem = bottomNavigationView.getMenu().findItem(R.id.toolbar_save).setEnabled(isEdited);
			// TODO: 20.03.2022 not working!
		});
	}

	private void showSaveFailDialog() {
		// TODO: 23.03.2022 richtiger inhalt
		AlertDialog dialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.SaveFailDialogTheme);
		builder.setMessage(R.string.note_failed_saving_message)
				.setTitle(R.string.note_failed_saving_title);
		builder.setPositiveButton(R.string.understood, (dialogInterface, which) -> dialogInterface.dismiss());
		dialog = builder.create();
		dialog.show();
	}

	/**
	 * Saves the note and finishes the activity if the saving was successful and at least a headline or a text was set
	 *
	 * @return true only if note has at least a headline or a body-text; otherwise false
	 */
	private boolean saveNoteAndFinish(String metaDataValue) {
		model.setBodyText(textEditBody.getText().toString());
		model.setTitleText(textEditHead.getText().toString());
		boolean success = model.generateNoteToSave();
		if (!success) return false;
		((NoteDetailActivity) context).setNoteAsResultAndFinish(metaDataValue);
		return true;
	}

	private void deleteNoteAndFinish() {
		((NoteDetailActivity) context).setNoteAsResultAndFinish(DataSharing.NOTE_DELETED);
	}

	private void openTagSelection() {
		FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.enter_zoom_in, R.anim.exit_zoom_out, R.anim.enter_zoom_in, R.anim.exit_zoom_out);

		NoteDetailTagFragment tagFragment = new NoteDetailTagFragment();
		transaction.add(R.id.notedetail_fragment_view, tagFragment, NoteDetailViewImageFragment.FRAGMENT_TAG);
		transaction.addToBackStack(NoteDetailViewImageFragment.FRAGMENT_TAG);
		transaction.commit();
		InterfaceUtility.hideSoftKeyboard(requireActivity());
	}

	/**
	 * Initializes the Note-Title and -Body TextViews
	 *
	 * @author Manuel Thöne
	 */
	private void initTextInputs() {
		textEditHead = fragmentView.findViewById(R.id.editText_headline);
		textEditBody = fragmentView.findViewById(R.id.editText_note_body);

		if (model.getNote().getValue() == null) {
			textEditBody.requestFocus();
		}
//		if(model.getBodyText().getValue().equals("")) {
//		}

		// TODO: 23.03.2022 check if still working
		// Lock input if note is archived
		if (model.getNote().getValue() != null && model.getNote().getValue().isArchived()) {
			Log.d("Notiz", "in initTextInputs() -> isArchived=" + model.isArchived().getValue());
			textEditHead.setFocusable(false);
			textEditBody.setFocusable(false);
		}
		// Set focus to body text when 'Enter'
		textEditHead.setOnEditorActionListener((view, actionId, event) -> {
			boolean handled = false;
			if (actionId == EditorInfo.IME_ACTION_NEXT) {
				textEditBody.requestFocus();
				handled = true;
			}
			return handled;
		});

		model.getTitleText().observe(getViewLifecycleOwner(), titleText -> {
			if (titleText != null && !titleText.isEmpty()) {
				textEditHead.setText(titleText);
			}
		});

		model.getBodyText().observe(getViewLifecycleOwner(), bodyText -> {
			if (bodyText != null && !bodyText.isEmpty()) {
				textEditBody.setText(bodyText);
			}
		});
	}

	/**
	 * Initializes the handling of incoming intents
	 *
	 * @author Manuel Thöne
	 */
	private void initActivityResultLauncher() {
		activityResultLauncher = registerForActivityResult(
				new ActivityResultContracts.StartActivityForResult(),
				result -> {
					if (result.getResultCode() == Activity.RESULT_OK) {
						// There are no request codes
						Intent data = result.getData();
						if ((data == null || data.getExtras() == null) && model.getLastCreatedFile().getValue() != null) {
							// Case: Image Capture intent!
							Bitmap thumbnail = model.getCapturedImageAsThumbnail(-1, -1);
							if (thumbnail != null) {
								String fileName = model.getLastCreatedFile().getValue();
								model.addNewCreatedImage(fileName);
								appendThumbnailToNote(new AttachmentThumbnail(fileName, thumbnail));
								model.setLastCreatedFile(null);
							}
						}
					} else { // Result not OK
						if (model.getLastCreatedFile().getValue() != null) {
							AttachmentManager.deleteIfEmptyImage(context, model.getLastCreatedFile().getValue());
							model.setLastCreatedFile(null);
						}
					}
				});
	}

	/**
	 * Appends the captured image to the note image preview element
	 *
	 * @param thumbnail The thumbnail as Bitmap
	 * @author Manuel Thöne
	 */
	private void appendThumbnailToNote(AttachmentThumbnail thumbnail) {
		model.addAttachmentThumbnail(thumbnail);
		showAttachmentLayout();
	}

	/**
	 * Shows the attachment layout
	 *
	 * @author Manuel Thöene
	 */
	private void showAttachmentLayout() {
		if (attachmentLayout != null) {
			attachmentLayout.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Hides the attachment layout
	 *
	 * @author Manuel Thöne
	 */
	private void hideAttachmentLayout() {
		if (attachmentLayout != null) {
			attachmentLayout.setVisibility(View.GONE);
		}
	}


	/**
	 * Shows the bottom sheet containing the attachment options
	 * Removes camera option if not available
	 *
	 * @author Manuel Thöne
	 */
	private void showAttachmentSheet() {
		bottomSheetDialog = new Dialog(getContext());
		bottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		bottomSheetDialog.setContentView(R.layout.layout_notedetail_attachment_sheet);

		Window dialogWindow = bottomSheetDialog.getWindow();

		NavigationView navigationView = dialogWindow.findViewById(R.id.attachment_bottomsheet_navigationview);
		navigationView.setNavigationItemSelectedListener(this::onOptionsItemSelected);

		if (!isCameraAvailable()) {
			navigationView.getMenu().removeItem(R.id.attachment_capture_image);
		}

		dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialogWindow.getAttributes().windowAnimations = R.style.AttachmentSheetAnimation;
		dialogWindow.setGravity(Gravity.BOTTOM);
		bottomSheetDialog.show();
	}

	/**
	 * Checks if a camera is available
	 *
	 * @author Manuel Thöne
	 */
	private boolean isCameraAvailable() {
		PackageManager pm = context.getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
	}

	/**
	 * Opens a Camera intent to capture an image
	 *
	 * @author Manuel Thöne
	 */
	public void openCameraActivityForResult() {
		File photoFile;
		model.setLastCreatedFile(null);
		try {
			photoFile = AttachmentManager.createAttachmentFile(context,
					AttachmentManager.AttachmentImageFormat.IMAGE_SUFFIX_JPG.suffix);
		} catch (IOException | AttachmentManager.SuffixNotSupportedException e) {
			// TODO: 15.03.2022 show error in ui
			e.printStackTrace();
			return;
		}
		if (photoFile != null) {
			Uri photoURI = FileProvider.getUriForFile(context,
					"com.passau.notizen.provider",
					photoFile);

			model.setLastCreatedFile(photoFile.getName());

			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
			activityResultLauncher.launch(intent);
		}
	}

	// Setup item click for imageAttachmentAdapter
	@Override
	public void onItemClicked(RecyclerView recyclerView, int position, View v) {
		Log.d("Notiz", "Clicked on thumbnail: index=" + position);

		NoteDetailViewImageFragment imageViewFragment = new NoteDetailViewImageFragment();
		Bundle arguments = new Bundle();

		ArrayList<AttachmentThumbnail> attachmentThumbnails = model.getAttachmentThumbnails().getValue();
		if (attachmentThumbnails != null) {
			AttachmentThumbnail attachmentThumbnail = attachmentThumbnails.get(position);
			arguments.putString(NoteDetailViewImageFragment.ARGUMENT_IMAGE_NAME, attachmentThumbnail.getFileName());
			imageViewFragment.setArguments(arguments);

			FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
			transaction.setCustomAnimations(R.anim.enter_zoom_in, R.anim.exit_zoom_out, R.anim.enter_zoom_in, R.anim.exit_zoom_out);

			transaction.add(R.id.notedetail_fragment_view, imageViewFragment, NoteDetailViewImageFragment.FRAGMENT_TAG);
			transaction.addToBackStack(NoteDetailViewImageFragment.FRAGMENT_TAG);
			transaction.commit();
			InterfaceUtility.hideSoftKeyboard(requireActivity());
		}

	}

	// Setup item long click for imageAttachmentAdapter
	@Override
	public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
		Log.d("Notiz", "Clicked LONG on thumbnail: index=" + position);
		return false;
	}

	/**
	 * Handles the onclick of menu items in the top action bar and the attachment bar
	 *
	 * @author Manuel Thöne
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d("Notiz", "clicked: " + item.getTitle());

		int id = item.getItemId();

		// Handle Action Bar Events:
		if (id == R.id.app_bar_archive) {
			model.setIsArchived(true);
			Toast.makeText(context, getResources().getString(R.string.note_archived), Toast.LENGTH_LONG).show();
			if (!saveNoteAndFinish(DataSharing.NOTE_ARCHIVED)) {
				showSaveFailDialog();
			}
		} else if (id == R.id.app_bar_unarchive) {
			model.setIsArchived(false);
			Toast.makeText(context, getResources().getString(R.string.note_unarchived), Toast.LENGTH_LONG).show();
			saveNoteAndFinish(DataSharing.NOTE_UNARCHIVED);
		} else if (id == R.id.app_bar_pin) {
			model.toggleIsPinned();
		} else if (id == R.id.app_bar_delete) {
			Toast.makeText(context, getResources().getString(R.string.note_deleted), Toast.LENGTH_LONG).show();
			deleteNoteAndFinish();
		}

		// TODO: 13.03.2022 Handle Attachment clicks
		// Handle attachment-menu clicks:
		else if (id == R.id.attachment_capture_image) {
			bottomSheetDialog.dismiss();
			activityResultPermissionLauncher.launch(appPerms);
//			openCameraActivityForResult();
		} else if (id == R.id.attachment_add_image) {
			bottomSheetDialog.dismiss();
			Toast.makeText(context, "Todo: Add Image", Toast.LENGTH_SHORT).show();
		} else if (id == R.id.attachment_record_audio) {
			bottomSheetDialog.dismiss();
			Toast.makeText(context, "Todo: Record Audio", Toast.LENGTH_SHORT).show();
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Inflates the action bar and initializes the model observer for action bar items
	 *
	 * @author Manuel Thöne
	 */
	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		menu.clear();

		boolean isArchived = model.isArchived().getValue();
		Log.d("Notiz", "in onCreateOPtionsMenu() -> isArchived=" + isArchived);

		if (isArchived) {
			inflater.inflate(R.menu.menu_actionbar_note_detail_archived, menu);
		} else {
			inflater.inflate(R.menu.menu_action_bar_note_detail, menu);
		}

		model.isPinned().observe(getViewLifecycleOwner(), isPinnedObserver -> {
			MenuItem pin = menu.findItem(R.id.app_bar_pin);
			if (pin != null) {
				if (isPinnedObserver) {
					pin.setIcon(AppCompatResources.getDrawable(context, R.drawable.ic_baseline_push_pin_24));
				} else {
					pin.setIcon(AppCompatResources.getDrawable(context, R.drawable.ic_outline_push_pin_24));
				}
			}

		});

		model.isArchived().observe(getViewLifecycleOwner(), isArchivedObserver -> {
			if (isArchivedObserver) {
				MenuItem archive = menu.findItem(R.id.app_bar_archive);
				if (archive != null) {
					archive.setIcon(AppCompatResources.getDrawable(context, R.drawable.ic_outline_unarchive_24));
				}
			} else {
				MenuItem unArchive = menu.findItem(R.id.app_bar_unarchive);
				if (unArchive != null) {
					unArchive.setIcon(AppCompatResources.getDrawable(context, R.drawable.ic_outline_archive_24));

				}
			}
		});

		super.onCreateOptionsMenu(menu, inflater);
	}

	// Text Watcher
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		Log.d("Notiz", "in beforeTextChanged()");
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		Log.d("Notiz", "in onTextChanged()");
		model.setIsEdited(true);
	}

	@Override
	public void afterTextChanged(Editable s) {
		Log.d("Notiz", "in afterTextChanged()");

	}
}

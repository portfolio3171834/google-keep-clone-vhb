package com.passau.notizen.ui.overview;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.passau.notizen.R;
import com.passau.notizen.cryptography.DatabaseKeyManager;
import com.passau.notizen.data.Note;
import com.passau.notizen.data.NoteListAdapter;
import com.passau.notizen.data.NoteManager;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.sync.PeriodicSyncService;
import com.passau.notizen.sync.SyncService;
import com.passau.notizen.ui.BaseActivity;
import com.passau.notizen.ui.account.AccountActivity;
import com.passau.notizen.ui.notedetail.NoteDetailActivity;
import com.passau.notizen.ui.settings.SettingsActivity;
import com.passau.notizen.ui.settings.SettingsPreferenceKeys;
import com.passau.notizen.ui.tag.EditTagActivity;
import com.passau.notizen.ui.util.DataSharing;
import com.passau.notizen.ui.util.ItemClickSupport;

import java.util.ArrayList;

public class OverviewActivity extends BaseActivity implements SearchView.OnQueryTextListener, NoteManager.NoteManagerListener, NoteListAdapter.NoteListAdapterListener {

	private DrawerLayout mDrawer;
	private Toolbar toolbar;
	private NavigationView nvDrawer;
	private ActionBarDrawerToggle drawerToggle;

	private ActivityResultLauncher<Intent> activityResultLauncher;

	private NoteListAdapter noteListAdapter;
	private NoteManager noteManager;

    private String password;
    private String emailAddress;

    private boolean periodicSyncServiceStarted, isSyncing;

    public static String REFRESH_VIEW = "refresh_view";

    private BroadcastReceiver  bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(OverviewActivity.this, getResources().getString(R.string.syncing_finished_with_new_notes), Toast.LENGTH_SHORT).show();
            noteManager.initNoteLists();
            noteManager.requestUpdate();
            noteListAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);

        getLoginIntent();

        // TODO: 27.03.2022 ENTFERNEN NUR FÜR TESTS
//        password = "";
//        emailAddress = "a@a.com";

        try {
            initNoteList(password, emailAddress, this);
        } catch (DatabaseKeyManager.DatabaseKeyException e) {
            e.printStackTrace();
        }
        noteManager.requestUpdate();

        initPeriodicSyncService();

        // Initialisieren von UI
        initResultLauncher();

        initToolbar();
        initFAB();
        initDrawer();
        initNavigationView();
    }

	/**
	 * Notifies the adapter to redraw based on the current preview style
	 */
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		super.onSharedPreferenceChanged(sharedPreferences, key);
		if (key.equals(SettingsPreferenceKeys.KEY_NOTE_ADAPTER_PREVIEW)) {
			noteListAdapter.notifyNoteStyleChanged();
		} else if (key.equals(SettingsPreferenceKeys.KEY_AUTO_SYNCHRONIZATION)) {
			initPeriodicSyncService();
		}
	}

	/**
	 * Needed to re-initialize the note- recyclerView
	 */
	@Override
	public void onConfigurationChanged(@NonNull Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

    /**
     * Returns the login parameters after launching overview activity from login
     *
     * @author Manuel Thöne
     */
    private void getLoginIntent() {
        Intent i = getIntent();
        password = i.getStringExtra(DataSharing.KEY_LOGIN_PASSWORD);
        emailAddress = i.getStringExtra(DataSharing.KEY_LOGIN_EMAIL);
    }

    private void initPeriodicSyncService() {
		SharedPreferences prefs = getSharedPreferences(SettingsPreferenceKeys.NAME_SETTINGS_PREFS, Context.MODE_PRIVATE);
		boolean doSync = prefs.getBoolean(SettingsPreferenceKeys.KEY_AUTO_SYNCHRONIZATION, false);
		Intent intent = new Intent(this, PeriodicSyncService.class);
		intent.putExtra(PeriodicSyncService.PASSKEY, password);
		isSyncing = true;
		Thread t = new Thread(() -> {
			if(doSync) {
				Log.d("Notiz", "Periodic Sync-Service started");
				startService(intent);
				periodicSyncServiceStarted = true;
			} else if(periodicSyncServiceStarted) {
				periodicSyncServiceStarted = stopService(intent);
				Log.d("Notiz", "Periodic Sync-Service stopped");
			}
			isSyncing = false;
		});
		t.start();

    }


	private void initNoteList(String passcode, String userMail, Context context) throws DatabaseKeyManager.DatabaseKeyException {
		noteManager = NoteManager.getInstance(context, this, passcode, userMail);
		noteListAdapter = new NoteListAdapter(context, this);
		RecyclerView noteList = findViewById(R.id.note_overview_list);
		noteList.setAdapter(noteListAdapter);

        ItemClickSupport.addTo(noteList).setOnItemClickListener((recyclerView, position, v) -> {
            Intent intent = new Intent(this, NoteDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(DataSharing.KEY_TAGS, noteManager.getCurrentTags());
            bundle.putParcelable(DataSharing.KEY_NOTE, noteListAdapter.getNote(position));
            bundle.putInt(DataSharing.KEY_POSITION, position);
            intent.putExtras(bundle);
            activityResultLauncher.launch(intent);
        });

    }

    /**
     * Initializs the resultLauncher <br>
     * Call inside onAttach or onCreate -> before activity is displayed!
     */

    // TODO: 25.03.2022 in mehrere Methoden aufteilen
    private void initResultLauncher() {
        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        if (data != null) {
                            Note resultNote = data.getParcelableExtra(DataSharing.KEY_NOTE);
                            String metaData = data.getStringExtra(DataSharing.KEY_META);
                            int notePosition = data.getIntExtra(DataSharing.KEY_POSITION, DataSharing.NO_POSITION);

                            if (metaData != null) {
                                if (resultNote != null) {
                                    if (metaData.equals(DataSharing.NOTE_SAVED)) {
                                        //adds the new note
                                        if (notePosition == DataSharing.NO_POSITION) {
                                            noteManager.addNote(resultNote);
                                        }//existing note was generally changed
                                        else {
                                            noteManager.updateNote(resultNote);
                                        }
                                        String currentTagFilter = noteListAdapter.getCurrentTagFilter();
                                        if (currentTagFilter.isEmpty()) {
                                            getSupportActionBar().setTitle(getResources().getString(R.string.overview));
                                        }
                                        else {
                                            getSupportActionBar().setTitle(currentTagFilter);
                                        }
                                    }
                                    //existing note was archived
                                    else if (metaData.equals(DataSharing.NOTE_ARCHIVED)) {
                                        noteManager.updateNote(resultNote);
                                    }
                                    //existing note was changed and unarchived
                                    else if (metaData.equals(DataSharing.NOTE_UNARCHIVED)) {
                                        noteManager.updateNote(resultNote);
                                    }
                                } else if (metaData.equals(DataSharing.NOTE_DELETED) && notePosition != DataSharing.NO_POSITION) {
                                    Log.d("Notiz", "in OverviewActivity.activityResultLauncher -> deleted = " + notePosition);
                                    noteManager.deleteNote(noteListAdapter.getNote(notePosition));
                                } else if (metaData.equals(DataSharing.TAG_EDIT)) {
                                    // Handle tag edit commands
                                    ArrayList<Tag> newTags = data.getParcelableArrayListExtra(DataSharing.KEY_NEW_TAGS);
                                    ArrayList<Tag> deletedTags = data.getParcelableArrayListExtra(DataSharing.KEY_DELETED_TAGS);
                                    ArrayList<Tag> editedTags = data.getParcelableArrayListExtra(DataSharing.KEY_EDITED_TAGS);

                                    if (newTags != null && !newTags.isEmpty()) {
                                        noteManager.addTags(newTags);
                                        Log.d("Notiz", "in OverviewActivity.activityResultLauncher -> newTags = " + newTags.toString());
                                    }
                                    if (deletedTags != null && !deletedTags.isEmpty()) {
                                        noteManager.deleteTags(deletedTags);
                                        Log.d("Notiz", "in OverviewActivity.activityResultLauncher -> deletedTags = " + deletedTags.toString());
                                    }
                                    if (editedTags != null && !editedTags.isEmpty()) {
                                        noteManager.updateTags(editedTags);
                                        Log.d("Notiz", "in OverviewActivity.activityResultLauncher -> editedTags = " + editedTags.toString());
                                    }
                                }
                            }
                            //TODO: REMOVE AFTER DEBUG
                            Log.d("Notiz", "in OverviewActivity.activityResultLauncher -> position = " + notePosition);
                            Log.d("Notiz", "in OverviewActivity.activityResultLauncher -> metaData = " + metaData);
                            if (resultNote != null) {
                                Log.d("Notiz", "in OverviewActivity.activityResultLauncher -> resultNote = " + resultNote.toString());
                            }
                        }


                    }


                });
    }


    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initDrawer() {
        mDrawer = findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        nvDrawer = findViewById(R.id.nav_view);
        setupDrawerContent(nvDrawer);
        // Setup hamburger icon with animation
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
        mDrawer.addDrawerListener(drawerToggle);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            selectDrawerItem(menuItem);
            mDrawer.closeDrawers();
            return true;
        });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Log.d("Notiz", "in selectDrawerItem() -> " + menuItem.getTitle().toString());
        int id = menuItem.getItemId();
        int groupId = menuItem.getGroupId();
        if (groupId == R.id.tag_menu) {
            String tagName = menuItem.getTitle().toString();
            getSupportActionBar().setTitle(tagName);        // Set actionbar title to currently selected tag
            noteListAdapter.setTagFilter(noteManager.getTagByName(tagName));
        } else {
            if (id == R.id.nav_overview) {
                getSupportActionBar().setTitle(getResources().getString(R.string.overview));        // Set actionbar title to currently selected tag
                noteListAdapter.setFilter(DataSharing.UN_ARCHIVED);

			} else if (id == R.id.nav_archiveActivity) {
				getSupportActionBar().setTitle(getResources().getString(R.string.archive));        // Set actionbar title to currently selected tag
				noteListAdapter.setFilter(DataSharing.ARCHIVED);

			} else if (id == R.id.nav_accountActivity) {
				Intent i = new Intent(getApplicationContext(), AccountActivity.class);
				Bundle bundle = new Bundle();
				// TODO: 27.03.2022 notemanager get counts!
				bundle.putInt(DataSharing.KEY_ACCOUNT_NOTE_COUNT, noteManager.getNoteCount());
				bundle.putInt(DataSharing.KEY_ACCOUNT_TAG_COUNT, noteManager.getTagCount());
				i.putExtras(bundle);
				startActivity(i);
			} else if (id == R.id.nav_settingsActivity) {
				Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
				startActivity(i);
			}
		}

		menuItem.setChecked(true);
		// Close the navigation drawer
	}

	private void initNavigationView() {
		NavigationView navigationView = findViewById(R.id.nav_view);
		initMenuButtons(navigationView);
		addTagsToNavigationMenu(navigationView);
	}

	/**
	 * Initializes the bottom navigation elements in the sidebar
	 * See: <a href="https://stackoverflow.com/questions/31722566/dynamic-adding-item-to-navigationview-in-android"> Add items programatically</a>
	 *
	 * @param navigationView the parent NavigationView which holds the menu
	 * @author Manuel Thöne
	 */
	private void addTagsToNavigationMenu(NavigationView navigationView) {
		Menu menu = navigationView.getMenu();
		SubMenu sm = menu.getItem(1).getSubMenu();
		sm.clear();
		for (Tag tag : noteManager.getCurrentTags()) {
			sm.add(R.id.tag_menu, 0, Menu.NONE, tag.getTagName()).setIcon(R.drawable.ic_outline_label_24);
		}
	}

	private void initMenuButtons(@NonNull NavigationView navigationView) {
		View menuDrawerTagButtonView = navigationView.getMenu().findItem(R.id.nav_edit_tags).getActionView();
		Button editTagButton = menuDrawerTagButtonView.findViewById(R.id.app_bar_edit_tag);
		Button addTagButton = menuDrawerTagButtonView.findViewById(R.id.app_bar_add_tag);

		editTagButton.setOnClickListener(view -> {
			Toast.makeText(getApplicationContext(), "onclick: editTagBtn", Toast.LENGTH_SHORT).show();
			// TODO: 12.03.2022 implement fragment call to edit tags
			Intent intent = new Intent(this, EditTagActivity.class);
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList(DataSharing.KEY_TAGS, noteManager.getCurrentTags());
			intent.putExtras(bundle);
			activityResultLauncher.launch(intent);
		});

		addTagButton.setOnClickListener(view -> {
			Toast.makeText(getApplicationContext(), "onclick: addTagBtn", Toast.LENGTH_SHORT).show();
			// TODO: 12.03.2022 implement fragment call to add tag
		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			mDrawer.openDrawer(GravityCompat.START);
			return true;
		} else if (id == R.id.app_bar_sync) {
			startManualSync();
			Toast.makeText(this, getResources().getString(R.string.syncing_started), Toast.LENGTH_SHORT).show();
			Log.d("Notiz", "onOptionsItemSelected -> Todo: Force Sync");
		} else if (item.getTitle() != null) {
			Log.d("Notiz", "onOptionsItemSelected -> Clicked: " + item.getTitle().toString());
		}
		return super.onOptionsItemSelected(item);
	}

    private void startManualSync() {
        Intent intent = new Intent(this, SyncService.class);
        intent.putExtra(SyncService.PASSKEY, password);

        Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
        		startService(intent);
			}
		});
        t.start();
    }

	/**
	 * Initializes the floating action button.<br>
	 * Onclick: Passes all available tags and starts the NoteDetailActivity for a result
	 *
	 * @author Manuel Thöne, Amin Yacine
	 */
	private void initFAB() {
		FloatingActionButton addNoteFab = findViewById(R.id.fab);
		addNoteFab.setOnClickListener(view -> {

			Intent intent = new Intent(this, NoteDetailActivity.class);
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList(DataSharing.KEY_TAGS, noteManager.getCurrentTags());
//            bundle.putParcelable(DataSharing.KEY_NOTE, createTestNote(8));
			intent.putExtras(bundle);
			activityResultLauncher.launch(intent);
		});
	}

	/**
	 * Handling of Toolbar items
	 *
	 * @author Manuel Thöne
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_action_bar_overview, menu);

		MenuItem searchItem = menu.findItem(R.id.app_bar_search);

		// Handle Search Bar Events:
		if (searchItem != null) {
			searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
				@Override
				public boolean onMenuItemActionExpand(MenuItem item) {
					Log.d("Notiz", "onMenuItemActionExpand -> Clicked: " + item.getTitle().toString());
					if (item.getItemId() == R.id.app_bar_search) {
						SearchView searchView = (SearchView) item.getActionView();
						searchView.setQueryHint(getResources().getString(R.string.search_in_notes));
						searchView.setOnQueryTextListener(OverviewActivity.this);
					}
					return true;
				}

				@Override
				public boolean onMenuItemActionCollapse(MenuItem item) {
					Log.d("Notiz", "onMenuItemActionCollapse -> Clicked: " + item.getTitle().toString());
					return true;
				}
			});
		}
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// Todo: Add method when search text submitted
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// Todo: Add method when search text changed
		return false;
	}


    @Override
    public void onNotesChanged() {
        noteListAdapter.setAllNotes(noteManager.getAllNotes());
    }


    /**
     * informs the adapter about edited and deleted tags to adjust the filter
     *
     * @param changedTags     the changed tags
     * @param operationOnTags describes if the tags were deleted or edited
     */
    @Override
    public void onTagsChanged(@NonNull ArrayList<Tag> changedTags, int operationOnTags) {
        initNavigationView();
        if (operationOnTags == DataSharing.TAGS_EDITED) {
            noteListAdapter.checkTagFilter(changedTags, DataSharing.TAGS_EDITED);
        } else if (operationOnTags == DataSharing.TAGS_DELETED) {
            noteListAdapter.checkTagFilter(changedTags, DataSharing.TAGS_DELETED);
        }
    }

	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}

	@Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(bReceiver, new IntentFilter(REFRESH_VIEW));
    }

    @Override
    protected void onPause (){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(bReceiver);
    }



    /**
     * sets the FilterName in the ActionBar to the updated tag name or if deleted
     * to the standard overview title
     *
     * @param newFilterName updated tag name
     */
    @Override
    public void onUsedTagFilterChanged(String newFilterName) {
        if (getSupportActionBar() != null) {
            if (newFilterName.isEmpty()) {
                getSupportActionBar().setTitle(R.string.overview);
            } else {
                getSupportActionBar().setTitle(newFilterName);
            }
        }
    }


}
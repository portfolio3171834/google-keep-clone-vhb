package com.passau.notizen.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.passau.notizen.R;
import com.passau.notizen.cryptography.DatabaseKeyManager;
import com.passau.notizen.database.NoteDatabaseService;
import com.passau.notizen.database.tables.User;
import com.passau.notizen.ui.BaseActivity;
import com.passau.notizen.ui.overview.OverviewActivity;
import com.passau.notizen.ui.util.DataSharing;

import java.time.LocalDateTime;


public class LoginActivity extends BaseActivity {

	NoteDatabaseService dbService = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// If user still has access to the database it will redirect him to the overview screen
		if (!route()) {
			setContentView(R.layout.activity_login);
		}

	}

	/**
	 * Calls the database and tries to decrypt it with the overhanded password.
	 * if the password is correct methode returns true otherwise false.
	 * also checks if user is already in Userdb if not creates one.
	 *
	 * @author Marco Wenger
	 */
	protected boolean dataBaseCall(String email, @NonNull String password) {
		User user = null;
		boolean success = false;

		try {
			dbService = NoteDatabaseService.getInstance(this, password.toCharArray());
			user = dbService.getUserByMail(email);
			success = true;
		} catch (Exception | DatabaseKeyManager.DatabaseKeyException ignored) {
		}

		if (success) {
			if (user == null) {
				dbService.addUser(new User(email, "", LocalDateTime.now()));
				String toastText = getResources().getString(R.string.account_added) + "\"" + email + "\"";
				Toast.makeText(this, toastText, Toast.LENGTH_LONG).show();
			}
			intentOverviewScreen(email, password);
			return true;
		}
		return false;
	}

	/**
	 * Intent to the Overview Activity with the given resources email, password
	 *
	 * @author Marco Wenger
	 */
	private void intentOverviewScreen(String email, String password) {
		Intent myIntent = new Intent(this, OverviewActivity.class);
		myIntent.putExtra(DataSharing.KEY_LOGIN_EMAIL, email);
		myIntent.putExtra(DataSharing.KEY_LOGIN_PASSWORD, password);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void onBackPressed() {
		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			getSupportFragmentManager().popBackStack();
		} else {
			finish();
		}
	}


	private boolean route() {
		String email = getLoggedInEmail();
		if (email != null) {
			boolean success = false;
			try {
				NoteDatabaseService.getInstance(this, "".toCharArray()).getUserByMail(email);
				success = true;
			} catch (Exception | DatabaseKeyManager.DatabaseKeyException ignored) {
			}
			if (success) {
				startOverviewIntent();
				return true;
			}
		}
		return false;
	}


	private void startOverviewIntent() {
		Intent myIntent = new Intent(this, OverviewActivity.class);
		startActivity(myIntent);
		finish();
	}

	private String getLoggedInEmail() {
		SharedPreferences prefs = getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
		return prefs.getString(LoginPreferenceKeys.KEY_EMAIL, null);
	}
}

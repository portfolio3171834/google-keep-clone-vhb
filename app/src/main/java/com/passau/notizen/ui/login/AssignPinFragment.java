package com.passau.notizen.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.passau.notizen.R;
import com.passau.notizen.ui.overview.OverviewActivity;
import com.passau.notizen.ui.util.DataSharing;

public class AssignPinFragment extends Fragment {

    private final static int PIN_MIN_LENGTH = 4;
    private final static int PIN_MAX_LENGTH = 12;

    Button buttonCancel,buttonAssign;
    EditText pinEditText;
    TextView pinMsgTextView;
    String personEmail,pin;




    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assign_pin, container ,false);

        /* set the variables*/
        pinEditText = view.findViewById(R.id.assign_pin_edittext);
        pinMsgTextView = view.findViewById(R.id.assign_pin_message_textview);
        buttonAssign = view.findViewById(R.id.button_assign_pin);
        buttonCancel = view.findViewById(R.id.button_sign_in_cancel);

        /* button for assign the Pin*/
        buttonAssign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPrefPinAssign();
            }
        });

        /* button to go back one screen*/
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBackStack();
            }
        });

        // get google Email
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(requireActivity());
        if (acct != null) {
            personEmail = acct.getEmail();
        }

        return view;
    }

    /**
     * Checks the length of the pin to see if it meets the specifications
     * if the pin meets the requirements, uses it for the database
     * @author Marco Wenger
     */
    private boolean checkLength() {
        String pin = pinEditText.getText().toString();
        if (pin.isEmpty()) {
            String text = getResources().getString(R.string.please_enter_pin);
            pinMsgTextView.setText(text);
            return false;
        }else if(pin.length() < PIN_MIN_LENGTH){
            String text = getResources().getString(R.string.pin_too_short)  + PIN_MIN_LENGTH;
            pinMsgTextView.setText(text);
            pinEditText.clearComposingText();
            return false;
        }else if(pin.length() > PIN_MAX_LENGTH){
            String text = getResources().getString(R.string.pin_too_long)  + PIN_MAX_LENGTH;
            pinMsgTextView.setText(text);
            pinEditText.clearComposingText();
            return false;
        }else{
            if(sharedPrefEmailLogin(personEmail)){
                if(requireActivity() instanceof LoginActivity){
                    ((LoginActivity)requireActivity()).dataBaseCall(personEmail, pin);
                }
            }
            return true;
        }
    }

    /**
     * Intent to the Overview Screen hands over the Email
     * @author Marco Wenger
     */
    private void intentOverviewScreen(){
            Intent myIntent = new Intent(requireActivity(), OverviewActivity.class);
            myIntent.putExtra(DataSharing.KEY_LOGIN_EMAIL, personEmail);
            myIntent.putExtra(DataSharing.KEY_LOGIN_PASSWORD, pin);
            startActivity(myIntent);
    }

    /**
     * Sets SharedPref for PinAssigned true if the pin meets the requirements
     * @author Marco Wenger
     */
    private void sharedPrefPinAssign(){
        if (checkLength()){
            SharedPreferences prefs = requireActivity().getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(LoginPreferenceKeys.KEY_PIN_ASSIGNED, true);
            editor.putInt(LoginPreferenceKeys.KEY_USER_TYPE, LoginPreferenceKeys.USER_TYPE_GOOGLE);
            editor.apply();
        }
    }

    /**
     * checks whether an email already exists in SharedPref or Implements it
     *
     * @author Marco Wenger
     */
    private boolean sharedPrefEmailLogin(String email) {

        SharedPreferences prefs = requireActivity().getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
        String i = prefs.getString(LoginPreferenceKeys.KEY_EMAIL, null);
        if (i == null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(LoginPreferenceKeys.KEY_EMAIL, email);
            editor.apply();
            return true;
        } else if (i.equals(email)) {
            String text = getResources().getString(R.string.user_already_assigned);
            pinMsgTextView.setText(text);
            return false;
        } else {
            return false;
        }
    }

    /**
     * Changes the Fragments to previous.
     *
     * @author Marco Wenger
     */
    private void popBackStack(){
        requireActivity().getSupportFragmentManager().popBackStack();
    }


}
package com.passau.notizen.ui.notedetail.adapters;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.passau.notizen.R;
import com.passau.notizen.database.tables.Tag;

import java.util.ArrayList;

public class RecyclerViewTagAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private static final int TYPE_UNCHECK = 0;
	private static final int TYPE_CHECKED = 1;
	private ArrayList<Tag> availableTags;
	private ArrayList<Tag> selectedTags;

	public void logLists() {
		Log.d("Notiz", "in Adapter: availableTags = " + availableTags.toString());
		Log.d("Notiz", "in Adapter: selectedTags = " + selectedTags.toString());
	}

	public RecyclerViewTagAdapter() {
		this.availableTags = new ArrayList<>();
		this.selectedTags = new ArrayList<>();
	}

	public void setAvailableTags(@NonNull ArrayList<Tag> availableTags) {
		this.availableTags = null;
		this.availableTags = new ArrayList<>(availableTags);
		notifyDataSetChanged();
	}

	public void setSelectedTags(@NonNull ArrayList<Tag> selectedTags) {
		this.selectedTags = null;
		this.selectedTags = new ArrayList<>(selectedTags);
		notifyDataSetChanged();
	}

	public Tag getTag(int position) {
		return availableTags.get(position);
	}


	/**
	 * Adds a new Tag at the beginning to both "avilableTags" and "selectedTags"
	 */
	public void addTag(@NonNull Tag tag) {
		if (!availableTags.contains(tag)) {
			this.availableTags.add(0, tag);
		}
		if (!selectedTags.contains(tag)) {
			this.selectedTags.add(0, tag);
		}
		notifyItemInserted(0);
	}


	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		if (viewType == TYPE_CHECKED) {
			View itemViewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_note_tag_selection_view_checked, parent, false);
			return new ViewHolderChecked(itemViewLayout);
		}
		View itemViewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_note_tag_selection_view, parent, false);
		return new ViewHolderUnChecked(itemViewLayout);
	}

	@Override
	public int getItemViewType(int position) {
		Tag tag = availableTags.get(position);

		if (selectedTags.contains(tag)) {
			return TYPE_CHECKED;
		} else {
			return TYPE_UNCHECK;
		}
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		Tag tag = availableTags.get(position);
		if (holder instanceof ViewHolderUnChecked) {
			((ViewHolderUnChecked) holder).checkedTextView.setText(tag.getTagName());
		} else if (holder instanceof ViewHolderChecked) {
			((ViewHolderChecked) holder).checkedTextView.setText(tag.getTagName());
		}
	}

	@Override
	public int getItemCount() {
		return availableTags.size();
	}

    public void toggleSelectedTag(Tag tag, int position) {
		if(!selectedTags.remove(tag)) {
			selectedTags.add(tag);
		}
		notifyItemChanged(position);
    }


    static class ViewHolderChecked extends RecyclerView.ViewHolder {
		public CheckedTextView checkedTextView;

		public ViewHolderChecked(@NonNull View itemView) {
			super(itemView);
			checkedTextView = itemView.findViewById(R.id.tag_checked_textview);
		}
	}

	static class ViewHolderUnChecked extends RecyclerView.ViewHolder {
		public CheckedTextView checkedTextView;

		public ViewHolderUnChecked(@NonNull View itemView) {
			super(itemView);
			checkedTextView = itemView.findViewById(R.id.tag_checked_textview);
		}
	}
}

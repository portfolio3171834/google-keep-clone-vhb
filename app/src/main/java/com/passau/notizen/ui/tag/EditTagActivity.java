package com.passau.notizen.ui.tag;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.passau.notizen.R;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.ui.BaseActivity;
import com.passau.notizen.ui.util.DataSharing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;


public class EditTagActivity extends BaseActivity implements RecyclerViewTagEditAdapter.TagAdapterListener {

	private final ArrayList<Tag> allAvailableTags = new ArrayList<>();
	private final HashMap<UUID, Tag> deletedTags = new HashMap<>();
	private final HashMap<UUID, Tag> newTags = new HashMap<>();
	private final HashMap<UUID, Tag> editedTags = new HashMap<>();

	private RecyclerViewTagEditAdapter tagEditAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handleIntent();
		initUI();
	}

	private void handleIntent() {
		Intent i = getIntent();
		ArrayList<Tag> parcelableTags = i.getParcelableArrayListExtra(DataSharing.KEY_TAGS);
		if (parcelableTags != null) {
			allAvailableTags.addAll(parcelableTags);
		}
	}

	private void initUI() {
		setContentView(R.layout.activity_edit_tag);
		initActionBar();
		initTagInput();
		showAndInitRecyclerViewAdapter();
	}

	private void showAndInitRecyclerViewAdapter() {
		RecyclerView recyclerView = findViewById(R.id.tag_edit_recyclerview);
		tagEditAdapter = new RecyclerViewTagEditAdapter(allAvailableTags, this, this);
		recyclerView.setAdapter(tagEditAdapter);
		printAllLists("initRecycler");
	}

	private void addNewTag(Tag newTag) {
		if (newTag != null) {
			allAvailableTags.add(0, newTag);
			newTags.put(newTag.getId(), newTag);
			tagEditAdapter.notifyItemInserted(0);
			tagEditAdapter.notifyItemRangeChanged(0, tagEditAdapter.getItemCount());
		}
	}

	private void initTagInput() {
		EditText tagInputText = findViewById(R.id.add_tag_edittext);
		ImageButton addTagBtn = findViewById(R.id.add_tag_imagebutton);
		disableImageButton(addTagBtn);

		tagInputText.setOnFocusChangeListener((v, hasFocus) -> {
			if (!hasFocus) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			}
		});

		// Enter the tag on "Enter" input
		tagInputText.setOnEditorActionListener((v, actionId, event) -> {
			boolean handled = false;
			if (actionId == EditorInfo.IME_ACTION_NEXT) {
				addTagBtn.performClick();
				handled = true;
			}
			return handled;
		});

		tagInputText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.toString().trim().length() > 0) {
					enableImageButton(addTagBtn);
				} else {
					disableImageButton(addTagBtn);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		addTagBtn.setOnClickListener(v -> {
			String newTagName = tagInputText.getText().toString().trim();
			if (newTagName.isEmpty()) return;
			Tag newTag = new Tag(UUID.randomUUID(), newTagName);

			if (allAvailableTags.contains(newTag)) {
				Toast.makeText(EditTagActivity.this, getResources().getString(R.string.tagname_already_exists), Toast.LENGTH_LONG).show();
			} else {
				addNewTag(newTag);
				printAllLists("onTagChanged()");
			}
			tagInputText.setText("");
			tagInputText.clearFocus();
			disableImageButton((ImageButton) v);
		});
	}

	private void enableImageButton(ImageButton button) {
		button.setEnabled(true);
		button.setClickable(true);
		button.setColorFilter(ContextCompat.getColor(this, R.color.primary_500));
	}

	private void disableImageButton(ImageButton button) {
		button.setEnabled(false);
		button.setClickable(false);
		button.setColorFilter(ContextCompat.getColor(this, R.color.grey_700));
	}

	@Override
	public void onTagChanged(Tag tag, int position) {
		allAvailableTags.set(position, tag);
		if (newTags.containsKey(tag.getId())) {
			newTags.put(tag.getId(), tag);
		} else {
			editedTags.put(tag.getId(), tag);
		}
		printAllLists("onTagChanged()");
	}

	@Override
	public void onTagRemoved(Tag tag, int position) {
		editedTags.remove(tag.getId());
		if (!newTags.containsKey(tag.getId())) {
			deletedTags.put(tag.getId(), tag);
		}
		newTags.remove(tag.getId());
		printAllLists("onTagRemoved()");
	}

	// TODO: 25.03.2022 remove debug
	private void printAllLists(String location) {
		Log.d("Notiz", location + " allAvailableTags = " + allAvailableTags.toString());
		Log.d("Notiz", location + " editedTags = " + editedTags.toString());
		Log.d("Notiz", location + " newTags = " + newTags.toString());
		Log.d("Notiz", location + " deletedTags = " + deletedTags.toString() + "\n-------------------------------------------");
	}

	private void setNoteAsResultAndFinish() {
		Log.d("Notiz", "EditTag -> pressedBack()");
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putString(DataSharing.KEY_META, DataSharing.TAG_EDIT);
		if(newTags!=null && !newTags.isEmpty()) {
			bundle.putParcelableArrayList(DataSharing.KEY_NEW_TAGS, new ArrayList<>(newTags.values()));
		}
		if(deletedTags!=null && !deletedTags.isEmpty()) {
			bundle.putParcelableArrayList(DataSharing.KEY_DELETED_TAGS, new ArrayList<>(deletedTags.values()));
		}
		if(editedTags!=null && !editedTags.isEmpty()) {
			bundle.putParcelableArrayList(DataSharing.KEY_EDITED_TAGS, new ArrayList<>(editedTags.values()));
		}
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	protected void initActionBar() {
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setNavigationOnClickListener(v -> setNoteAsResultAndFinish());
	}

	@Override
	public void onBackPressed() {
		setNoteAsResultAndFinish();
	}
}
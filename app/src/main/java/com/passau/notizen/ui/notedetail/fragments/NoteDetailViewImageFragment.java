package com.passau.notizen.ui.notedetail.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.passau.notizen.R;
import com.passau.notizen.data.AttachmentThumbnail;
import com.passau.notizen.ui.notedetail.AttachmentManager;
import com.passau.notizen.ui.notedetail.NoteDetailActivity;
import com.passau.notizen.ui.notedetail.NoteDetailSharedViewModel;

public class NoteDetailViewImageFragment extends Fragment implements NoteDetailSharedViewModel.AttachmentObserverListener {

	public static final String FRAGMENT_TAG = "note_detail_image_view_fragment";

	protected static final String ARGUMENT_IMAGE_NAME = "filename";

	protected Context context;
	private View fragmentView;
	private FragmentActivity activity;
	private NoteDetailSharedViewModel model;

	// TODO: Rename and change types of parameters
	private String imageName;


	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		if (this.context == null && context instanceof NoteDetailActivity) {
			this.context = context;    // Sets the context
		}
	}

	@Override
	public void onDetach() {
		this.context = null;
		super.onDetach();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			imageName = getArguments().getString(ARGUMENT_IMAGE_NAME);
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, @Nullable Bundle savedInstanceState) {

		model = new ViewModelProvider(requireActivity()).get(NoteDetailSharedViewModel.class);
		activity = requireActivity();

		setHasOptionsMenu(true);
		fragmentView = inflater.inflate(R.layout.fragment_note_detail_view_image, root, false);

		initUI();

		return fragmentView;
	}

	/**
	 * Initializes the UI
	 *
	 * @author Manuel Thöne
	 */
	private void initUI() {
		setHasOptionsMenu(true);
		initActionBar();
		initImage();
	}


	private void initImage() {
		SubsamplingScaleImageView imageView = fragmentView.findViewById(R.id.scale_image_view);
		Bitmap lowResImage = AttachmentManager.getThumbnailByNameFromInternalStorage(context, imageName, 600, 800);
		if (lowResImage != null) {
			imageView.setImage(ImageSource.bitmap(lowResImage));
		}

		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				Bitmap img = AttachmentManager.getImageByNameFromInternalStorage(context, imageName);
				onFullResImageLoaded(img);
			}
		});
		t.start();
	}

	public void onFullResImageLoaded(Bitmap image) {
		//Use "fixed" activity -> not requireActivity to prevent "not attached to fragment exception
		// Fails when the imageview is closed, before the full res image was loaded
		// TODO: 21.03.2022 find better alternative for this "fix" !
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				SubsamplingScaleImageView imageView = fragmentView.findViewById(R.id.scale_image_view);
				if (image != null) {
					imageView.setImage(ImageSource.bitmap(image));
				} else {
					Toast.makeText(context, "No Image found", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	/**
	 * Sets up the action bar and the back button
	 *
	 * @author Manuel Thöne
	 */
	private void initActionBar() {
		Toolbar toolbar = fragmentView.findViewById(R.id.imageview_toolbar);
		((AppCompatActivity) context).setSupportActionBar(toolbar);
		ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayShowTitleEnabled(false);
		}

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				requireActivity().getSupportFragmentManager().popBackStack();
			}
		});
	}

	// Inflates the action bar
	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.menu_actionbar_attachment_view, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	/**
	 * Handles the onclick of menu items in the top action bar and the attachment bar
	 *
	 * @author Manuel Thöne
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d("Notiz", "clicked: " + item.getTitle());

		int id = item.getItemId();
		if (id == R.id.attachment_view_delete) {
			boolean removed = model.removeImageAttachment(imageName, this);
			if (removed) {
				Toast.makeText(context, context.getString(R.string.image_deleted_toast), Toast.LENGTH_SHORT).show();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onAttachmentViewFileDelete() {
		requireActivity().getSupportFragmentManager().popBackStack();
	}

	@Override
	public void onExistingNoteAttachmentThumbnailLoaded(AttachmentThumbnail thumbnail) {
	}
}

package com.passau.notizen.ui.settings;

import android.content.Context;
import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.passau.notizen.R;
import com.passau.notizen.ui.BaseActivity;

public class SettingsActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initUI(savedInstanceState);
	}

	private void initUI(Bundle savedInstanceState) {
		setContentView(R.layout.activity_settings);
		initActionBar();
		if (savedInstanceState == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.settings, new SettingsFragment())
					.commit();
		}
	}

	/**
	 * Handles and displays the application settings
	 *
	 * @author Manuel Thöne
	 */
	public static class SettingsFragment extends PreferenceFragmentCompat {
		@Override
		public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
			PreferenceManager prefMgr = getPreferenceManager();
			prefMgr.setSharedPreferencesName(SettingsPreferenceKeys.NAME_SETTINGS_PREFS);
			prefMgr.setSharedPreferencesMode(Context.MODE_PRIVATE);
			setPreferencesFromResource(R.xml.root_preferences, rootKey);
		}
	}
}
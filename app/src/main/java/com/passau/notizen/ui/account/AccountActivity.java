package com.passau.notizen.ui.account;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.snackbar.Snackbar;
import com.passau.notizen.R;
import com.passau.notizen.ui.BaseActivity;
import com.passau.notizen.ui.login.LoginPreferenceKeys;
import com.passau.notizen.ui.util.DataSharing;

public class AccountActivity extends BaseActivity implements View.OnClickListener {
	private TextView changeMailTextView, changePasswordTextView, changePinTextView, logoutTextView, deleteDBTextView;

	private int noteCount, tagCount, userType;
	private String activeUserAddress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initUI();
	}

	private void initUI() {
		setContentView(R.layout.activity_account);
		initActionBar();
		getData();
		initUserInformationViews();
		initOnClickListener();
	}

	private void getData() {
		Bundle bundle = getIntent().getExtras();
		noteCount = bundle.getInt(DataSharing.KEY_ACCOUNT_NOTE_COUNT, DataSharing.NO_POSITION);
		tagCount = bundle.getInt(DataSharing.KEY_ACCOUNT_TAG_COUNT, DataSharing.NO_POSITION);
		SharedPreferences loginPrefs = getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
		activeUserAddress = loginPrefs.getString(LoginPreferenceKeys.KEY_EMAIL, null);
		userType = loginPrefs.getInt(LoginPreferenceKeys.KEY_USER_TYPE, LoginPreferenceKeys.DEFAULT_ERROR_RETURN);

	}

	/**
	 * Initializes the User Information with email-address, note- and tag count.
	 *
	 * @author Manuel Thöne
	 */
	private void initUserInformationViews() {
		TextView emailTextView = findViewById(R.id.account_email_textview);
		TextView noteCountTextView = findViewById(R.id.account_note_count_textview);
		TextView tagCountTextView = findViewById(R.id.account_tag_count_textview);

		if (activeUserAddress != null && !activeUserAddress.isEmpty()) {
			emailTextView.setText(activeUserAddress);
		} else {
			emailTextView.setVisibility(View.GONE);
		}

		String whiteSpace = " ";
		if (noteCount >= 0) {
			String noteString = noteCount + whiteSpace + ((noteCount == 1) ? getString(R.string.note_singular) : getString(R.string.note_plural));
			noteCountTextView.setText(noteString);
		} else {
			noteCountTextView.setVisibility(View.GONE);
		}
		if (tagCount >= 0) {
			String tagString = tagCount + whiteSpace + ((tagCount == 1) ? getString(R.string.tag_singular) : getString(R.string.tag_plural));
			tagCountTextView.setText(tagString);
		} else {
			tagCountTextView.setVisibility(View.GONE);
		}

		if (userType == LoginPreferenceKeys.USER_TYPE_GOOGLE) {
			showGoogleParameters();
		}

	}

	/**
	 * Initializes onClickListener on all Account items
	 *
	 * @author Manuel Thöne
	 */
	private void initOnClickListener() {
		changeMailTextView = findViewById(R.id.account_change_mail_textview);
		changeMailTextView.setOnClickListener(this);
		changePasswordTextView = findViewById(R.id.account_change_pw_textview);
		changePasswordTextView.setOnClickListener(this);
		changePinTextView = findViewById(R.id.account_change_pin_textview);
		changePinTextView.setOnClickListener(this);
		logoutTextView = findViewById(R.id.account_logout_textview);
		logoutTextView.setOnClickListener(this);
		deleteDBTextView = findViewById(R.id.account_delete_database_textview);
		deleteDBTextView.setOnClickListener(this);
	}


	/**
	 * Shows different account options depending on the user type (local or google)
	 *
	 * @author Manuel Thöne
	 */
	private void showGoogleParameters() {
		LinearLayout locParameters = findViewById(R.id.account_local_user_parameters_include);
		LinearLayout googleParameters = findViewById(R.id.account_google_user_parameters_include);

		locParameters.setVisibility(View.GONE);
		googleParameters.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		if (v.equals(changeMailTextView)) {
			// TODO: 13.03.2022 Method for mail change
			Toast.makeText(this, "Clicked: " + ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
		} else if (v.equals(changePasswordTextView)) {
			// TODO: 13.03.2022 Method for pw change
			Toast.makeText(this, "Clicked: " + ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
		} else if (v.equals(changePinTextView)) {
			// TODO: 13.03.2022 Method for pin change
			Toast.makeText(this, "Clicked: " + ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
		} else if (v.equals(logoutTextView)) {
			// TODO: 13.03.2022 Method for logout
			Toast.makeText(this, "Clicked: " + ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
		} else if (v.equals(deleteDBTextView)) {
			removeAppData();
		}
	}

	private void removeAppData() {

		AlertDialog dialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
		String message = getResources().getString(R.string.delete_godmode);

		builder.setMessage(message)
				.setTitle(getResources().getString(R.string.delete_godmode_title));
		builder.setPositiveButton(getResources().getString(R.string.delete_godmode_title), (dialogInterface, which) -> {
			((ActivityManager)this.getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData();
		});
		builder.setNegativeButton(R.string.cancel, (dialogInterface, which) -> dialogInterface.dismiss());
		dialog = builder.create();
		dialog.show();
	}
}
package com.passau.notizen.ui.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.passau.notizen.R;

public class LoginRegisterFragment extends Fragment {

	private static final int PW_MIN_LENGTH = 8;
	Button buttonCancel, buttonRegister;
	EditText etEmail, etPassword, etPasswordRepeat;
	String email, password, passwordRepeat;
	TextView registerMsgTextView;

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_login_register, container, false);
		etEmail = view.findViewById(R.id.register_email_edittext);
		etPassword = view.findViewById(R.id.register_password_edittext);
		etPasswordRepeat = view.findViewById(R.id.register_password_repeat_edittext);
		registerMsgTextView = view.findViewById(R.id.register_message_textview);

		buttonCancel = view.findViewById(R.id.button_register_cancel);
		buttonRegister = view.findViewById(R.id.button_register);

		/* Button for Sign Up */
		buttonRegister.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				email = etEmail.getText().toString().trim();
				password = etPassword.getText().toString().trim();
				passwordRepeat = etPasswordRepeat.getText().toString().trim();
				checkRegisterData(email, password, passwordRepeat);

			}
		});

		/* Button to go back to previous screen */
		buttonCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				popBackStack();
			}
		});

		return view;
	}

	/**
	 * checks if the register data are valid
	 *
	 * @author Marco Wenger
	 */
	private void checkRegisterData(String email, String p1, String p2) {
		/* checks if user is already used */
		if (isEmailInSharedPrefs()) {
			String text = getResources().getString(R.string.an_account_already_exists);
			registerMsgTextView.setText(text);
		} else {
			//CHECK PASSWORD
			/* First checks entered password if meets requirements*/
			if (email.isEmpty()) {
				String text = getResources().getString(R.string.enter_email);
				registerMsgTextView.setText(text);

			} else if (!p1.equals(p2)) {
				String text = getResources().getString(R.string.password_dont_match);
				registerMsgTextView.setText(text);
			} else if (p1.isEmpty() || p1.length() < PW_MIN_LENGTH) {
				String text = getResources().getString(R.string.password_not_meet_requirements) + " " + PW_MIN_LENGTH + " " + getResources().getString(R.string.characters);
				registerMsgTextView.setText(text);
			} else {
				setSharedPrefLocalUser(email);
				if (requireActivity() instanceof LoginActivity) {
					((LoginActivity) requireActivity()).dataBaseCall(email, p1);
				}

			}
		}

	}


	/**
	 * checks if any email already exists
	 *
	 * @author Marco Wenger
	 */
	private boolean isEmailInSharedPrefs() {
		SharedPreferences prefs = requireActivity().getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
		return prefs.getString(LoginPreferenceKeys.KEY_EMAIL, null) != null;
	}

	/**
	 * checks or implements the email in a SharedPref data called login_preferences
	 *
	 * @author Marco Wenger
	 */
	private void setSharedPrefLocalUser(String email) {
		SharedPreferences prefs = requireActivity().getSharedPreferences(LoginPreferenceKeys.NAME_LOGIN_PREFS, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(LoginPreferenceKeys.KEY_EMAIL, email);
		editor.putInt(LoginPreferenceKeys.KEY_USER_TYPE, LoginPreferenceKeys.USER_TYPE_LOCAL);
		editor.apply();
	}

	/**
	 * Changes the Fragments to previous.
	 *
	 * @author Marco Wenger
	 */
	private void popBackStack() {
		requireActivity().getSupportFragmentManager().popBackStack();
	}
}

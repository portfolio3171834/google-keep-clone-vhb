package com.passau.notizen.ui.login;

public abstract class LoginPreferenceKeys {

	public static final String KEY_PIN_ASSIGNED = "Pin_Assigned";
	public static final String KEY_USER_TYPE = "user_type";
	public static final String KEY_EMAIL = "Email";

	public static final int DEFAULT_ERROR_RETURN = -1;
	public static final int USER_TYPE_LOCAL = 0;
	public static final int USER_TYPE_GOOGLE = 1;

	public static final String NAME_LOGIN_PREFS = "login_preferences";
}

package com.passau.notizen.ui.notedetail.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.passau.notizen.R;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.ui.notedetail.NoteDetailActivity;
import com.passau.notizen.ui.notedetail.NoteDetailSharedViewModel;
import com.passau.notizen.ui.notedetail.adapters.RecyclerViewTagAdapter;
import com.passau.notizen.ui.util.ItemClickSupport;

import java.util.ArrayList;
import java.util.UUID;

public class NoteDetailTagFragment extends Fragment implements ItemClickSupport.OnItemClickListener {

	protected Context context;
	private View fragmentView;
	private RecyclerView tagRecyclerView;
	private RecyclerViewTagAdapter tagAdapter;
	private NoteDetailSharedViewModel model;
	boolean isTagInitialized;
	boolean clickedOnTagItem;

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		if (this.context == null && context instanceof NoteDetailActivity) {
			this.context = context;    // Sets the context
		}
	}

	@Override
	public void onDetach() {
		this.context = null;
		super.onDetach();
	}

	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {

		model = new ViewModelProvider(requireActivity()).get(NoteDetailSharedViewModel.class);

		setHasOptionsMenu(true);
		fragmentView = inflater.inflate(R.layout.fragment_note_detail_tag, container, false);

		initActionBar();
		initTagInput();
		initTagRecycler();
		return fragmentView;

	}

	private void initTagInput() {
		EditText tagInputText = fragmentView.findViewById(R.id.add_tag_edittext);
		ImageButton addTagBtn = fragmentView.findViewById(R.id.add_tag_imagebutton);
		disableImageButton(addTagBtn);

		tagInputText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				}
			}
		});

		// Enter the tag on "Enter" input
		tagInputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				boolean handled = false;
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					addTagBtn.performClick();
					handled = true;
				}
				return handled;
			}
		});

		tagInputText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() > 0) {
					enableImageButton(addTagBtn);
				} else {
					disableImageButton(addTagBtn);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		addTagBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String newTagName = tagInputText.getText().toString();
				Tag newTag = new Tag(UUID.randomUUID(), newTagName);

				if(model.allAvailableTagsContain(newTag)) {
					Toast.makeText(context, getResources().getString(R.string.tagname_already_exists), Toast.LENGTH_LONG).show();
				} else {
					model.addNewSelectedTag(newTag);
					model.addToAllAvailableTags(newTag);
				}
				tagInputText.setText("");
				tagInputText.clearFocus();
				disableImageButton((ImageButton) v);
			}
		});
	}

	private void enableImageButton(ImageButton button) {
		button.setEnabled(true);
		button.setClickable(true);
		button.setColorFilter(ContextCompat.getColor(context, R.color.primary_500));
	}

	private void disableImageButton(ImageButton button) {
		button.setEnabled(false);
		button.setClickable(false);
		button.setColorFilter(ContextCompat.getColor(context, R.color.grey_700));
	}

	/**
	 * Initializes the tag adapter
	 *
	 * @author Manuel Thöne
	 */
	private void initTagRecycler() {
		tagAdapter = new RecyclerViewTagAdapter();
		tagRecyclerView = fragmentView.findViewById(R.id.tag_edit_recyclerview);
		tagRecyclerView.setAdapter(tagAdapter);
		ItemClickSupport.addTo(tagRecyclerView).setOnItemClickListener(this);

		model.getAllAvailableTags().observe(getViewLifecycleOwner(), new Observer<ArrayList<Tag>>() {
			@Override
			public void onChanged(ArrayList<Tag> tags) {
				if(!isTagInitialized) {
					tagAdapter.setAvailableTags(tags);
					isTagInitialized = true;
				} else {
					Tag tag = tags.get(tags.size()-1);
					tagAdapter.addTag(tag);
					tagRecyclerView.scrollToPosition(0);
				}
			}
		});

		model.getSelectedTags().observe(getViewLifecycleOwner(), new Observer<ArrayList<Tag>>() {
			@Override
			public void onChanged(ArrayList<Tag> tags) {
				if(! clickedOnTagItem && !tags.isEmpty()) {
					Log.d("Notiz", "in selectedTags:Observer -> selectedTags = " + tags.toString());
					tagAdapter.setSelectedTags(tags);
					tagRecyclerView.scrollToPosition(0);
				}
			}
		});
	}

	// Inflates the action bar
	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
		menu.clear();
		super.onCreateOptionsMenu(menu, inflater);
	}

	/**
	 * Sets up the action bar and the back button
	 *
	 * @author Manuel Thöne
	 */
	private void initActionBar() {
		Toolbar toolbar = fragmentView.findViewById(R.id.tag_edit_toolbar);
		((AppCompatActivity) context).setSupportActionBar(toolbar);
		ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayShowTitleEnabled(false);
		}

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				requireActivity().getSupportFragmentManager().popBackStack();
			}
		});
	}

	// Setup item click for imageAttachmentAdapter
	@Override
	public void onItemClicked(RecyclerView recyclerView, int position, View v) {
		// TODO: 22.03.2022 mark as checked
		Tag tag = tagAdapter.getTag(position);
		if(tag != null) {
			clickedOnTagItem = true;
			model.toggleSelectedTag(tag);
			tagAdapter.toggleSelectedTag(tag,position);
			clickedOnTagItem = false;
			// TODO: 23.03.2022 still massive hack!
		}
	}
}
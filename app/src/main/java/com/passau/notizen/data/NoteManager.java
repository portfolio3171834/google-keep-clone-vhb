package com.passau.notizen.data;

import android.content.Context;

import androidx.annotation.NonNull;

import com.passau.notizen.cryptography.DatabaseKeyManager;
import com.passau.notizen.database.NoteDatabaseService;
import com.passau.notizen.database.tables.BaseNote;
import com.passau.notizen.database.tables.MediaObject;
import com.passau.notizen.database.tables.NoteTag;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.database.tables.User;
import com.passau.notizen.ui.util.DataSharing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;
import java.util.stream.Collectors;

public class NoteManager {

	private static NoteManager noteManager;
	private final NoteDatabaseService dbService;

	private final ArrayList<Note> allNotes;
	private ArrayList<Tag> allTags;
	public final User user;

	private final NoteManagerListener listener;


	private NoteManager(Context context, NoteManagerListener listener, String passcode, String userEmail) throws DatabaseKeyManager.DatabaseKeyException {
		this.listener = listener;
		this.dbService = NoteDatabaseService.getInstance(context, passcode.toCharArray());

//        UUID noteId = UUID.randomUUID();
//        UUID tagId = UUID.randomUUID();
//        dbService.addUser(new User("a@a.com", "rootPath", LocalDateTime.now()));
//        dbService.addBaseNote(new BaseNote(noteId,"unpinned", "archived", LocalDateTime.now(),"a@a.com", true, false, null ));
//        dbService.addMediaObject(new MediaObject(UUID.randomUUID(), "localPath", MediaObject.MediaType.IMAGE, noteId));
//        dbService.addTag(new Tag(tagId, "TagName"));
//        dbService.addTagToBaseNote(new NoteTag(noteId, tagId));


		user = dbService.getUserByMail(userEmail);

		allTags = new ArrayList<Tag>();
		allNotes = new ArrayList<Note>();
		initNoteLists();
	}

	public static NoteManager getInstance(Context context, NoteManagerListener listener, String passcode, String userEmail) throws DatabaseKeyManager.DatabaseKeyException {
		if (noteManager == null) {
			noteManager = new NoteManager(context, listener, passcode, userEmail);
			return new NoteManager(context, listener, passcode, userEmail);
		}
		return noteManager;
	}


	/**
	 * generates the UI Note list by getting every BaseNote from DB and
	 * retrieving the correspondent Tags and MediaObjects
	 */

	public void initNoteLists() {
		allTags = dbService.getAllTags();
		ArrayList<BaseNote> baseNotes = dbService.getAllNotes();

		baseNotes.forEach(baseNote -> {
			ArrayList<MediaObject> noteMediaObjects = getMediaObjectsByNoteId(baseNote.getId());
			ArrayList<Tag> noteTags = getTagsByNoteId(baseNote.getId());
			Note note = new Note(baseNote.getId(), baseNote.getHeadline(), baseNote.getBody(), baseNote.getEmail(),
					baseNote.isArchived(), baseNote.isPinned(), baseNote.getLastChange(), baseNote.getDueDate(), noteMediaObjects, noteTags);
			allNotes.add(note);
		});
	}

	private ArrayList<MediaObject> getMediaObjectsByNoteId(UUID noteId) {
		ArrayList<MediaObject> allMediaObjects = dbService.getAllMediaObjects();
		ArrayList<MediaObject> mediaObjects = new ArrayList<>();
		for (MediaObject o : allMediaObjects) {
			UUID mediaNoteId = o.getNoteUUID();
			if (noteId.equals(mediaNoteId)) {
				mediaObjects.add(o);
			}
		}
		return mediaObjects;
	}

	private ArrayList<Tag> getTagsByNoteId(UUID noteId) {
		ArrayList<UUID> tagIds = dbService.getTagIdsByNoteId(noteId);
		return allTags.stream().filter(tag -> tagIds.contains(tag.getId())).collect(Collectors.toCollection(ArrayList::new));
	}

	public void requestUpdate() {
		listener.onNotesChanged();
	}


	/**
	 * returns a copy of all notes
	 *
	 * @return ArrayList copy of currents notes
	 */
	public ArrayList<Note> getAllNotes() {
		ArrayList<Note> currentNotes = new ArrayList<>();
		Collections.sort(allNotes);

		for (Note note : allNotes) {
			currentNotes.add(note.copy());
		}
		return currentNotes;
	}


	public ArrayList<Tag> getCurrentTags() {
		ArrayList<Tag> tags = new ArrayList<>();
		allTags.forEach(tag -> {
			tags.add(tag.copy());
		});
		return tags;
	}

	public void addNote(Note noteToAdd) {
		saveNoteToDbAndToList(noteToAdd);
		persistTags(noteToAdd);
		saveMediaObjectToDB(noteToAdd);
	}

	/**
	 * deletes a Note in the list and in the db with its MediaObjects
	 *
	 * @param noteToDelete note to delete
	 */
	public void deleteNote(Note noteToDelete) {
		dbService.deleteBaseNote(new BaseNote(noteToDelete));
		allNotes.remove(getNotePositionInListBy(noteToDelete.getId()));
		listener.onNotesChanged();
	}

	public void updateNote(Note updatedNote) {
		int position = getNotePositionInListBy(updatedNote.getId());
		Note oldNote = allNotes.get(position);

		updateNoteInDbAndList(position, updatedNote);
		updateMediaObjectFromNote(oldNote, updatedNote);
		saveCreatedTagsToDbAndList(updatedNote);
		updateTagsFromNote(oldNote, updatedNote);
	}


	private void saveNoteToDbAndToList(Note noteToAdd) {
		// save BaseNote to db
		noteToAdd.setEmail(user.getEmailAddress());
		dbService.addBaseNote(new BaseNote(noteToAdd));

		// save Note to internal lists
		allNotes.add(noteToAdd);

		listener.onNotesChanged();
	}

	private void persistTags(Note noteToAdd) {
		saveCreatedTagsToDbAndList(noteToAdd);

		// save tags from new note to helper table in db
		noteToAdd.getTags().forEach(tag -> {
			dbService.addTagToBaseNote(new NoteTag(noteToAdd.getId(), tag.getId()));
		});
	}

	private void saveMediaObjectToDB(Note noteToAdd) {
		// save mediaObject to db
		noteToAdd.getMediaObjects().forEach(mediaObject -> {
			dbService.addMediaObject(mediaObject);
		});
	}

	private void updateNoteInDbAndList(int position, Note updatedNote) {
		if (position != DataSharing.NO_POSITION) {
			allNotes.set(position, updatedNote);
		}
		dbService.updateBaseNote(new BaseNote(updatedNote));
		listener.onNotesChanged();
	}

	private void updateMediaObjectFromNote(Note oldNote, Note updatedNote) {
		ArrayList<MediaObject> oldMOs = oldNote.getMediaObjects();
		ArrayList<MediaObject> currentMOs = updatedNote.getMediaObjects();

		currentMOs.removeAll(oldMOs);
		currentMOs.forEach(mediaObject -> {
			dbService.updateMediaObject(mediaObject);
		});

		oldMOs.remove(updatedNote.getMediaObjects());
		oldMOs.forEach(mediaObject -> {
			dbService.deleteMediaObject(mediaObject);
		});
	}

	private void saveCreatedTagsToDbAndList(Note updatedNote) {
		ArrayList<Tag> noteTags = updatedNote.getTags();

		// extracts the new tags from the note
		ArrayList<Tag> newTags = new ArrayList<>(noteTags);
		newTags.removeAll(allTags);

		if (!newTags.isEmpty()) {
			// save new tags to internal list
			allTags.addAll(newTags);
			newTags.forEach(tag -> {
				// save new tags to db
				dbService.addTag(tag);
			});
			listener.onTagsChanged(newTags, DataSharing.TAGS_ADDED);
		}
	}

	// update NoteTag table in db
	private void updateTagsFromNote(Note oldNote, Note updatedNote) {
		ArrayList<Tag> oldTags = oldNote.getTags();
		ArrayList<Tag> currentTags = updatedNote.getTags();

		currentTags.removeAll(oldTags);
		currentTags.forEach(tag -> {
			dbService.addTagToBaseNote(new NoteTag(updatedNote.getId(), tag.getId()));
		});

		oldTags.remove(updatedNote.getTags());
		oldTags.forEach(tag -> {
			dbService.removeTagFromBaseNote(new NoteTag(updatedNote.getId(), tag.getId()));
		});
	}

	private int getNotePositionInListBy(UUID id) {
		for (int i = 0; i < allNotes.size(); i++) {
			if (allNotes.get(i).getId().equals(id))
				return i;
		}
		return DataSharing.NO_POSITION;
	}

	private int getTagPositionInListBy(UUID id) {
		for (int i = 0; i < allTags.size(); i++) {
			if (allTags.get(i).getId().equals(id)) {
				return i;
			}
		}
		return DataSharing.NO_POSITION;
	}

	public void addTags(ArrayList<Tag> newTags) {
		//saved new tags to db
		newTags.forEach(tag -> {
			dbService.addTag(tag);
		});
		//added new tags to list
		allTags.addAll(newTags);
		listener.onTagsChanged(newTags, DataSharing.TAGS_ADDED);
	}

	public int getTagCount() {
		return allTags.size();
	}

	public int getNoteCount() {
		return allNotes.size();
	}

	public Tag getTagByName(String tagName) {
		for (int i = 0; i < allTags.size(); i++) {
			if (allTags.get(i).getTagName().equals(tagName)) {
				return allTags.get(i);
			}
		}
		return null;
	}

	public void deleteTags(ArrayList<Tag> deletedTags) {
		//deleted tags in db
		deletedTags.forEach(tag -> {
			dbService.deleteTag(tag);
		});
		//deleted tags in list
		allTags.removeAll(deletedTags);
		listener.onTagsChanged(deletedTags, DataSharing.TAGS_DELETED);
	}

	public void updateTags(ArrayList<Tag> editedTags) {
		//updated tags in db
		editedTags.forEach(tag -> {
			dbService.updateTag(tag);
		});
		//updated tags in list
		editedTags.forEach(tag -> {
			int position = getTagPositionInListBy(tag.getId());
			allTags.set(position, tag);
		});
		initNoteLists();
		listener.onNotesChanged();
		listener.onTagsChanged(editedTags, DataSharing.TAGS_EDITED);
	}


	public interface NoteManagerListener {
		void onNotesChanged();

		void onTagsChanged(@NonNull ArrayList<Tag> updatedTags, int operationOnTags);
	}

}

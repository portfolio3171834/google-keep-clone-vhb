package com.passau.notizen.data;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.passau.notizen.R;
import com.passau.notizen.database.tables.Tag;
import com.passau.notizen.ui.util.DataSharing;
import com.passau.notizen.ui.settings.SettingsPreferenceKeys;

import java.util.ArrayList;
import java.util.Collections;

public class NoteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_SECTION_HEADER = 0;
    private final int TYPE_NOTE_ITEM = 1;

    private final Context context;
    private String previewStyle;

    private ArrayList<Note> allNotes;
    private ArrayList<Note> filteredNotes;
    private boolean isArchived;
    private Tag tagFilter;

    private NoteListAdapterListener listener;


    public NoteListAdapter(Context context, NoteListAdapterListener listener) {
        this.context = context;
        setNotePreviewStyle();
        setHasStableIds(true);
        allNotes = new ArrayList<>();
        filteredNotes = new ArrayList<>();
        this.listener = listener;
    }

    /**
     * Sets the preview style according to the preferences
     * */
    private void setNotePreviewStyle() {
        if(context != null) {
            String defaultStyle = context.getResources().getString(R.string.settings_preview_compact);
            this.previewStyle = context.getSharedPreferences(SettingsPreferenceKeys.NAME_SETTINGS_PREFS, Context.MODE_PRIVATE).getString(SettingsPreferenceKeys.KEY_NOTE_ADAPTER_PREVIEW, defaultStyle);
        }
    }

    /**
     * Notifies the adapter, that the preview style of the notes has changed.
     * */
    public void notifyNoteStyleChanged() {
        setNotePreviewStyle();
        notifyDataSetChanged();
    }

    // used by callback Method to update the internal note list
    public void setAllNotes(@NonNull ArrayList<Note> allNotes) {
        Log.d("Notiz", "setAllNotes()!");
        this.allNotes = allNotes;
        filterList();
    }

    @Override
    public long getItemId(int position) {
        return (filteredNotes.get(position).getId().getMostSignificantBits() & Long.MAX_VALUE);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_NOTE_ITEM) {
            View noteItemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_notelist_note_item, parent, false);
            return new ViewHolderNoteItem(noteItemLayout);
        } else if (viewType == TYPE_SECTION_HEADER) {
            View headerPinnedLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_notelist_section_heading, parent, false);
            return new ViewHolderSectionHeading(headerPinnedLayout);
        }
        View headerLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_notelist_section_heading, parent, false);
        return new ViewHolderSectionHeading(headerLayout);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Note currentNote = filteredNotes.get(position);
        String headline = currentNote.getHeadline();
        String body = currentNote.getBody();
        int imageCount = currentNote.getMediaObjects() != null ? currentNote.getMediaObjects().size() : 0;
        String imageCountText = (imageCount == 1) ? imageCount + " " + context.getResources().getString(R.string.image) : imageCount + " " + context.getResources().getString(R.string.images);

        if(previewStyle.equals(context.getResources().getString(R.string.settings_preview_compact))) {
            int headCharCount = context.getResources().getInteger(R.integer.note_preview_compact_character_count_headline);
            int bodyCharCount = context.getResources().getInteger(R.integer.note_preview_compact_character_count_body);

            if(headline != null && headline.length() > headCharCount) {
                Log.d("Notiz", "headline.length() > headCharCount");
                headline = headline.substring(0, headCharCount) + "...";
            }
            if(body != null && body.length() > bodyCharCount) {
                Log.d("Notiz", "body.length() > bodyCharCount");
                body = body.substring(0, bodyCharCount) + "...";
            }
        }

        if (holder instanceof ViewHolderNoteItem) {
            ViewHolderNoteItem itemHolder = (ViewHolderNoteItem) holder;

            itemHolder.noteItemHeader.setVisibility(View.VISIBLE);
            itemHolder.noteItemBody.setVisibility(View.VISIBLE);
            itemHolder.imageItem.setVisibility(View.VISIBLE);

            if (headline == null || headline.isEmpty())
                itemHolder.noteItemHeader.setVisibility(View.GONE);
            else
                itemHolder.noteItemHeader.setText(headline);
            if (body.isEmpty())
                itemHolder.noteItemBody.setVisibility(View.GONE);
            else
                itemHolder.noteItemBody.setText(body);

            if(imageCount == 0) {
                itemHolder.imageItem.setVisibility(View.GONE);
            } else {
                itemHolder.imageItem.setText(imageCountText);
            }

        } else if (holder instanceof ViewHolderSectionHeading) {
            ViewHolderSectionHeading sectionHolder = (ViewHolderSectionHeading) holder;
            sectionHolder.noteItemHeader.setVisibility(View.VISIBLE);
            sectionHolder.noteItemBody.setVisibility(View.VISIBLE);
            sectionHolder.imageItem.setVisibility(View.VISIBLE);


            if (currentNote.isPinned()) {
                sectionHolder.sectionHeader.setText(R.string.view_holder_note_overview_pinned_section_name);
            } else {
                sectionHolder.sectionHeader.setText(R.string.view_holder_note_overview_second_section_name);
            }
            if (headline == null || headline.isEmpty())
                sectionHolder.noteItemHeader.setVisibility(View.GONE);
            else
                sectionHolder.noteItemHeader.setText(headline);
            if (body.isEmpty())
                sectionHolder.noteItemBody.setVisibility(View.GONE);
            else
                sectionHolder.noteItemBody.setText(body);

            if(imageCount == 0) {
                sectionHolder.imageItem.setVisibility(View.GONE);
            } else {
                sectionHolder.imageItem.setText(imageCountText);
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        boolean currentPinned = filteredNotes.get(position).isPinned();
        boolean previousPinned = false;
        if (position - 1 >= 0) {
            previousPinned = filteredNotes.get(position - 1).isPinned();
        }

        if (position == 0 && currentPinned) {
            return TYPE_SECTION_HEADER;
        } else if (position == 0) {
            return TYPE_SECTION_HEADER;
        } else if (!currentPinned && previousPinned) {
            return TYPE_SECTION_HEADER;
        } else return TYPE_NOTE_ITEM;
    }

    @Override
    public int getItemCount() {
        if (filteredNotes == null)
            return 0;
        return filteredNotes.size();
    }

    //gets a Note out of the filtered  List by position
    public Note getNote(int position) {
        return filteredNotes.get(position);
    }

    public String getCurrentTagFilter() {
        if (tagFilter == null) {
            return "";
        } else return tagFilter.getTagName();
    }


    // filters the note list by archived status, and if unarchived by the defined tag name
    private void filterList() {
        filterListForArchived();

        if (!isArchived) {
            filterListByTag();
        }
        notifyDataSetChanged();
    }


    //filters the notes by archived
    private void filterListForArchived() {
        ArrayList<Note> filteredNotes = new ArrayList<>();
        Collections.sort(allNotes);

        for (Note note : allNotes) {
            if (this.isArchived) {
                if (note.isArchived())
                    filteredNotes.add(note.copy());
            } else {
                if (!note.isArchived()) {
                    filteredNotes.add(note.copy());
                }
            }
        }
        this.filteredNotes = filteredNotes;
    }

    //filters by the tag defined
    private void filterListByTag() {
        if (tagFilter != null) {
            ArrayList<Note> tagFilteredNotes = new ArrayList<>();
            for (Note note : filteredNotes) {
                boolean equals = note.getTags().stream().anyMatch(tag -> tag.getTagName().equals(tagFilter.getTagName()));
                if (equals) {
                    tagFilteredNotes.add(note);
                }
            }
            this.filteredNotes = tagFilteredNotes;
        }
    }

    //unsets the tag Filter to show either all unarchived or archived notes
    public void setFilter(boolean isArchived) {
        this.tagFilter = null;
        this.isArchived = isArchived;
        filterListForArchived();
        notifyDataSetChanged();
    }

    //sets a tag filter and filters the unarchived notes by the tag name
    public void setTagFilter(Tag tagFilter) {
        this.tagFilter = tagFilter;
        this.isArchived = false;
        filterListForArchived();
        filterListByTag();
        notifyDataSetChanged();
    }


    public void checkTagFilter(@NonNull ArrayList<Tag> changedTagList, int operationOnTags) {

        for (int i = 0; i < changedTagList.size(); i++) {
            Tag changedTag = changedTagList.get(i);
            String newTagFilter = "";
            if (tagFilter != null) {
                if (changedTag.getId().equals(tagFilter.getId())) {

                    if (operationOnTags == DataSharing.TAGS_EDITED) {
                        setTagFilter(changedTag);
                        newTagFilter = changedTag.getTagName();
                        Log.d("Notiz", "new TagFilter name:  " + newTagFilter);
                    } else if (operationOnTags == DataSharing.TAGS_DELETED) {
                        setTagFilter(null);
                    }
                    listener.onUsedTagFilterChanged(newTagFilter);
                    break;
                }
            }

        }
    }

    static class ViewHolderNoteItem extends RecyclerView.ViewHolder {
        public TextView noteItemHeader;
        public TextView noteItemBody;
        public TextView imageItem;

        public ViewHolderNoteItem(@NonNull View itemView) {
            super(itemView);
            noteItemHeader = itemView.findViewById(R.id.list_note_item_header);
            noteItemBody = itemView.findViewById(R.id.list_note_item_body);
            imageItem = itemView.findViewById(R.id.list_note_item_image);
        }
    }

    static class ViewHolderSectionHeading extends RecyclerView.ViewHolder {
        public TextView sectionHeader;
        public TextView noteItemHeader;
        public TextView noteItemBody;
        public TextView imageItem;

        public ViewHolderSectionHeading(@NonNull View itemView) {
            super(itemView);
            sectionHeader = itemView.findViewById(R.id.list_note_section_header);
            noteItemHeader = itemView.findViewById(R.id.list_note_item_header);
            noteItemBody = itemView.findViewById(R.id.list_note_item_body);
            imageItem = itemView.findViewById(R.id.list_note_item_image);
        }
    }

    public interface NoteListAdapterListener {
        void onUsedTagFilterChanged(String newFilterName);
    }

}
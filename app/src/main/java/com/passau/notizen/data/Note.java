package com.passau.notizen.data;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.passau.notizen.database.tables.BaseNote;
import com.passau.notizen.database.tables.MediaObject;
import com.passau.notizen.database.tables.Tag;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class Note implements Comparable<com.passau.notizen.data.Note>, Parcelable {


    private UUID id;
    private String headline;
    private String body;
    private String email;

    private boolean isArchived;
    private boolean isPinned;

    private LocalDateTime lastChange;
    private LocalDateTime dueDate;

    private ArrayList<MediaObject> mediaObjects;
    private ArrayList<Tag> tags;

    public Note() {

    }

    public Note(UUID id, String headline, String body, String email, boolean isArchived, boolean isPinned, LocalDateTime lastChange, LocalDateTime dueDate, ArrayList<MediaObject> mediaObjects, ArrayList<Tag> tags) {
        this.id = id;
        this.headline = headline;
        this.body = body;
        this.email = email;
        this.isArchived = isArchived;
        this.isPinned = isPinned;
        this.lastChange = lastChange;
        this.dueDate = dueDate;
        this.mediaObjects = mediaObjects;
        this.tags = tags;
    }

    public Note(String headline, String body, String email, boolean isArchived, boolean isPinned, LocalDateTime dueDate, ArrayList<MediaObject> mediaObjects, ArrayList<Tag> tags) {
        this.headline = headline;
        this.body = body;
        this.isArchived = isArchived;
        this.isPinned = isPinned;
        this.dueDate = dueDate;
        this.mediaObjects = mediaObjects;
        this.tags = tags;
        this.email = email;
        this.id = UUID.randomUUID();
        this.lastChange = LocalDateTime.now();
    }

    public Note copy() {
        return new Note(
                id, headline, body,
                email, isArchived, isPinned,
                lastChange, dueDate,
                mediaObjects, tags
        );
    }

	/*=====================
	========= GETTER ======
	* ====================*/

    public UUID getId() {
        return id;
    }

    public String getHeadline() {
        return headline;
    }

    public String getBody() {
        return body;
    }

    public String getEmail() {
        return email;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public boolean isPinned() {
        return isPinned;
    }

    public LocalDateTime getLastChange() {
        return lastChange;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public ArrayList<MediaObject> getMediaObjects() {
        return mediaObjects;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }



	/*=====================
	========= SETTER ======
	* ====================*/

    public Note setHeadline(String headline) {
        this.headline = headline;
        return this;
    }

    public Note setBody(String body) {
        this.body = body;
        return this;
    }

    public Note setLastChange(LocalDateTime lastChange) {
        this.lastChange = lastChange;
        return this;
    }

    public Note setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public Note setMediaObjects(ArrayList<MediaObject> mediaObjects) {
        this.mediaObjects = mediaObjects;
        return this;
    }

    public Note setTags(ArrayList<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public Note setId(UUID id) {
        this.id = id;
        return this;
    }

    public Note setEmail(String email) {
        this.email = email;
        return this;
    }

    public Note setArchived(boolean archived) {
        isArchived = archived;
        return this;
    }

    public Note setPinned(boolean pinned) {
        isPinned = pinned;
        return this;
    }

    public void toggleIsArchived() {
        this.isArchived = !this.isArchived;
    }

    public void toggleIsPinned() {
        this.isPinned = !this.isPinned;
    }

    @NonNull
    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", headline='" + headline + '\'' +
                ", body='" + body + '\'' +
                ", email='" + email + '\'' +
                ", isArchived=" + isArchived +
                ", isPinned=" + isPinned +
                ", lastChange=" + lastChange +
                ", dueDate=" + dueDate +
                ", mediaObjects=" + mediaObjects +
                ", tags=" + tags +
                '}';
    }


    /**
     * method to sort the Note first by isPinned and then by the lastChanged date
     *
     * @param o second Note
     * @return
     */

    @Override
    public int compareTo(Note o) {
        int result = 0;

        if (this.isPinned() == o.isPinned())
            result = 0;
        else if (this.isPinned() && !o.isPinned())
            result = -1;
        else if (!this.isPinned() && o.isPinned())
            result = 1;

        if (result == 0) {
            result = this.getLastChange().compareTo(o.getLastChange()) *(-1);
        }
        return result;
    }
	/*=====================
	===== Parcelable ======
	* ====================*/


    protected Note(Parcel in) {
        id = UUID.fromString(in.readString());
        String tempHeadline = in.readString();
        headline = tempHeadline.equals("") ? null : tempHeadline;
        body = in.readString();
        email = in.readString();
        isArchived = in.readByte() != 0;
        isPinned = in.readByte() != 0;
        lastChange = Instant.ofEpochMilli(in.readLong()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        long tempDueDate = in.readLong();
        dueDate = tempDueDate == -1 ? null : Instant.ofEpochMilli(tempDueDate).atZone(ZoneId.systemDefault()).toLocalDateTime();
        mediaObjects = in.createTypedArrayList(MediaObject.CREATOR);
        tags = in.createTypedArrayList(Tag.CREATOR);
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id.toString());
        dest.writeString(headline == null ? "" : headline);
        dest.writeString(body);
        dest.writeString(email);
        dest.writeByte((byte) (isArchived ? 1 : 0));
        dest.writeByte((byte) (isPinned ? 1 : 0));
        dest.writeLong(lastChange.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        dest.writeLong(dueDate == null ? -1 : dueDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        dest.writeTypedList(mediaObjects);
        dest.writeTypedList(tags);

    }
}

package com.passau.notizen.data;

import android.graphics.Bitmap;

public class AttachmentThumbnail {
	private final String fileName;
	private final Bitmap thumbnail;

	public AttachmentThumbnail(String fileName, Bitmap thumbnail) {
		this.fileName = fileName;
		this.thumbnail = thumbnail;
	}

	public String getFileName() {
		return fileName;
	}

	public Bitmap getThumbnail() {
		return thumbnail;
	}
}

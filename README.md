# Notiz App für Android 

## Projektbeschreibung/ Projektziele
Die App soll Teil eines verschlüsselten Notizen-Ökosystems sein, mit der Notizen geräteübergreifend erstellt, bearbeitet und betrachtet werden können. Die Notizen werden dabei sowohl lokal gespeichert, als auch synchron auf einem Server gesichert. Jedes verbundene Gerät hat somit immer den gleichen Stand.

### Allgemeines
* minSdk 28
* targetSdk 31
* compileSdk 31

* sourceCompatibility JavaVersion.VERSION_1_8
* targetCompatibility JavaVersion.VERSION_1_8

### Aufbau des Codes

Die Packages gliedern sich in:
* cryptography (Datenbankverschlüsselung)
* data (Klassen für Objekte, die in der View verwendet werden)
* database (Klassen/Interfaces für die Datenbank)
* sync (Klassen für den Synchronisationsservice)
* ui (Activities und Fragments; unter-pakete aufgeteilt nach Screens)


### Funktionen
**Datenbank Integration auf dem Gerät**

Für die Datenbank wurde die room Bibliothek verwendet.
Die Datenbank ergibt sich aus 4 Tabellen. Die BaseNote enthält alle Informationen zur Notiz und ist mit den Tags verknüpft. Die Tag tabelle enthält alle Tags. Die Tag und BaseNote tabelle wurde über eine Zwischentablle namens NoteTags verknüpft, die jeweil die ID der verknüpften BaseNote und ID des zugehörigen Tags enthält. Des Weiteren gibt es die User Tabelle, die alle Benutzer, die registriert sind enthält.

![struktur](https://user-images.githubusercontent.com/78700458/160297211-7f83d054-57fe-4de2-93d1-8bded60ac6f9.png)

Die Tabelle MediaObject enthält die verknüpfung zu den angelegten Medien wie Bildern. In den Interfaces mit dem Namen der Tabelle und DAO (siehe Bild) sind die einzelnen Datenbankabfragen von den jeweiligen Tabellen. In jeder DAO befindet sich eine Insert Funktion, um ein neues Objekt zur Tabelle hinzuzufügen, eine Update Funktion, um die vorhandenen Objekte zu aktualisieren und eine delete Funktion, um ein Objekt aus der Datenbank zu löschen. Um das richtige Objekt zu erhalten, wird jeweils das zu bearbeitende Objekt an die DAO übergeben. Zusätzlich haben wir eine TypeConverter Klasse für die Datenbank erstellt, welche alle benötigten übergaben in Valide datentypen umwandelt. Diese wird benötigt um daten wie Date auf der datenbank zu speichern.

### Verschlüsselung der Datenbank
Angelegt nach diesem Blogpost. Für die Verschlüsselung wurde die Bibliothek SQLCipher verwendet, welche bei der initialisierung der Datenbank als Factory Übergeben werden kann.
```java
SupportFactory factory = new SupportFactory(SQLiteDatabase.getBytes(dbKey));
db = Room.databaseBuilder(context, NoteDatabase.class, DATABASE_NAME)
       .openHelperFactory(factory)
       .allowMainThreadQueries().build();
```

Dafür wird zuerst ein 256-bit Schlüssel zufällig generiert, welcher in einen Hex-Array   umgewandelt wird. Dies stellt nun den Key dar, der zur Verschlüsselung der DB verwendet werden kann. Um diesen Schlüssel jedoch nicht nach dem schließen der App zu verlieren, wird dieser über das vom User gelieferte Passwort verschlüsselt. Dafür wird ein Salt (zusätzlich zufälliger Wert) und das Passwort der Users mit zu einem geheimen Schlüssel durch einen hmac und der SHA256 zusammengefügt. Dieser wird verwendet, um den Key der Datenbank über den AES-Algorithmus im GCM-Modus zu verschlüsseln. Der dabei verwendete Initialisierungsvektor, der Salt und dem verschlüsselten Datenbank-Key werden in einem Objekt gespeichert, in ein Json-String umgewandelt und in den shared-Präferenzen der App gespeichert. Dies ist das Storable-Objekt. Für die Implementierung der Logik wurde das Singelton-Objekt DatabaseKeyManager erstellt, welcher alle benötigten Methoden beinhaltet.

### Login Verwaltung (Google-Integration)
[**Location**](app/src/main/java/com/passau/notizen/ui/login/LoginActivity.java) 

Beim start der App gelangt man durch die LoginActivity auf den Login Screen. Alle Screens für den login Prozess haben wir mit Fragments realisiert die auf der LoginActivity aufgerufen werden. Über das “fragment_login.xml” kann sich der Benutzer mit einem Passwort und einer Email-Adresse anmelde und die Eingabe mit dem Login Button bestätigen. Beim drücken des Buttons werden auch verschiedene Abfragen gemacht, u.a. ob bereits der benutzer vorhanden ist oder die Email und das Passwort stimmen. Durch den “Sign up” Button wird die “LoginRegisterFragment” Klasse aufgerufen und der Screen vom “fragment_login_register.xml” erscheint. Hier kann sich der Benutzer nun für die App Registrieren und muss dafür eine E-Mail und ein Passwort, welches er bestätigen muss, eingeben. Hier werden die Anforderungen an das Passwort geprüft und ob die eingegebenen Passwörter übereinstimmen.

Ebenfalls kann man sich mittels seines Google Kontos registrieren oder anmelden durch den Google Sign in Button auf dem Login Screen. Wenn sich der Benutzer mit seinem google Konto anmeldet, wird als zusätzliche Sicherheitsanforderung ein Pin abgefragt. Bei der ersten anmeldung mit einem Google konto muss zuerst dieser Pin festgelegt werden. Dafür gibt es jeweils das AssignPinFragment und EnterPinFragment. Ob der Benutzer bereits angemeldet war, wird in einer Shared Preference hinterlegt, dennoch muss immer das Passwort zur anmeldung eingegeben werden.
Das Anmelden mit dem google Konto kann leider beim Debuggen nicht aufgerufen werden, wenn nicht explizit ein Google API Console-Projekt für den PC, auf welchem der debugger gestartet wird angelegt wurde. Dafür wird der SHA-1-Hash des Gerätes benötigt.

Das überprüfen des Passworts oder Pins wird mit der Entschlüsselung der Datenbank und einer darauf folgenden Anfrage an die db getestet. Wenn die Datenbankabfrage erfolgreich war, wird der Benutzer auf den Overview screen weitergeleitet (Siehe nachfolgende Methode).

```java
protected boolean dataBaseCall(String email, @NonNull String password) {
     User user = null;
     boolean success = false;

     try {
        dbService = NoteDatabaseService.getInstance(this, password.toCharArray());
        user = dbService.getUserByMail(email);
        success = true;
     } catch (Exception | DatabaseKeyManager.DatabaseKeyException exception) {
     }

     if (success) {
        if (user == null) {
           dbService.addUser(new User(email, "", LocalDateTime.now()));
           String toastText = getResources().getString(R.string.account_added) + "\"" + email + "\"";
           Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
        }
        intentOverviewScreen(email, password);
        return true;
     }
     return false;
  }

```

### Navigation über ein “Burger Menü”
[**Location**](app/src/main/java/com/passau/notizen/ui/overview/OverviewActivity.java) 

In der Übersichts-Activity kann der Nutzer die Notizen über angelegte Tags filtern, die über ein Drawer-Layout aufgerufen werden. Dort befinden sich auch die Einstiegspunkte zu den Einstellungen, den Acoount-Settings und dem Archiv. Das Menü wird über entsprechende XML-Menüs erstellt, wobei die Tags zur Filterung programmatisch und dynamisch eingefügt werden. Die Navigation lebt in der OverViewActivity - dort werden auch die Tags programmatisch hinzugefügt, mit denen der Nutzer die vorhandenen Notizen filtern kann.

### Anlegen und Bearbeiten von Notizen
[**Location**](app/src/main/java/com/passau/notizen/ui/notedetail/NoteDetailActivity.java) 

Eine Notiz kann einen Titel, einen Beschreibungstext, Anhänge und Tags haben, sowie in der Übersicht angepinnt oder ins Archiv geschoben werden. Von dort werden sie entgültig gelöscht oder ent-archiviert. Die Erstellung und Bearbeitung findet über “NoteDetailEditFragment.java” statt, welche in der “NoteDetailActivity.java” Activity lebt. Diese Activity besitzt lediglich eine FragmentView, innerhalb der - je nach Anwendungsfall - die Fragments zur Bearbeitung/Betrachtung der verschiedenen Notiz-Elemente eingeblendet werden.

Die Activity ist Owner für das ViewModel, welches in allen zugehörigen Fragments zur Verarbeitung von Daten verwendet wird.


**Anhänge für Notizen**

Beim Anlegen und Bearbeiten einer Notiz können Fotos aufgenommen und als Anhänge hinzugefügt werden.


**Aufnahme von Bildern**

Über den Menüpunkt “Attachments”, welcher einen an das Design angepassten Dialog erstellt, kann den Nutzer ein Foto aufnehmen. Hierzu wird ein Intent für die Fotoaufnahme erstellt. Der Nutzer bekommt diese Option nur angezeigt wenn das Gerät über eine Kamera verfügt und einen Dienst zu Aufnahme bereit stellen kann. Nach dem Klick wird die Kamera-Permission geprüft und sollte der Nutzer noch nicht eingewilligt haben, wird ein Dialog angezeigt. Dies geschieht auf “freiweilliger Basis”, da ein Kamera-Intent auch ohne Berechtigung möglich wäre, da die Bilder im internen Verzeichnis gespeichert werden. Dem Intent  wird der App-interne Speicherpfad für Bilder übergeben. Sofern der Nutzer das Aufnehmen abbricht, wird die erstellte (und leere) Bilddatei wieder gelöscht. Andernfalls wird es der Notiz angefügt und ist in der RecyclerView für Thumbnails sichtbar.


**Betrachten von Bildern**

[**Location**](app/src/main/java/com/passau/notizen/ui/notedetail/fragments/NoteDetailViewImageFragment.java) 

Hat der Nutzer ein oder mehrer Fotos hinzugefügt kann er diese über eine _Thumbnail-Recycler-View_ im unteren Bereich des Screens aufrufen, betrachten und auch wieder löschen. Ausstehende Funktionen, zu denen wir nicht mehr gekommen sind, wären das Abspeichern im öffentlichen Storage, sowie das Versenden von Bildern.

Im Layout wurde, um Zoom- und Drag-Gesten zu ermöglichen, die externe Bibliothek _Subsampling Scale Image View_ als ImageView verwendet.

Für die einfachere Ansprache von Medien-Dateien wurde die Hilfsklasse _AttachmentManager_ erstellt, welche statische Methoden zum Abrufen von Bildern und Generieren von z.B. Thumbnails bereitstellt.


**Erstellen & Auswählen von Tags**

[**Location**](app/src/main/java/com/passau/notizen/ui/notedetail/fragments/NoteDetailTagFragment.java) 

Um einer Notiz einen oder mehrere Tags zuzuweisen, kann der Nutzer über den Menüpunkt “Tags” auswählen und neue erstellen. Es können keine Tags erstellt werden, die bereits mit dem gleichen Namen vorhanden sind. Die Tags werden in ihrem Fragment über eine RecyclerView dargestellt.


**Tag-RecyclerView & -Adapter**

Der zuerst erstellte Adapter für die Darstellung der Tags wurde mit zwei View-Holdern erstellt. Einen für vom User ausgewählte Tags, den anderen für nicht ausgewählte Tags. Im weiter unten beschriebenen Adapter wurde dies durch das ein- und ausblenden von verschiedenen Layout-Containern gelöst um nicht zwei ViewHolder verwenden zu müssen.
Klickt der Nutzer auf einen Eintrag, wird das Tag als ab/angewählt markiert und entsprechend im ViewModel vermerkt.

### Interaktive RecyclerView | Bearbeiten von Tags
[**Location**](app/src/main/java/com/passau/notizen/ui/tag/RecyclerViewTagEditAdapter.java) 

Die separate Activity zur Bearbeitung (umbenennen, löschen und hinzufügen) von Tags, besteht zum großen Teil aus einer RecyclerView. Dort wurde nicht mit verschiedenen ViewHoldern gearbeitet, sondern die Layouts für die Zustände “angewählt” und “nicht angewählt” über include im Layout des ViewHolders platziert. Über einen eigenen Listener, der “View.OnFocusChangeListener” und “View.OnClickListener” implementiert, wird die Funktionalität und Logik gesteuert.


### Updaten der RecyclerView der Notes
**Schnittstelle zur DB-Ebene und dem UI**

[**Location**](app/src/main/java/com/passau/notizen/data/NoteManager.java) 

Die Schnittstelle zwischen der Datenbank Ebene und dem UI stellt der NoteManager dar. Dieser hält als Instanzvariablen immer den aktuellen Stand der DB in Listen fest, welche alle Notizen mit MediaObjekten und Zugehörigen Tags sowie die Liste aller vorhandenen Tags beinhalten. Zusätzlich wird der NoteManager as Singelton angelegt, damit es eine single source of truth bezüglich der Listen in der App vorhanden ist. Der Notemanager greift über eine Instanz des DatabaseServices auf die Datenbank zu und nimmt dort die Änderungen des Users vor. Dabei werden die Listen parallel zu den DB-Änderungen gepflegt. Um die OverviewActivity, welche den Notemanger instanziiert und verwendet, über die Änderungen in den Listen zu benachrichtigen, wurde das Interface NoteManagerListener mit zwei Methoden erstellt, welche bei Änderungen in der Tag- und Note-Liste aufgerufen werden. Dieses Interface wird von der OverviewActivity implementiert, so dass der Adapter für die RecyclerView die Liste für diese aktualisieren kann. Bei den erhaltenen Listen handelt es sich um Deep-Copies der tatsächlichen Liste des Managers.


**Updaten der RecyclerView per Adapter**

[**Location**](app/src/main/java/com/passau/notizen/data/NoteListAdapter.java) 

Der NoteListAdapter wird in der OverViewActivity an den Recycler gehangen. Im Adapter selbst,  werden die Filter für die interne Liste verwaltet und dementsprechend der ViewHolder befüllt. Es besteht die Möglichkeit die angezeigten Notizen nach einem ausgewählten Tag zu Filtern. Ebenso wird die Liste nach archivierten Notizen gefiltert sobald der User das Archiv auswählt.

### UI-Elemente
Für eine angepasste Darstellung wurden in den Ressources mehrere Styles definiert, sowie eigene Button-Themes und auch Animationen eingefügt. Für ein einheitliches UI wurden unter anderem festgelegte Abstände, Schriftgrößen und Farben festgehalten, die über die Ressourcen im Code und den Layouts aufgerufen werden.

### Serverdienste
Zur Umsetzung der Synchronisation der Notizen zwischen Geräten wurde ein Webservice mittels Spring Boot / Spring Web implementiert. Dieser läuft unter diesem: [Link](https://sharednotes.christopheuskirchen.de.) 

Das Repository des Webservices ist einsehbar unter diesem [Link]( https://gitlab.com/Chreuseo/passauwebservicenotitzapp/.) 

### Service für längere Hintergrundabfrage (Synchronisation mit dem Server)
**WebserviceController**

[**Location**](app/src/main/java/com/passau/notizen/sync/WebServiceController.java) 

Um eine Synchronisation betreiben zu können müssen entsprechende Webrequests ausgeführt werden. Diese sind alle im WebserviceContoller implementiert:
* public void getAllNotes(Context context, SyncService syncService, String userMail) throws DatabaseKeyManager.DatabaseKeyException
* public List<NoteDTO> getMissingNotes(Context context, SyncService syncService, List<String> noteIDs)
* public void uploadNotes(Context context, SyncService syncService, List<NoteDTO> noteDTOs)

Dabei wird der Context übergeben um eine RequestQueue instanziieren zu können und die SyncService-Instanz aus der er aufgerufen wird um nach dem Erhalt der Antwort vom Server die Daten entsprechend verarbeiten zu können.

       
**SyncService**
       
[**Location**](app/src/main/java/com/passau/notizen/sync/SyncService.java) 
       
Der SyncService ist ein als Android-Service implementiertes Konstrukt um die Synchronisation durchzuführen ohne den Haupt-Thread zu belasten. Bei manuellem Start der Synchronisation wird eine Notification gesendet, während der Synchronisationsvorgang läuft, um den User auf den laufenden Prozess aufmerksam zu machen. 

```java
private Notification getNotificationForForegroundService() {
   return new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
           .setContentTitle(getText(R.string.sync_foreground_notification_title))
           .setContentText(getText(R.string.sync_foreground_notification_text))
           .setSmallIcon(R.drawable.ic_outline_sync_24)
           .build();
}
```
       
Für den Synchronisationsprozess wird ein Webservicecontroller instanziiert und die RemoteNotes über getAllNotes() mit dem lokalen Datenbankstand verglichen. Wurden Notes geändert oder hinzugefügt werden die Änderungen übernommen und ein Broadcast an die OverviewActivity gesendet um die Ansicht neu zu laden.
       
**PeriodicSyncService**
       
[**Location**](app/src/main/java/com/passau/notizen/sync/PeriodicSyncService.java) 
       
Der PeriodicSyncService wird kontinuierlich während der Ausführung der App ausgeführt um regelmäßig einen Synchronisationsprozess anzustoßen. Dieser erstellt einen SyncService alle 2 Minuten.


### Handling zwischen Fragments und Activities
[**Location**](app/src/main/java/com/passau/notizen/ui/login/LoginActivity.java) 

Mittels dem Aufruf der Methode changeFragment, welche sich in einem Fragment befindet, wird das aktuelle Fragment im Stack nach hinten geschoben und ein neues Fragment, welches der Methode übergeben wird, nach vorne gesetzt und aufgerufen. Für diese Interaktion wird die FragmentTransaction Klasse verwendet. Für das zurückgehen über den zurück Button am Device oder in der App wir die popBackStack
Methode verwendet [sie Codebeispiel].
Code:
```java
private void changeFragment(Fragment newFragment) {
  FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
  fragmentTransaction.replace(R.id.login_fragment_view, newFragment);
  fragmentTransaction.addToBackStack(null);
  fragmentTransaction.commit();
}

private void popBackStack() {
  requireActivity().getSupportFragmentManager().popBackStack();
}
```
       
Um zwischen Activitys zu wechseln wird ein Intent verwendet. Hier wird die aktuell verwendete Activity und die aufzurufende Klasse übergeben[Siehe Codebeispiel]. Zusätzlich können noch Daten mit übergeben werden. Zusätzlich wird in der “NoteDetailActivity” bei ändern der Activity zur vorherigen in der “onBackPressed” Methode noch ein Alert für den Benutzer angezeigt, da der Inhalt sonst verloren geht.

Code (LoginActivity):
```java
private void intentOverviewScreen(String email, String password) {
  Intent myIntent = new Intent(this, OverviewActivity.class);
  myIntent.putExtra(DataSharing.KEY_LOGIN_EMAIL, email);
  myIntent.putExtra(DataSharing.KEY_LOGIN_PASSWORD, password);
  startActivity(myIntent);
  finish();
}
@Override
public void onBackPressed() {
  if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
     getSupportFragmentManager().popBackStack();
  } else {
     showAlertDialog();
  }
}
```

### Externe Libraries und Dependencies
* Subsampling Scale Image View für interaktive ImageViews
* ItemClickSupport für performantere RecyclerView-Onclick-Handling
* Volley zum Versenden von Webrequests
* SQLCipher

## Team

### Vorstellung
* Manuel Thöne 
       * Getestet auf Emulator API 30 & 29, sowie echtem Gerät mit API 30  
* Amin Yacine
       * Getestet auf Emulator API 28  
* Marco Wenger
       * Getestet auf Emulator API 28,30, sowie echtem Gerät mit API 31  
* Christoph Euskirchen
       * Getestet auf Emulator API 30

       
### Zuständigkeiten
Manuel Thöne
* UX-Prototyp vor der Implementierung
* Overview, Tag-Edit, Account und Settings
* Activity, Fragments & ViewModel für die Notenbearbeitung  
       
Amin Yacine
* Verschlüsselung der Datenbank
* Implementierung einiger Datenklassen und DAOS
* Webservice: Controller
* Webservice: Datenbank
* NoteManager
* NoteListAdapter  
       
Marco Wenger
* DatabaseService
* Implementierung einiger Datenklassen und DAOS
* Login, Registrierung + Google Sign In  
       
Christoph Euskirchen
* Synchronisation Service Android
* Notification der Synchronisation
* Webservice: Netzwerk-Zugriffsarchitektur / DNS / HTTPS-Reverse-Proxy
* Webservice: Controller

### Take-Aways und Fazit
Da einige Funktionen, die zunächst leicht umsetzbar gewirkt, mehr Zeit in Anspruch genommen haben und viele der in der Vorlesung behandelten Funktionen mittlerweile als deprecated markiert waren, fiel ein Großteil der Zeit für die Umsetzung der Recherche und dem “Neulernen” zum Opfer. 

Die regelmäßigen Team-Meetings (für gewöhnlich Montags und Freitags) hielten alle Teammitglieder auf dem aktuellen Stand und ermöglichten es die einzelnen Module selbstständig zu bearbeiten. Leider konnten jedoch aus Zeitmangel nicht alle Funktionen wie anfänglich geplant umgesetzt werden.

Das Speichern der Attachments benötigt leider etwas zeit und wird nicht ganz ausgeführt, wenn die App zu schnell beendet wird nach dem Anlegen. Das liegt daran, dass wir es leider zeitlich nicht geschafft haben, die Datenbankabfragen auf einen seperate Thread zu legen.

### Vorgehen
       
Zunächst haben wir uns im Team grundlegende Funktionen für die App überlegt. Grundlegend für uns war eine Datenbank auf dem Gerät für das Speichern der Notizen. Den Notizen wollten wir Daten wie Bilder oder Sprachaufzeichnungen anhängen. Zusätzlich brauchen wir einen Login und zusätzlich bieten wir einen Google Login anbieten. Zu einfacheren Navigierbarkeit zwischen den Funktionen stellen wir ein Burger-Menü zur Verfügung. Für die Sicherstellung der Daten müssen wir diese auf dem Gerät verschlüsselt speichern und zusätzlich auf einem Server verschlüsselt Synchronisieren. Beim Synchronisieren zeigt die App den Status der Synchronisation in einer nicht-interaktiven Notification an. Ebenfalls soll die Synchronisation mit dem Server für Entlastung des UI einen eigenen Service verwenden. Für die verschiedenen Interaktionen werden wir auch verschiedene Adapter benötigen. 
       
Nachdem die Anforderungen an unsere App festgelegt waren, haben wir die Aufgaben aufgeteilt. So haben sich zunächst Amin und Christoph um den Server und das Backend für die Kommunikation gekümmert und Manuel und Marco haben ein Prototyp erstellt, um die Interaktionen zu erkennen und ein grobes Layout festzulegen.
Mithilfe von trello.com, einem Aufgabenmanager, haben wir die Aufgaben in der Gruppe organisiert und zugewiesen. Ebenso haben wir uns zu besseren Absprachen 2x in der Woche zu festen Terminen (jour fix) getroffen und unseren neuen stand abgesprochen.
       
Für die App Erstellung habe wir zunächst die Datenbank auf dem Gerät erstellt und implementiert [siehe Datenbank Integration]. Währenddessen wurden bereits mehrere Layouts für Screens erstellt sowie das Burger-Menü. Daraufhin wurden die verschiedenen Aktivitäten implementiert und benötigte Adapter erstellt. Währenddessen wurden die Funktionen regelmäßig getestet und von anderen Projektbeteiligten überprüft. Die Funktionen wurden nach erfolgreichen Tests regelmäßig zusammengeführt (mittels GitHub Merge) und es wurde mit dem neuen Stand weitergearbeitet.

## Design-Prototyp in Adobe XD
![Overview_Prototype](https://user-images.githubusercontent.com/46481173/160302242-87df677f-2796-467e-be7b-961af7a169b4.png)
